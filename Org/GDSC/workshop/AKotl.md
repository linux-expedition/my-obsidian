[Android Mobile App Developer Tools – Android Developers](https://developer.android.com/)
https://developers.google.com/community/gdsc/leads
[Teach Android Development  |  Android Developers](https://developer.android.com/teach)
[Quality  |  Android Developers](https://developer.android.com/quality)
[Samples  |  Android Developers](https://developer.android.com/samples)
[Developer Guides  |  Android Developers](https://developer.android.com/guide)
[Design for Android  |  Android Developers](https://developer.android.com/design)

### College
 #### 3. Bigger Number
 #### 7. Layout & Widget
 #### 8. Static & Dynamic List
 #### 9. Click list event
 #### 10. Game Vocab dynamic list
 #### 11. File & Activity Lifecycle
 #### 12. (theory) Activity lifecycle
 #### 13. Activity lifecycle
 #### 14. Shared Preference
 #### 15. Intent
 #### 16. Intent I
 #### 17. Intent II
 #### 18. Fragment to Fragment Activity
 #### Dynamic UI
 #### Media Player
 #### Media Player text to speech camera
 #### Grafik 2D

#### Lecture
Prerequisites
##### 1 Start kotlin
- Install repl through intell ij or android studio
- operators
- data types
- variables
- conditionals
- lists and arrays
- null safety
##### 4 Build first Android app
- ini project
- anatomy project directory 
- layout & resources
- activities
- Make an app interactive
- Gradle
- Accessbility
##### 13. App UI design
- Android styling
- typography
- Material Design
- Material components
- localization
- example apps
