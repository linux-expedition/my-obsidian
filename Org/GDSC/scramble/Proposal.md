#### Questioned
- rangkap jabatan club/ukm/bpmu/bemu/hmp
- Perjinan diklat
#### Answered
- proses pengajuan menjadi UKM
- jumlah kegiatan rutin per semester
	- 5
- Keikutsertaan review proker, machung festival, sidang  akhir
	- menyendiri
- keikutsertaan pic bpmu
	- per meetup
- Teknis peminjaman ruangan
	- baa - bka

### Google Developer Student Clubs are university based community groups for students interested in Google developer technologies. Students from all undergraduate or graduate programs with an interest in growing as a developer are welcome. By joining a GDSC, students grow their knowledge in a peer-to-peer learning environment and build solutions for local businesses and their community.


#### Step to build
- Judul + logo + tahun bulan
- Daftar isi
- Bab 1 Pendahuluan / latar belakang - visi misi
- Bab 2 Tujuan Kegiatan
- Bab 3 Manfaat Kegiatan (Universitas - pihak penyelenggara - mahasiswa)
- Bab 4 Sasaran kegiatan (kualitatif & kuantitatif)
- Bab 5 Timeline dan tema ukm
- Bab 6 Evaluasi dari kegiatan sebelumnya (ketika jadi club)
- Bab 7 Pengurus ukm
- Bab 8 rencana besaran kredit keaktifan
- penutup (pendamping ukm - ketua gdsc - ketua bpmu - kepala bka)
- lampiran (foto/bukti tentang gdsc)

