###### What is your motivation to run a Google Developer Student Club at your Institution?
yes, I have been a part of a local community before. Their main objective was to build connections and educate people by running seminars or workshops for free. Yet most of my colleagues are not aware of this community or any other local community. This brings to me gdsc that introduce about the existence of local communities and building connection. This can be done through a partnership of an event or any kind of thing

educate, partnership to local communities, 


###### What is your experience in leading a project or a team?
each team or project that I've ever led always has its unique set of problems. And the most challenging that I've ever encountered is idea generation. I solved it by listing all the ideas whether from myself or my teammate. Then cast voting with an explanation for each vote. But the best thing is all of the options that are available, are up to the lead to decide. To put it simply, the fate of being a successful team or not is in the hand of the team lead


###### What technology do you find most interesting and why? What is your experience with this technology?
And I choose it for all of my projects because it has a great popularity, friendly communities, the documentation is easy to understand, many free tutorials available online, and codes are much simpler than native php. But order to update the version of an older project requires great effort to do it. It makes me think twice about whether the latest version is necessary or not. Also I knew about livewire, webpack, jetstream is by using laravel.

###### only through gdsc
	being part of international community
	able to participate soultion challenge
	have circle or connection between gdsc from other college
###### what you do now
bringing theory from college to practice. Via workshop, seminar, or some kind of event. and introduce local community from it. 
Because through local community, we're able to recognize set of problem in our society. Or widen our circle to have more opportunity to finding job
or just search for help to those that already expert of it
###### skill required
- ability to teach people 
	- (patient, knowledge of the material that being taught)
	- recognize those in need
	- helpfull
- social
	- empathy
- critical thinking
- integrity
- realistic
###### step to realize
introduce gdsc and motivate about benefit of it
Strengthen relationship to getting know of each other
	treat other like friend, everyone are same
Ask some local community to become partner
Workshop with local community

### 1. Decisiveness
-   [Problem-solving](https://www.indeed.com/career-advice/resumes-cover-letters/problem-solving-skills)
-   [Initiative](https://www.indeed.com/career-advice/career-development/ways-to-take-initiative-at-work)
-   [Research](https://www.indeed.com/career-advice/career-development/research-skills)
-   Evaluation
-   Expectation setting

### 2. Integrity
-   [Diplomatic](https://www.indeed.com/career-advice/resumes-cover-letters/diplomatic-skills)    
-   [Ethical](https://www.indeed.com/career-advice/career-development/ethical-leadership)
-   [Reliability](https://www.indeed.com/career-advice/career-development/work-ethic-skills)
-   [Professionalism](https://www.indeed.com/career-advice/career-development/the-ultimate-guide-to-professionalism)
-   Confidentiality
-   Honest

### 3. Relationship building (or team building)
-   [Collaboration](https://www.indeed.com/career-advice/career-development/collaboration-skills)
-   [Management](https://www.indeed.com/career-advice/career-development/management-skills)
-   [Interpersonal](https://www.indeed.com/career-advice/resumes-cover-letters/interpersonal-skills)
-   [Social](https://www.indeed.com/career-advice/career-development/social-skills)
-   [Communication](https://www.indeed.com/career-advice/resumes-cover-letters/communication-skills)
-   [Active listening](https://www.indeed.com/career-advice/career-development/active-listening-skills)
-   [Teamwork](https://www.indeed.com/career-advice/career-development/teamwork-skills)

### 4. Problem-solving
-   [Critical thinking](https://www.indeed.com/career-advice/career-development/critical-thinking-skills)
-   [Analytical skills](https://www.indeed.com/career-advice/resumes-cover-letters/analytical-skills)
-   [Research](https://www.indeed.com/career-advice/career-development/research-skills)
-   [Decisiveness](https://www.indeed.com/career-advice/career-development/decision-making-skills)

### 5. Dependability
-   [Realistic goal-setting](https://www.indeed.com/career-advice/career-development/set-goals-at-work)
-   [Integrity](https://www.indeed.com/career-advice/career-development/integrity-at-work)
-   [Timeliness](https://www.indeed.com/career-advice/career-development/time-management-skills)
-   [Initiative](https://www.indeed.com/career-advice/career-development/what-is-a-self-starter)
-   [Detail-oriented](https://www.indeed.com/career-advice/resumes-cover-letters/detail-oriented)
-   Loyal
-   Truthfulness

### 6. Ability to teach and mentor
-   [Motivation](https://www.indeed.com/career-advice/career-development/how-to-motivate-employees)
-   Clarity
-   Able to recognize and reward
-   Understanding employee differences
-   Assessing
-   Helpfulness
-   Positive reinforcement

