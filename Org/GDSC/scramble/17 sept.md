# GDSC
grow knowledge, network with their peirs, solution impact local communities and businesses

----
#### Purpose
- [ ] Gain membership from other program study
- [ ] Maintain an active member
- [ ] Collaboration with other gdsc
- [ ] Organize a successfull workshop
- [ ] Form a team to submit solution challenge

---
#### Policy
- Min an event in 90 days
- Contribute kas in the amount of 10k for each meetup
- Meetup once or twice a week
- Ijin maks H+1
- Maintain professional integrity

---

#### Time line
![[timeline-gdsc.png]]

----
#### Target
- Beginning of october Open House
- October - Mid Nov, weekly event
- December Holiday with some assigment
- 2023...

----

#### Faculty Advisor
Paulus Lucky Irmawan

----
#### Community manager
Fikry
- Deciding Content
- Gain renown

----
#### Management & Administration
Marvin
Louis
- Meeting notes
- Attendees
- Treasury Manager
- Manage project resource
- Proposal, LPJ, LPK

----
#### Developer
Teresha
Natasha
- IT advisor
- Program manager
- Project Manager
- Speaker
- Documentation Project

---
#### Event Organizer
Stanley
Vincent Dwi
- Manage events
- Speaker Matching
- Performance event measurer

---
#### Media & Creative
Natanael
Kenji
- Design Poster, Content, Certif, etc
- Documentation
- Content writer


---
#### Public Relation
Steven
- First Responder Sosmed
- Explore Partnership
- Securing sponsorship
- Event promotion

---
#### Recognize job desc
- Schedule meetup
- Build connection between gdsc
- Design Content: Intro new core team and Open House
- Registration and Feedback Form for an event
- Rundown

---

#### Usefull link
https://developers.google.com
https://gdsc.community.dev/

---
