1. What is your motivation to run a Google Developer Student Club at your Institution?
	Have you been a part of any community before? Mention what inspires you to run a community and how GDSC would impact your community
	
2. What is your experience in leading a project or a team?
	Tell us about your experience as a team leader. What was challenging for you, how did you solve problems, what is the best part of being a leader for you?
3. What technology do you find most interesting and why? What is your experience with this technology?
	What technology do you find most interesting and why? What is your experience with this technology?
4. Record a 90 second video and show us why you are the right person to be a GDSC Leader at your Institution. In the video feel free to present yourself and mention what you think is relevant and what we should know about you.


1. yes, I have been a part of a local community before. Their main objective was to build connections and educate people by running seminars or workshops for free. Yet most of my colleagues are not aware of this community or any other local community. This brings to me gdsc that introduce about the existence of local communities and building connection. This can be done through a partnership of an event or any kind of thing

2. each team or project that I've ever led always has its unique set of problems. And the most challenging that I've ever encountered is idea generation. I solved it by listing all the ideas whether from myself or my teammate. Then cast voting with an explanation for each vote. But the best thing is all of the options that are available, are up to the lead to decide. To put it simply, the fate of being a successful team or not is in the hand of the team lead

3. Framework Laravel is the most interesting. Simply just because the only one that use on my project about backend and it makes me know deep enough to know the benefit of it.  
And I choose it for all of my projects because it has a great popularity, friendly communities, the documentation is easy to understand, many free tutorials available online, and codes are much simpler than native php.  
But order to update the version of an older project requires great effort to do it. It makes me think twice about whether the latest version is necessary or not.

Greetings, my name is Fikry Catur Farenza from Ma Chung University in Indonesia. 
I am studying informatics engineering and will be graduating next year. I've been leading many events in the last semester. I have led a college organization for a half year up until now and successfully realize aspirations among lecturers and students through events that have been made.
As a junior backend engineer, I have finished up a project the last semester using laravel 8, where I have a chance to learn and practice alpine js and livewire. I have excellent time management which have helped me to always keep up between the project that I was working on and the college activity. I am very excited about preprocessing css and javascript and also about tailwind and typescript. Pretty sure by learning that technology will help me to assist some of the frontend's works.
However, there is always still much to learn and improve about myself and also, especially about the community in college. Which led me to gdsc.
that's it that I can tell about myself. If there is any mistaken words or attitude, I deeply apologize. So good day and have a nice day, thank you

menghubungkan aspirasi di antara dosen dan mahasiswa melalui event yang telah dibuat