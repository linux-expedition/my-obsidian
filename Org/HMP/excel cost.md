
### Tujuan
Membantu menemukan kost yang sesuai

---
### Batasan
- Malang
- Pemberi Informasi

---
### Opsi yg Dipakai
- mamikos
	Menyarankan pakai app
- excel
	Menyebarluaskan informasi kost melalui excel open access

---
### mamikos App
- Direkomendasikan pada saat penyebarannya nanti
- Sumber info yang bisa dipake di excel

---
## Excel
---
### Tools
- Form online
- 2 Excel
	- Excel A: untuk masyarakat umum
	- Excel B: hasil generate dari form
- platform: Microsoft

---
### Teknis Excel A
1. Buat Excel A dengan nama" kolom yang dibutuhkan dalam pencarian kost
	- Di onedrive HMP by account HMP
2. Isi Excel A dengan data yang sudah ada di Mamikos
3. Lengkapi data di Excel A dengan informasi yang belum ada di Mamikos
4. Jadikan Excel A open access dengan restriction: View only
5. Excel A ke HMP, restriction: Can edit

---
### Teknis Excel B dari Form
1. Buat form dengan pertanyaan mengenai info kost (tidak ada yang required)
2. Restrict yang bisa mengisi, warga Ma Chung saja
3. Buat beberapa section, diantaranya:
	1. Report untuk perbaikan atau update info
	2. Menambahkan info baru
4. Generate excel dari form tersebut atau Excel B
5. Excel B ke HMP, restriction: Can edit

---

### Job Desc
- Send info ke public (Draft) (BPH)
- Judul form (PS)
- Judul Excel A (PS)
- Init Excel A
	https://machungac-my.sharepoint.com/:x:/g/personal/hmp_tif_machung_ac_id/EbvgJyATQXJGqs09R-r1TDUBrDEiv6kaCBGPHUZO4oAWjw?e=2HGoFM
- Isi Excel A kolom info (PR, SW)
- Isi Excel A dgn info dari Mamikos (PS)
- PIC 
	rekomendasi org yang akan di contact:
	- list ke note
	- execute BPH
- Init Form [x]
	https://forms.office.com/Pages/DesignPage.aspx?fragment=FormId%3DNEV6NyeFtEiEr7xaqQZSiW3BjhytYidPpWJOtLIsx11UNzI4SlpTWEhSMjRFSVpYU1FJRkgzWjZKUi4u%26Token%3D0142cc17f1e245ab9a1a6fe2199d2bd0
- Isi Form dengan pertanyaan & generate Excel B 
	https://machungac-my.sharepoint.com/:x:/g/personal/hmp_tif_machung_ac_id/EUEYNoO4bhlIrLXO0CsQPyAB10aPLEhKSd2G62MEW6SAHQ?e=YuKOqb

view only excel A
https://machungac-my.sharepoint.com/:x:/g/personal/hmp_tif_machung_ac_id/EbvgJyATQXJGqs09R-r1TDUBWVuJieLxQedOTMbqvqyniA?e=pn4lRn&isSPOFile=1
- Daily Check hasil form







#### 

sosialisasi internal => jujur masih belum menghu





































1. yoga
2. dito
3. agnes
4. david
5. fufuk
6. marvin
7. cevin
8. advent
9. handika
10. vincent
11. nata
12. henrie
13. ibra