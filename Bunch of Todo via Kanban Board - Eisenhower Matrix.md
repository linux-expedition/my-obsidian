---

kanban-plugin: basic

---

## ## Urgent & Important

- [ ] **DO IT NOW!!**


## ## URGENT, NOT IMPORTANT

- [ ] **DELEGATE OR DO IT AFTER TASK BESIDE ME**


## ## IMPORTANT, NOT URGENT

- [ ] **DECIDE WHEN TO DO IT**
- [ ] KTM, LATEST SELFIE for PITCH TOEIC
- [ ] certificate seminar it
- [ ] dicoding course
- [ ] machis certificate?


## ## NOT URGENT, NOT IMPORTANT

- [ ] **DO IT LATER / DUMP IT**
- [ ] dicoding - cloud if interesting -> see rps cloud computing to checking redundant material
- [ ] certificate scatter around the inet
- [ ] tidying colab (ml, fuzzy, etc)
- [ ] [[Note Management]]


## done

- [ ] apriori @{2022-04-03}
- [ ] idea generation @{2022-04-02}
- [ ] pustaka @{2022-04-02}@@{23:30}
- [ ] cloud computing, m-12 lab
- [ ] Embed System & IoT
- [ ] Excel Open Access
- [ ] Techno weekly progress
- [ ] Purge MetPen Assignments
- [ ] presentasi ide wirausaha<br>latar blkg, solusi yg ditawarkan, gambaran kebutuhan biaya<br>buat video, unggah di channel 12 sblm hari rabu




%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%