https://itexamanswers.net/modules-13-17-threats-and-attacks-group-exam-answers.html
https://quizlet.com/627118684/threats-and-attacks-group-chapter-13-17-flash-cards/

Which two types of attacks are examples of reconnaissance attacks? (Choose two.)
-   <ins>port scan</ins>
-   SYN flood
-   brute force
-   **ping sweep**
-   man-in-the-middle

What is the significant characteristic of worm malware?
-   Once installed on a host system, a worm does not replicate itself.
-   **A worm can execute independently of the host system.**
-   Worm malware disguises itself as legitimate software.
-   A worm must be triggered by an event on the host system.
-   Navigation Bar

Which type of security attack would attempt a buffer overflow?
-   scareware
-   reconnaissance
-   ransomware
-   **DoS**

Why would a rootkit be used by a hacker?
-   **to gain access to a device without being detected**
-   to try to guess a password
-   to do reconnaissance
-   to reverse engineer binary files

Which two functions are provided by NetFlow? (Choose two.)
-   It allows an administrator to capture real-time network traffic and analyze the entire contents of packets.
**-   It provides a complete audit trail of basic information about every IP flow forwarded on a device.**
-   It uses artificial intelligence to detect incidents and aid in incident analysis and response.
**-   It provides 24x7 statistics on packets that flow through a Cisco router or multilayer switch.**
-   It presents correlated and aggregated event data in real-time monitoring and long-term summaries.

Match the network monitoring solution with a description. (Not all options are used.)
used to capture traffic and show what is happening on the network
**Protocl analysis**
monitors traffic and compares it against configured rules
**IPS**
copies frames received on one or more ports to a port connected to an analysis device
**SPAN**

What functionality is provided by Cisco SPAN in a switched network?
-   It copies traffic that passes through a switch interface and sends the data directly to a syslog or SNMP server for analysis.
-   It protects the switched network from receiving BPDUs on ports that should not be receiving them.
-   It inspects voice protocols to ensure that SIP, SCCP, H.323, and MGCP requests conform to voice standards.
-   It prevents traffic on a LAN from being disrupted by a broadcast storm.
-   It mitigates MAC address overflow attacks.
**-   It mirrors traffic that passes through a switch port or VLAN to another port for traffic analysis.**

Which statement describes the function of the SPAN tool used in a Cisco switch?
**-   It copies the traffic from one switch port and sends it to another switch port that is connected to a monitoring device.**
-   It provides interconnection between VLANs over multiple switches.
-   It supports the SNMP trap operation on a switch.
-   It is a secure channel for a switch to send logging to a syslog server.

What are three functionalities provided by SOAR? (Choose three.)
**-   It automates complex incident response procedures and investigations.**
-   It provides a complete audit trail of basic information about every IP flow forwarded on a device.
-   It provides 24x7 statistics on packets that flow through a Cisco router or multilayer switch.
**-   It provides case management tools that allow cybersecurity personnel to research and investigate incidents.**
-   It presents the correlated and aggregated event data in real-time monitoring and long-term summaries.
**-   It uses artificial intelligence to detect incidents and aid in incident analysis and response.**

Once a cyber threat has been verified, the US Cybersecurity Infrastructure and Security Agency (CISA) automatically shares the cybersecurity information with public and private organizations. What is this automated system called?
-   NCSA
-   NCASM
-   ENISA
**-   AIS**

Which attack involves threat actors positioning themselves between a source and destination with the intent of transparently monitoring, capturing, and controlling the communication?
**-   man-in-the-middle attack**
-   DoS attack
-   SYN flood attack
-   ICMP attack

Match the security concept to the description.
the likelihood of undesireable consequences
**risk**
a mechanism used to compromise an asset
**exploit**
a weakness in a system
**vulnerability**
a potential danger to an asset
**threat**

What is the goal of a white hat hacker?
**-   protecting data**
-   validating data
-   modifying data
-   stealing data

Match the type of cyberattackers to the description. (Not all options are used.)
hacktivist
**make political statement in order to create an awareness of issues that are important to them**
vulnerability brokers
**discover exploits and report them to vendors**
state sponsored attackers
**gather intellegence or commit sabotage on behalf of the government**

How can a DNS tunneling attack be mitigated?
-   by securing all domain owner accounts
-   by preventing devices from using gratuitous ARP
-   by using strong passwords and two-factor authentication
**-   by using a filter that inspects DNS traffic**

An attacker is redirecting traffic to a false default gateway in an attempt to intercept the data traffic of a switched network. What type of attack could achieve this?
**-   DHCP spoofing**
-   DHCP snooping
-   MAC address starvation
-   MAC address snooping

What is the function of a gratuitous ARP sent by a networked device when it boots up?
**-   to advise connected devices of its MAC address**
-   to request the MAC address of the DNS server
-   to request the netbios name of the connected system
-   to request the IP address of the connected network

Which cyber attack involves a coordinated attack from a botnet of zombie computers?
**-   DDoS**
-   address spoofing
-   ICMP redirect
-   MITM

What would be the target of an SQL injection attack?
-   DNS
**-   database**
-   email
-   DHCP

A network administrator is checking the system logs and notices unusual connectivity tests to multiple well-known ports on a server. What kind of potential network attack could this indicate?
-   information theft
-   access
**-   reconnaissance**
-   denial of service

Which devices should be secured to mitigate against MAC address spoofing attacks?
**-   Layer 2 devices**
-   Layer 7 devices
-   Layer 4 devices
-   Layer 3 devices

Which field in the IPv6 header points to optional network layer information that is carried in the IPv6 packet?
**-   next header**
-   flow label
-   traffic class
-   version

Why would an attacker want to spoof a MAC address?
-   so that the attacker can capture traffic from multiple VLANs rather than from just the VLAN that is assigned to the port to which the attacker device is attached
-   so that a switch on the LAN will start forwarding all frames toward the device that is under control of the attacker (that can then capture the LAN traffic)
-   so that the attacker can launch another type of attack in order to gain access to the switch
**-   so that a switch on the LAN will start forwarding frames to the attacker instead of to the legitimate host**

What are two purposes of launching a reconnaissance attack on a network? (Choose two.)
-   to escalate access privileges
-   to retrieve and modify data
-   to prevent other users from accessing the system
**-   to gather information about the network and devices**
-   **to scan for accessibility**
