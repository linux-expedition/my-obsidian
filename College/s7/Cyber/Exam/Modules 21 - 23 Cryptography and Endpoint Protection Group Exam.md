[It exam answers](https://itexamanswers.net/modules-21-23-cryptography-and-endpoint-protection-group-exam-answers.html)

Match the network profile element to the description
- **port used**
	- a list of TCP or UDP processes that are available to accept data
- **critical asset address space**
	- the IP addresses or the logical location of essential systems or data
- **session duration**
	- the time between the establishment of a data flow and its termination
- **total throughput**
	- the amount of data passing from a given source to a given destination in a given period of time


Which statement describes the Cisco Threat Grid Glovebox?
-   **It is a sandbox product for analyzing malware behaviors.**
-   It is a host-based intrusion detection system (HIDS) solution to fight against malware.
-   It is a firewall appliance.
-   It is a network-based IDS/IPS.


Which technology is used by Cisco Advanced Malware Protection (AMP) in defending and protecting against known and emerging threats?
-   website filtering and block listing
-   network profiling
-   **threat intelligence**
-   network admission control


An administrator suspects polymorphic malware has successfully entered the network past the HIDS system perimeter. The polymorphic malware is, however, successfully identified and isolated. What must the administrator do to create signatures to prevent the file from entering the network again?
-   **Execute the polymorphic file in the Cisco Threat Grid Glovebox.**
-   Use Cisco AMP to track the trajectory of a file through the network.
-   Run a baseline to establish an accepted amount of risk, and the environmental components that contribute to the risk level of the polymorphic malware.
-   Run the Cisco Talos security intelligence service.


Which statement describes the term iptables?
-   It is a file used by a DHCP server to store current active IP addresses.
-   **It is a rule-based firewall application in Linux.**
-   It is a DHCP application in Windows.
-   It is a DNS daemon in Linux.


What is a feature of distributed firewalls?
-   They use only iptables to configure network rules.
-   They use only TCP wrappers to configure rule-based access control and logging systems.
-   They all use an open sharing standard platform.
-   **They combine the feature of host-based firewalls with centralized management.**


Which statement describes the policy-based intrusion detection approach?
-   **It compares the operations of a host against well-defined security rules.**
-   It compares the antimalware definitions to a central repository for the latest updates.
-   It compares the behaviors of a host to an established baseline to identify potential intrusion.
-   It compares the signatures of incoming traffic to a known intrusion database.


What is the difference between an HIDS and a firewall?

-   A firewall performs packet filtering and therefore is limited in effectiveness, whereas an HIDS blocks intrusions.
-   An HIDS works like an IPS, whereas a firewall just monitors traffic.
-   **An HIDS monitors operating systems on host computers and processes file system activity. Firewalls allow or deny traffic between the computer and other systems.**
-   An HIDS blocks intrusions, whereas a firewall filters them.
-   A firewall allows and denies traffic based on rules and an HIDS monitors network traffic.


Which technique could be used by security personnel to analyze a suspicious file in a safe environment?
-   **sandboxing**
-   block listing
-   baselining
-   allow listing


What is the purpose for using digital signatures for code signing?
-   to establish an encrypted connection to exchange confidential data with a vendor website
-   to authenticate the identity of the system with a vendor website
-   **to verify the integrity of executable files downloaded from a vendor website**
-   to generate a virtual ID


A company is developing a security policy for secure communication. In the exchange of critical messages between a headquarters office and a branch office, a hash value should only be recalculated with a predetermined code, thus ensuring the validity of data source. Which aspect of secure communications is addressed?
-   non-repudiation
-   data integrity
-   data confidentiality
-   **origin authentication**


What technology has a function of using trusted third-party protocols to issue credentials that are accepted as an authoritative identity?
-   symmetric keys
-   **PKI certificates**
-   digital signatures
-   hashing algorithms


What is the purpose of a digital certificate?
-   It ensures that the person who is gaining access to a network device is authorized.
-   It provides proof that data has a traditional signature attached.
-   **It authenticates a website and establishes a secure connection to exchange confidential data.**
-   It guarantees that a website has not been hacked


Which two statements correctly describe certificate classes used in the PKI? (Choose two.)
-   A class 0 certificate is more trusted than a class 1 certificate.
-   The lower the class number, the more trusted the certificate.
-   **A class 4 certificate is for online business transactions between companies.**
-   A class 5 certificate is for users with a focus on verification of email.
-   **A class 0 certificate is for testing purposes.**


What is the purpose of the DH algorithm?
-   to provide nonrepudiation support
-   to encrypt data traffic after a VPN is established
-   **to generate a shared secret between two hosts that have not communicated before**
-   to support email data confidentiality


A customer purchases an item from an e-commerce site. The e-commerce site must maintain proof that the data exchange took place between the site and the customer. Which feature of digital signatures is required?
-   confidentiality of the public key
-   **nonrepudiation of the transaction**
-   integrity of digitally signed data
-   authenticity of digitally signed data


Which type of attack does the use of HMACs protect against?
-   DoS
-   brute force
-   **man-in-the-middle**
-   DDoS


Which two classes of metrics are included in the CVSS Base Metric Group? (Choose two.)
-   Exploit Code Maturity
-   Modified Base
-   **Exploitability**
-   Confidentiality Requirement
-   **Impact metrics**


Which security management plan specifies a component that involves tracking the location and configuration of networked devices and software across an enterprise?
-   risk management
-   **asset management**
-   patch management
-   vulnerability management


A cybersecurity analyst is performing a CVSS assessment on an attack where a web link was sent to several employees. Once clicked, an internal attack was launched. Which CVSS Base Metric Group Exploitability metric is used to document that the user had to click on the link in order for the attack to occur?
-   availability requirement
-   integrity requirement
-   **user interaction**
-   scope


In what order are the steps in the vulnerability management life cycle conducted?
-   discover, prioritize assets, assess, remediate, verify, report
-   discover, prioritize assets, assess, remediate, report, verify
-   discover, assess, prioritize assets, report, remediate, verify
-   **discover, prioritize assets, assess, report, remediate, verify**


In addressing an identified risk, which strategy aims to stop performing the activities that create risk?
-   risk retention
-   risk sharing
-   risk reduction
-   **risk avoidance**


When a server profile for an organization is being established, which element describes the TCP and UDP daemons and ports that are allowed to be open on the server?
-   service accounts
-   critical asset address space
-   **listening ports**
-   software environment


In network security assessments, which type of test employs software to scan internal networks and Internet facing servers for various types of vulnerabilities?
-   penetration testing
-   **vulnerability assessment**
-   strength of network security testing
-   risk analysis


In addressing a risk that has low potential impact and relatively high cost of mitigation or reduction, which strategy will accept the risk and its consequences?
-   **risk retention**
-   risk avoidance
-   risk sharing
-   risk reduction