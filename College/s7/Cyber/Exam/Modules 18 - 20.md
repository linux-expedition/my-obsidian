https://itexamanswers.net/modules-18-20-network-defense-group-exam-answers.html

Match the type of business policy to the description.
defines system requirements and objectives, rules, and requirements for users when they attach to or on the network
**security**
protects the rights of workers and the company interests
**company**
identifies salary, pay schedule, benefits, work schedule, vacations, etc.
**employee**

Match the information security component with the description.
Only authorized individuals, entities, or processes can access sensitive information.
**confidentially**
Data is protected from unauthorized alteration.
**integrity**
Authorized users must have uninterrupted access to important resources and data.
**availability**

Match the threat intelligence sharing standards with the description.
This is the specification for an application layer protocol that allows the communication of CTI over HTTPS.
**TAXII**
This is a set of specifications for exchanging cyberthreat information between organizations.
**STIX**
This is is a set of standardized schemata for specifying, capturing, characterizing, and communicating events and properties of network operations.
**CybOX**

What does the incident handling procedures security policy describe?
-   It describes the procedure for auditing the network after a cyberattack.
**-   It describes how security incidents are handled.**
-   It describes how to prevent various cyberattacks.
-   It describes the procedure for mitigating cyberattacks.

In a defense-in-depth approach, which three options must be identified to effectively defend a network against attacks? (Choose three.)
-   total number of devices that attach to the wired and wireless network
-   past security breaches
-   location of attacker or attackers
**-   vulnerabilities in the system**
**-   assets that need protection**
**-   threats to assets**

What is the benefit of a defense-in-depth approach?
-   Only a single layer of security at the network core is required.
-   All network vulnerabilities are mitigated.
-   The need for firewalls is eliminated.
**-   The effectiveness of other security measures is not impacted when a security mechanism fails.**

What is the purpose of mobile device management (MDM) software?
-   It is used by threat actors to penetrate the system.
**-   It is used to implement security policies, setting, and software configurations on mobile devices.**
-   It is used to create a security policy.
-   It is used to identify potential mobile device vulnerabilities

What is the first line of defense when an organization is using a defense-in-depth approach to network security?
-   IPS
**-   edge router**
-   firewall
-   proxy server

Why is asset management a critical function of a growing organization against security threats?
-   It serves to preserve an audit trail of all new purchases.
**-   It identifies the ever increasing attack surface to threats.**
-   It allows for a build of a comprehensive AUP.
-   It prevents theft of older assets that are decommissioned.

Which two options are security best practices that help mitigate BYOD risks? (Choose two.)
-   Only allow devices that have been approved by the corporate IT team.
**-   Keep the device OS and software updated.**
**-   Only turn on Wi-Fi when using the wireless network.**
-   Decrease the wireless antenna gain level.
-   Use wireless MAC address filtering.
-   Use paint that reflects wireless signals and glass that prevents the signals from going outside the building.

When designing a prototype network for a new server farm, a network designer chooses to use redundant links to connect to the rest of the network. Which business goal will be addressed by this choice?
-   scalability
**-   availability**
-   manageability
-   security

Which AAA component can be established using token cards?
**-   authentication**
-   accounting
-   authorization
-   auditing

Which term describes the ability of a web server to keep a log of the users who access the server, as well as the length of time they use it?
**-   accounting**
-   assigning permissions
-   authorization
-   authentication

What is the principle behind the nondiscretionary access control model?
**-   It allows access decisions to be based on roles and responsibilities of a user within the organization.**
-   It applies the strictest access control possible.
-   It allows users to control access to their data as owners of that data.
-   It allows access based on attributes of the object be to accessed.

When a security audit is performed at a company, the auditor reports that new users have access to network resources beyond their normal job roles. Additionally, users who move to different positions retain their prior permissions. What kind of violation is occurring?
**-   least privilege**
-   network policy
-   password
-   audit

Which type of access control applies the strictest access control and is commonly used in military or mission critical applications?
-   discretionary access control (DAC)
**-   mandatory access control (MAC)**
-   Non-discretionary access control
-   attribute-based access control (ABAC)

Passwords, passphrases, and PINs are examples of which security term?
-   identification
-   authorization
-   access
**-   authentication**

What are two characteristics of the RADIUS protocol? (Choose two.)
-   the use of TCP port 49
-   encryption of the entire body of the packet
-   the separation of the authentication and authorization processes
**-   the use of UDP ports for authentication and accounting**
**-   encryption of the password only**

What is the primary function of the Center for Internet Security (CIS)?
-   to provide a security news portal that aggregates the latest breaking news pertaining to alerts, exploits, and vulnerabilities
-   to provide vendor-neutral education products and career services to industry professionals worldwide
**-   to offer 24x7 cyberthreat warnings and advisories, vulnerability identification, and mitigation and incident responses**
-   to maintain a list of common vulnerabilities and exposures (CVE) used by security organizations

How does AIS address a newly discovered threat?
-   by advising the U.S. Federal Government to publish internal response strategies
-   by creating response strategies against the new threat
-   by mitigating the attack with active response defense mechanisms
**-   by enabling real-time exchange of cyberthreat indicators with U.S. Federal Government and the private sector**

What is the primary purpose of the Malware Information Sharing Platform (MISP) ?
-   to exchange all the response mechanisms to known threats
-   to provide a set of standardized schemata for specifying and capturing events and properties of network operations
-   to publish all informational materials on known and newly discovered cyberthreats
**-   to enable automated sharing of IOCs between people and machines using the STIX and other exports formats**

Which statement describes Trusted Automated Exchange of Indicator Information (TAXII)?
**-   It is the specification for an application layer protocol that allows the communication of CTI over HTTPS.**
-   It is a dynamic database of real-time vulnerabilities.
-   It is a signature-less engine utilizing stateful attack analysis to detect zero-day threats.
-   It is a set of specifications for exchanging cyber threat information between organizations.

What is the primary purpose of the Forum of Incident Response and Security Teams (FIRST)?
-   to provide vendor neutral education products and career services to industry professionals worldwide
**-   to enable a variety of computer security incident response teams to collaborate, cooperate, and coordinate information sharing, incident prevention, and rapid reaction strategies**
-   to offer 24x7 cyberthreat warnings and advisories, vulnerability identification, and mitigation and incident response
-   to provide a security news portal that aggregates the latest breaking news pertaining to alerts, exploits, and vulnerabilities

How does FireEye detect and prevent zero-day attacks?
-   by only accepting encrypted data packets that validate against their configured hash values
**-   by addressing all stages of an attack lifecycle with a signature-less engine utilizing stateful attack analysis**
-   by keeping a detailed analysis of all viruses and malware
-   by establishing an authentication parameter prior to any data exchange

Which organization defines unique CVE Identifiers for publicly known information-security vulnerabilities that make it easier to share data?
-   FireEye
**-   MITRE**
-   Cisco Talos
-   DHS