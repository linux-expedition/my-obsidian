[IT EXAM ANSWERS](https://itexamanswers.net/cyberops-associate-1-0-practice-final-exam-answers.html)


Which two statements describe access attacks? (Choose two.)
-   **Password attacks can be implemented by the use of brute-force attack methods, Trojan horses, or packet sniffers.**
-   Trust exploitation attacks often involve the use of a laptop to act as a rogue access point to capture and copy all network traffic in a public location, such as a wireless hotspot.
-   **Buffer overflow attacks write data beyond the allocated buffer memory to overwrite valid data or to exploit systems to execute malicious code.**
-   Port redirection attacks use a network adapter card in promiscuous mode to capture all network packets that are sent across a LAN.
-   To detect listening services, port scanning attacks scan a range of TCP or UDP port numbers on a host.


After complaints from users, a technician identifies that the college web server is running very slowly. A check of the server reveals that there are an unusually large number of TCP requests coming from multiple locations on the Internet. What is the source of the problem?
-   **A DDoS attack is in progress.**
-   There is insufficient bandwidth to connect to the server.
-   There is a replay attack in progress.
-   The server is infected with a virus.


Users report that a database file on the main server cannot be accessed. A database administrator verifies the issue and notices that the database file is now encrypted. The organization receives a threatening email demanding payment for the decryption of the database file. What type of attack has the organization experienced?
-   Trojan horse
-   **ransomware**
-   DoS attack
-   man-in-the-middle attack


What two kinds of personal information can be sold on the dark web by cybercriminals? (Choose two.)
-   name of a bank
-   city of residence
-   **street address**
-   **Facebook photos**
-   name of a pet


What is the most common goal of search engine optimization (SEO) poisoning?
-   **to increase web traffic to malicious sites**
-   to trick someone into installing malware or divulging personal information
-   to build a botnet of zombies
-   to overwhelm a network device with maliciously formed packets


An attacker is redirecting traffic to a false default gateway in an attempt to intercept the data traffic of a switched network. What type of attack could achieve this?
-   ARP cache poisoning
-   TCP SYN flood
-   DNS tunneling
-   **DHCP spoofing**


A network administrator is configuring an AAA server to manage TACACS+ authentication. What are two attributes of TACACS+ authentication? (Choose two.)
-   **separate processes for authentication and authorization**
-   encryption for only the password of a user
-   TCP port 40
-   UDP port 1645
-   single process for authentication and authorization
-   **encryption for all communication**


A network security specialist is tasked to implement a security measure that monitors the status of critical files in the data center and sends an immediate alert if any file is modified. Which aspect of secure communications is addressed by this security measure?
-   data confidentiality
-   nonrepudiation
-   origin authentication
-   **data integrity**


Which statement describes the state of the administrator and guest accounts after a user installs Windows desktop version to a new computer?
-   By default, the guest account is enabled but the administrator account is disabled.
-   By default, both the administrator and guest accounts are enabled.
-   **By default, both the administrator and guest accounts are disabled.**
-   By default, the administrator account is enabled but the guest account is disabled.


What is a purpose of entering the **nslookup cisco.com** command on a Windows PC?
-   to test if the Cisco server is reachable
-   **to check if the DNS service is running**
-   to discover the transmission time needed to reach the Cisco server
-   to connect to the Cisco server


Which two actions can be taken when configuring Windows Firewall? (Choose two.)
-   Enable MAC address authentication.
-   **Allow a different software firewall to control access.**
-   Perform a rollback.
-   Turn on port screening.
-   **Manually open ports that are required for specific applications.**


What are two monitoring tools that capture network traffic and forward it to network monitoring devices? (Choose two.)
-   **SPAN**
-   Wireshark
-   SIEM
-   **network tap**
-   SNMP


Which network monitoring tool is in the category of network protocol analyzers?
-   SIEM
-   SNMP
-   SPAN
-   **Wireshark**


Refer to the exhibit. Which field in the Sguil application window indicates the priority of an event or set of correlated events?
![[Pasted image 20221206030510.png]]
-   AlertID
-   **ST**
-   Pr
-   CNT


What is indicated by a true negative security alert classification?
-   **Normal traffic is correctly ignored and erroneous alerts are not being issued.**
-   An alert is verified to be an actual security incident.
-   Exploits are not being detected by the security systems that are in place.
-   An alert is incorrectly issued and does not indicate an actual security incident.


Match the job titles to SOC personnel positions. (Not all options are used.)
- involved in hunting for potential threats and implements threat detection tools
	- **Tier 3 Subject Matter Expert**
- involved in deep investigation of incidents
	- **Tier 2 Invident Responder**
- monitors incoming alerts and verifies that a true incident has occurred
	- **Tier 1 Alert Analyst**


How does a security information and event management system (SIEM) in a SOC help the personnel fight against security threats?
-   by dynamically implementing firewall rules
-   by analyzing logging data in real time
-   by integrating all security devices and appliances in an organization
-   **by combining data from multiple technologies**


Which three algorithms are designed to generate and verify digital signatures? (Choose three.)
-   **ECDSA**
-   IKE
-   AES
-   **RSA**
-   3DES
-   **DSA**


What are the two important components of a public key infrastructure (PKI) used in network security? (Choose two.)
-   pre-shared key generation
-   **certificate authority**
-   intrusion prevention system
-   symmetric encryption algorithms
-   **digital certificates**


Which three pieces of information are found in session data? (Choose three.)
-   default gateway IP address
-   source and destination MAC addresses
-   **source and destination IP addresses**
-   user name
-   **source and destination port numbers**
-   **Layer 4 transport protocol**


What are two elements that form the PRI value in a syslog message? (Choose two.)
-   hostname
-   timestamp
-   **facility**
-   header
-   **severity**


How might corporate IT professionals deal with DNS-based cyber threats?
-   Use IPS/IDS devices to scan internal corporate traffic.
-   **Monitor DNS proxy server logs and look for unusual DNS queries.**
-   Limit the number of simultaneously opened browsers or browser tabs.
-   Limit the number of DNS queries permitted within the organization.


How does using HTTPS complicate network security monitoring?
-   Web browser traffic is directed to infected servers.
-   HTTPS cannot protect visitors to a company-provided web site.
-   HTTPS can be used to infiltrate DNS queries.
-   **HTTPS adds complexity to captured packets.**


Which network monitoring tool saves captured network frames in PCAP files?
-   SNMP
-   NetFlow
-   **Wireshark**
-   SIEM


What is the TCP mechanism used in congestion avoidance?
-   three-way handshake
-   two-way handshake
-   **sliding window**
-   socket pair


What is the purpose of CSMA/CA?
-   to prevent loops
-   to isolate traffic
-   **to prevent collisions**
-   to filter traffic


A router has received a packet destined for a network that is in the routing table. What steps does the router perform to send this packet on its way? Match the step to the task performed by the router.
- Step 1
	- It de-encapsulates the Layer 2 frame header and trailer to expose the Layer 3 packet.
- Step 2
	- It examines the destination IP address to find the best path in the routing table.
- Step 3
	- It encapsulates the Layer 3 packet into a new Layer 2 frame and forwards the frame out the exit interface.


What are three benefits of using symbolic links over hard links in Linux? (Choose three.)
-   **They can link to a directory.**
-   **They can link to a file in a different file system.**
-   **They can show the location of the original file.**
-   They can be encrypted.
-   Symbolic links can be exported.
-   They can be compressed.


Based on the command output shown, which file permission or permissions have been assigned to the other user group for the data.txt file?  
  
**ls –l data.txt**  
-rwxrw-r-- sales staff 1028 May 28 15:50 data.txt

-   read, write, execute
-   **read**
-   read, write
-   full access


Which two types of network traffic are from protocols that generate a lot of routine traffic? (Choose two.)
-   **routing updates traffic**
-   **STP traffic**
-   IPsec traffic
-   Windows security auditing alert traffic
-   SSL traffic


How is the event ID assigned in Sguil?
-   All events in the series of correlated events are assigned the same event group ID.
-   All events in the series of correlated events are assigned the same event ID.
-   Only the first event in the series of correlated events is assigned a unique ID.
-   **Each event in the series of correlated events is assigned a unique ID.**


What will a threat actor do to create a back door on a compromised target according to the Cyber Kill Chain model?
-   **Add services and autorun keys.**
-   Collect and exfiltrate data.
-   Obtain an automated tool to deliver the malware payload.
-   Open a two-way communications channel to the CnC infrastructure.


When dealing with a security threat and using the Cyber Kill Chain model, which two approaches can an organization use to help block potential exploitations on a system? (Choose two.)
-   Conduct full malware analysis.
-   Build detections for the behavior of known weaponizers.
-   **Train web developers for securing code.**
-   **Perform regular vulnerability scanning and penetration testing.**
-   Collect email and web logs for forensic reconstruction.


Which type of evidence supports an assertion based on previously obtained evidence?
-   best evidence
-   indirect evidence
-   **corroborating evidence**
-   direct evidence


What are two shared characteristics of the IDS and the IPS? (Choose two.)  
-   Both have minimal impact on network performance.​
-   Both analyze copies of network traffic.
-   Both rely on an additional network device to respond to malicious traffic.
-   **Both are deployed as sensors.**
-   **Both use signatures to detect malicious traffic.**


Which statement describes a typical security policy for a DMZ firewall configuration?
-   **Traffic that originates from the DMZ interface is selectively permitted to the outside interface.**
-   Return traffic from the inside that is associated with traffic originating from the outside is permitted to traverse from the inside interface to the outside interface.
-   Traffic that originates from the inside interface is generally blocked entirely or very selectively permitted to the outside interface.
-   Return traffic from the outside that is associated with traffic originating from the inside is permitted to traverse from the outside interface to the DMZ interface.
-   Traffic that originates from the outside interface is permitted to traverse the firewall to the inside interface with few or no restrictions.


Match the description to the antimalware approach. (Not all options are used.)
- behavior-based
	- analyzing suspisious activites
- signature-based
	- recognizing various characteristics of known malware files
- heuristics-based
	- recognizing general features shared by various types of malware


Which statement describes the anomaly-based intrusion detection approach?
-   It compares the antivirus definition file to a cloud based repository for latest updates.
-   It compares the signatures of incoming traffic to a known intrusion database.
-   **It compares the behavior of a host to an established baseline to identify potential intrusions.**
-   It compares the operations of a host against a well-defined security policy.


What are two problems that can be caused by a large number of ARP request and reply messages? (Choose two.)
-   **All ARP request messages must be processed by all nodes on the local network.**
-   Switches become overloaded because they concentrate all the traffic from the attached subnets.
-   A large number of ARP request and reply messages may slow down the switching process, leading the switch to make many changes in its MAC table.
-   The network may become overloaded because ARP reply messages have a very large payload due to the 48-bit MAC address and 32-bit IP address that they contain.
-   **The ARP request is sent as a broadcast, and will flood the entire subnet.**


Which two types of messages are used in place of ARP for address resolution in IPv6? (Choose two.)
-   broadcast
-   **neighbor advertisement**
-   **neighbor solicitation**
-   echo reply
-   echo request
-   anycast


What three services are offered by FireEye? (Choose three.)
-   subjects all traffic to deep packet inspection analysis
-   **blocks attacks across the web**
-   **identifies and stops latent malware on files**
-   creates firewall rules dynamically
-   deploys incident detection rule sets to network security tools
-   **identifies and stops email threat vectors**


What are the three impact metrics contained in the CVSS 3.0 Base Metric Group? (Choose three.)
-   attack vector
-   **integrity**
-   exploit
-   **availability**
-   **confidentiality**
-   remediation level


A network administrator is creating a network profile to generate a network baseline. What is included in the critical asset address space element?
-   the time between the establishment of a data flow and its termination
-   **the IP addresses or the logical location of essential systems or data**
-   the TCP and UDP daemons and ports that are allowed to be open on the server
-   the list of TCP or UDP processes that are available to accept data


Match the network security testing technique with how it is used to test network security. (Not all options are used.)
- used to determine the possible consequences of successful attacks on the network
	- penetration testing
- used to discover available resources on the network
	- network scanning
- used to find weaknesses and misconfigurations on network systems
	- vulnerability scanning


In an attempt to prevent network attacks, cyber analysts share unique identifiable attributes of known attacks with colleagues. What three types of attributes or indicators of compromise are helpful to share? (Choose three.)
-   system ID of compromised systems
-   **changes made to end system software**
-   **IP addresses of attack servers**
-   BIOS of attacking systems
-   **features of malware files**
-   netbios names of compromised firewalls


Which two protocols are associated with the transport layer? (Choose two.)
-   **UDP**
-   **TCP**
-   ICMP
-   PPP
-   IP


At which OSI layer is a source IP address added to a PDU during the encapsulation process?
-   transport layer
-   data link layer
-   **network layer**
-   application layer


What is the Internet?
-   **It provides connections through interconnected global networks.**
-   It is a network based on Ethernet technology.
-   It provides network access for mobile devices.
-   It is a private network for an organization with LAN and WAN connections.


When a connectionless protocol is in use at a lower layer of the OSI model, how is missing data detected and retransmitted if necessary?
-   **Upper-layer connection-oriented protocols keep track of the data received and can request retransmission from the upper-level protocols on the sending host.**
-   Network layer IP protocols manage the communication sessions if connection-oriented transport services are not available.
-   The best-effort delivery process guarantees that all packets that are sent are received.
-   Connectionless acknowledgements are used to request retransmission.


What is the prefix length notation for the subnet mask 255.255.255.224?
-   /26
-   /25
-   **/27**
-   /28


If the default gateway is configured incorrectly on the host, what is the impact on communications?
-   There is no impact on communications.
-   The host is unable to communicate on the local network.
-   The host can communicate with other hosts on remote networks, but is unable to communicate with hosts on the local network.
-   **The host can communicate with other hosts on the local network, but is unable to communicate with hosts on remote networks.**


Which section of a security policy is used to specify that only authorized individuals should have access to enterprise data?
-   statement of authority
-   campus access policy
-   Internet access policy
-   acceptable use policy
-   **identification and authentication policy**
-   statement of scope


Match the security concept to the description.
- the likelihood of undesireable consequences
	- risk
- a mechanism used to compromise an asset
	- exploit
- a weakness in a system
	- vulnerability
- a potential danger to an asset
	- threat


A flood of packets with invalid source IP addresses requests a connection on the network. The server busily tries to respond, resulting in valid requests being ignored. What type of attack has occurred?
-   **TCP SYN flood**
-   TCP session hijacking
-   UDP flood
-   TCP reset


What kind of ICMP message can be used by threat actors to perform network reconnaissance and scanning attacks?
-   **ICMP unreachable**
-   ICMP mask reply
-   ICMP router discovery
-   ICMP redirects


What are two ICMPv6 messages that are not present in ICMP for IPv4? (Choose two.)
-   **Neighbor Solicitation**
-   Destination Unreachable
-   Time Exceeded
-   **Router Advertisement**
-   Route Redirection
-   Host Confirmation


Which protocol is used by the traceroute command to send and receive echo-requests and echo-replies?
-   **ICMP**
-   Telnet
-   SNMP
-   TCP


Why is DHCP preferred for use on large networks?
-   **It is a more efficient way to manage IP addresses than static address assignment.**
-   Large networks send more requests for domain to IP address resolution than do smaller networks.
-   Hosts on large networks require more IP addressing configuration settings than hosts on small networks.
-   DHCP uses a reliable transport layer protocol.
-   It prevents sharing of files that are copyrighted.


Refer to the exhibit. A cybersecurity analyst is viewing captured packets forwarded on switch S1. Which device has the MAC address d8:cb:8a:5c:d5:8a?  
![[Pasted image 20221206033637.png]]
-   router ISP
-   DNS server
-   web server
-   router DG
-   **PC-A**


Match the steps with the actions that are involved when an internal host with IP address 192.168.10.10 attempts to send a packet to an external server at the IP address 209.165.200.254 across a router R1 that is running dynamic NAT. (Not all options are used.)
- R1 replaces the address 192.168.10.10 with a translated inside global address.
	- Step 5
- R1 checks the NAT configuration to determine if this packet should be translated.
	- Step 2
- R1 selects an available global address from the dynamic address pool.
	- Step 4
- The host sends packets that request a connection to the server at the address 209.165.200.254.
	- Step 1
- If there is no translation entry for this IP address, R1 determines that the source address 192.168.10.10 must be translated.
	- Step 3