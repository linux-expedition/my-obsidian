[IT EXAM ANSWERS](https://itexamanswers.net/modules-26-28-analyzing-security-data-group-exam-answers.html)

Match the Snort rule source to the description.
- older rules created by Sourcefire
	- **GPL**
- rules created and maintained by Cisco Talos
	- **VRT**
- open source rules under BSD license
	- **ET**


What is the purpose for data reduction as it relates to NSM?
-   to make the alert data transmission fast
-   to remove recurring data streams
-   to enhance the secure transmission of alert data
-   **to diminish the quantity of NSM data to be handled**


Which tool is a Security Onion integrated host-based intrusion detection system?
-   Snort
-   ELK
-   Sguil
-   **OSSEC**


Which type of events should be assigned to categories in Sguil?
-   false negative
-   false positive
-   **true positive**
-   true negative


How does an application program interact with the operating system?
-   using processes
-   sending files
-   accessing BIOS or UEFI
-   **making API calls**


Which tool included in the Security Onion provides a visual interface to NSM data?
-   **Squert**
-   Beats
-   Curator
-   OSSEC


Which term is used to describe the process of converting log entries into a common format?
-   systemization
-   standardization
-   classification
-   **normalization**


What is the purpose for data normalization?
-   to reduce the amount of alert data
-   to make the alert data transmission fast
-   to enhance the secure transmission of alert data
-   **to simplify searching for correlated events**


Which technology is a major standard consisting of a pattern of symbols that describe data to be matched in a query?
-   Squert
-   OSSEC
-   Sguil
-   **POSIX**


A threat actor has successfully breached the network firewall without being detected by the IDS system. What condition describes the lack of alert?
-   false positive
-   true positive
-   **false negative**
-   true negative


What are security event logs commonly based on when sourced by traditional firewalls?
-   **5-tuples**
-   static filtering
-   signatures
-   application analysis


A cybersecurity analyst is going to verify security alerts using the Security Onion. Which tool should the analyst visit first?
-   CapME
-   Bro
-   ELK
-   **Sguil**


What is indicated by a Snort signature ID that is below 3464?
-   The SID was created by the Snort community and is maintained in Community Rules.
-   This is a custom signature developed by the organization to address locally observed rules.
-   The SID was created by members of EmergingThreats.
-   **The SID was created by Sourcefire and distributed under a GPL agreement.**


Refer to the exhibit. Which field in the Sguil application window indicates the priority of an event or set of correlated events?
![[Pasted image 20221206011639.png]]
-   Pr
-   AlertID
-   **ST**
-   CNT


A network administrator is trying to download a valid file from an internal server. However, the process triggers an alert on a NMS tool. What condition describes this alert?
-   false negative
-   **false positive**
-   true negative
-   true positive


After a security monitoring tool identifies a malware attachment entering the network, what is the benefit of performing a retrospective analysis?
-   It can identify how the malware originally entered the network.
-   It can calculate the probability of a future incident.
-   It can determine which network host was first affected.
-   **A retrospective analysis can help in tracking the behavior of the malware from the identification point forward.**


A cybersecurity analyst has been called to a crime scene that contains several technology items including a computer. Which technique will be used so that the information found on the computer can be used in court?
-   rootkit
-   Tor
-   log collection
-   **unaltered disk image**


What is the objective the threat actor in establishing a two-way communication channel between the target system and a CnC infrastructure?
-   to launch a buffer overflow attack
-   to steal network bandwidth from the network where the target is located
-   **to allow the threat actor to issue commands to the software that is installed on the target**
-   to send user data stored on the target to the threat actor


What is defined in the SOP of a computer security incident response capability (CSIRC)?
-   **the procedures that are followed during an incident response**
-   the metrics for measuring incident response capabilities
-   the details on how an incident is handled
-   the roadmap for increasing incident response capabilities


Which meta-feature element in the Diamond Model describes information gained by the adversary?
-   **results**
-   methodology
-   direction
-   resources


What two shared sources of information are included within the MITRE ATT&CK framework? (Choose two.)
-   eyewitness evidence from someone who directly observed criminal behavior
-   collection of digital evidence from most volatile evidence to least volatile
-   **mapping the steps in an attack to a matrix of generalized tactics**
-   **attacker tactics, techniques, and procedures**
-   details about the handling of evidence including times, places, and personnel involved


What information is gathered by the CSIRT when determining the scope of a security incident?
-   the amount of time and resources needed to handle an incident
-   the processes used to preserve evidence
-   the strategies and procedures used for incident containment
-   **the networks, systems, and applications affected by an incident**


According to NIST, which step in the digital forensics process involves drawing conclusions from data?
-   examination
-   **analysis**
-   reporting
-   collection


A threat actor collects information from web servers of an organization and searches for employee contact information. The information collected is further used to search personal information on the Internet. To which attack phase do these activities belong according to the Cyber Kill Chain model?
-   weaponization
-   action on objectives
-   exploitation
-   **reconnaissance**


Why would threat actors prefer to use a zero-day attack in the Cyber Kill Chain weaponization phase?
-   to launch a DoS attack toward the target
-   **to avoid detection by the target**
-   to gain faster delivery of the attack on the target
-   to get a free malware package