[IT EXAM ANSWERS](https://itexamanswers.net/ccna-3-v7-modules-13-14-emerging-network-technologies-exam-answers.html)

Which term is used to describe a set of instructions for execution by the configuration management tool Puppet?
-   Pillar
-   **Manifest**
-   Cookbook
-   Playbook  


Which scenario describes the use of a public API?
-   It is used between a company and its business partners.
-   It requires a license.
-   It is used only within an organization.
-   **It can be used with no restrictions.**


Which action takes place in the assurance element of the IBN model?
-   **verification and corrective action**
-   configuring systems
-   integrity checks
-   translation of policies


Refer to the exhibit. Which data format is used to represent the data for network automation applications?
![[Pasted image 20221206015009.png]]
-   HTML
-   YAML
-   XML
-   **JSON**


Which two configuration management tools are developed using Ruby? (Choose two.)
-   Ansible
-   **Puppet**
-   RESTCONF
-   **Chef**
-   SaltStack


Which RESTFul operation corresponds to the HTTP GET method?
-   patch
-   **read**
-   post
-   update


In which situation would a partner API be appropriate?
-   company sales staff accessing internal sales data from their mobile devices
-   someone creating an account on an external app or website by using his or her social media credentials
-   **a vacation service site interacting with hotel databases to display information from all the hotels on its web site**
-   an internet search engine allowing developers to integrate the search engine into their own software applications


Which term is used to describe a set of instructions for execution by the configuration management tool SaltStack?
-   **Pillar**
-   Cookbook
-   Playbook
-   Manifest


Match the term to the RESTful API request **http://www.mapquestapi.com/directions/v2/route?outFormat=json&key=KEY&from=San+Jose,Ca&to=Monterey,Ca** component. (Not all options are used.)
- key=KEY
	- **key**
- outFormat=json
	- **format**
- directions/v2/route
	- **resources**
- http://www.mapquestapi.com
	- **API Server**
- from=San+Jose,Ca&to=Monterey,Ca
	- **parameters**
- outFormat=json&key=KEY&from=San+Jose,Ca&to=Monterey,Ca
	- **query**


What is the function of the key contained in most RESTful APIs?
-   It represents the main query components in the API request.
-   It is the top-level object of the API query.
-   **It is used to authenticate the requesting source.**
-   It is used in the encryption of the message by an API request.


To avoid purchasing new hardware, a company wants to take advantage of idle system resources and consolidate the number of servers while allowing for multiple operating systems on a single hardware platform. What service or technology would support this requirement?
-   software defined networking
-   **virtualization**
-   Cisco ACI
-   dedicated servers


What is the most widely used API for web services?
-   SOAP
-   JSON-RPC
-   XML-RPC
-   **REST**


Which technology virtualizes the network control plane and moves it to a centralized controller?​
-   **SDN**
-   IaaS
-   fog computing
-   cloud computing


What is a difference between the functions of Cloud computing and virtualization?
-   **Cloud computing separates the application from the hardware whereas virtualization separates the OS from the underlying hardware.**
-   Cloud computing requires hypervisor technology whereas virtualization is a fault tolerance technology.
-   Cloud computing provides services on web-based access whereas virtualization provides services on data access through virtualized Internet connections.
-   Cloud computing utilizes data center technology whereas virtualization is not used in data centers.


What are two functions of hypervisors? (Choose two.)
-   to protect the host from malware infection from the virtual machines
-   **to allocate physical system resources to virtual machines**
-   to partition the hard drive to run virtual machines
-   to share the antivirus software across the virtual machines
-   **to manage virtual machines**


What is REST?
-   It is a human readable data structure that is used by applications for storing, transforming, and reading data.
-   **It is an architecture style for designing web service applications.**
-   It is a protocol that allows administrators to manage nodes on an IP network.
-   It is a way to store and interchange data in a structured format.


What is a difference between the XML and HTML data formats?
-   XML encloses data within a pair of tags whereas HTML uses a pair of quotation makes to enclose data.
-   **XML does not use predefined tags whereas HTML does use predefined tags.**
-   XML does not require indentation for each key/value pair but HTML does require indentation.
-   XML formats data in binary whereas HTML formats data in plain text.


How is the YAML data format structure different from JSON?
-   It uses brackets and commas.
-   It uses end tags.
-   **It uses indentations.**
-   It uses hierarchical levels of nesting.


Which cloud model provides services for a specific organization or entity?
-   **a private cloud**
-   a community cloud
-   a public cloud
-   a hybrid cloud


What is a function of the data plane of a network device?
-   sending information to the CPU for processing
-   **forwarding traffic flows**
-   resolving MAC addresses
-   building the routing table


What technology allows users to access data anywhere and at any time?
-   data analytics
-   **Cloud computing**
-   virtualization
-   micromarketing


Which type of Hypervisor is implemented when a user with a laptop running the Mac OS installs a Windows virtual OS instance?
-   virtual machine
-   type 1
-   bare metal
-   **type 2**


How does virtualization help with disaster recovery within a data center?
-   guarantee of power
-   improvement of business practices
-   supply of consistent air flow
-   **support of live migration**


What two benefits are gained when an organization adopts cloud computing and virtualization? (Choose two.)
-   **enables rapid responses to increasing data volume requirements**
-   elimination of vulnerabilities to cyber attacks
-   distributed processing of large data sets in the size of terabytes
-   increases the dependance on onsite IT resources
-   **provides a "pay-as-you-go" model, allowing organizations to treat computing and storage expenses as a utility**


Which is a characteristic of a Type 1 hypervisor?​
-   installed on an existing operating system​
-   **installed directly on a server**​
-   best suited for consumers and not for an enterprise environment  
-   does not require management console software


What pre-populates the FIB on Cisco devices that use CEF to process packets?
-   the DSP
-   the ARP table
-   **the routing table**
-   the adjacency table


What component is considered the brains of the ACI architecture and translates application policies​?
-   **the Application Policy Infrastructure Controller**
-   the Nexus 9000 switch
-   the Application Network Profile endpoints
-   the hypervisor​


Which two layers of the OSI model are associated with SDN network control plane functions that make forwarding decisions? (Choose two.)​
-   Layer 1
-   **Layer 2**
-   **Layer 3**
-   Layer 4
-   Layer 5


Which statement describes the concept of cloud computing?
-   separation of operating system from hardware
-   separation of management plane from control plane
-   separation of control plane from data plane
-   **separation of application from hardware**


A small company is considering moving many of its data center functions to the cloud. What are three advantages of this plan? (Choose three.)
-   **The company does not need to be concerned about how to handle increasing data storage and processing demands with in-house data center equipment.**
-   Cloud services are billed at a fixed fee no matter how much processing and storage are used by the company.
-   **The company can increase processing and storage capacity as needed and then decrease capacity when it is no longer needed.**
-   Cloud services enable the company to own and administer its own servers and storage devices.
-   Single-tenant data centers can easily grow to accommodate increasing data storage requirements.
-   **The company only needs to pay for the amount of processing and storage capacity that it uses.**