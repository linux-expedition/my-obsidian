[It exam answers](https://itexamanswers.net/ccna-3-v7-modules-9-12-optimize-monitor-and-troubleshoot-networks-exam-answers.html)

Match the borderless switched network guideline description to the principle.
- allows intellegent traffic load sharing by using all network resources
	- **flexibility**
- allows seamless network expansion and integrated service enablement on an on-demand basis
	- **modularity**
- facilitates understanding the role of each device at every tier, simplifies deployment, operation, management, and reduces fault domains at every tier
	- **hierarchical**
- satisfies user expectations for keeping the network always on
	- **resiliency**


Match the functions to the corresponding layers.
- **access layer**
	- represents the network edge
	- provides network access to the user
- **distribution layer**
	- implements network access policy
	- establishes layer 3 routing boundaries
- **core layer**
	- provides high-speed backbone connectivity
	- functions as an aggregator for all the campus blocks


A network manager wants to determine the size of the Cisco IOS image file on the networking device. What command should the administrator use on a Cisco router?
-   ntp server 10.10.14.9
-   clock timezone PST -7
-   **show flash:0**
-   lldp enable


What are three functions provided by the syslog service? (Choose three.)
-   to provide traffic analysis
-   to provide statistics on packets that are flowing through a Cisco device
-   **to gather logging information for monitoring and troubleshooting**
-   **to specify the destinations of captured messages**
-   **to select the type of logging information that is captured**
-   to periodically poll agents for data


An administrator wants to replace the configuration file on a Cisco router by loading a new configuration file from a TFTP server. What two things does the administrator need to know before performing this task? (Choose two.)
-   configuration register value
-   **TFTP server IP address**
-   **name of the configuration file that is stored on the TFTP server**
-   name of the configuration file that is currently stored on the router
-   router IP address


What is the function of the MIB element as part of a network management system?
-   to change configurations on SNMP agents
-   to send and retrieve network management information
-   **to store data about a device**
-   to collect data from SNMP agents


Refer to the exhibit. What two types of devices are connected to R1? (Choose two.)
![[Pasted image 20221130131744.png]]
-   hub
-   **switch**
-   **router**
-   repeater
-   Source Route Bridge


Refer to the exhibit. Which of the three Cisco IOS images shown will load into RAM?
![[Pasted image 20221130132100.png]]
-   **The router selects an image depending on the *boot system* command in the configuration.**
-   The router selects the third Cisco IOS image because it contains the advipservicesk9 image.
-   The router selects an image depending on the value of the configuration register.
-   The router selects the second Cisco IOS image because it is the smallest IOS image.
-   The router selects the third Cisco IOS image because it is the most recent IOS image.


What is a basic function of the Cisco Borderless Architecture access layer?
-   aggregates Layer 3 routing boundaries
-   aggregates Layer 2 broadcast domains
-   provides fault isolation
-   **provides access to the user**


Refer to the exhibit. Which devices exist in the failure domain when switch S3 loses power?
![[Pasted image 20221130132248.png]]
-   **PC_3 and AP_2**
-   AP_2 and AP_1
-   PC_3 and PC_2
-   S4 and PC_2
-   S1 and S4


A network designer is considering whether to implement a switch block on the company network. What is the primary advantage of deploying a switch block?
-   **The failure of a switch block will not impact all end users.**
-   This is network application software that prevents the failure of a single network device.
-   A single core router provides all the routing between VLANs.
-   This is a security feature that is available on all new Catalyst switches.


Which action should be taken when planning for redundancy on a hierarchical network design?
-   implement STP portfast between the switches on the network
-   **add alternate physical paths for data to traverse the network**
-   immediately replace a non-functioning module, service or device on a network
-   continually purchase backup equipment for the network


What are two benefits of extending access layer connectivity to users through a wireless medium? (Choose two.)
-   decreased number of critical points of failure
-   increased bandwidth availability
-   **increased flexibility**
-   increased network management options
-   **reduced costs**


Which type of network traffic cannot be managed using congestion avoidance tools?
-   ICMP
-   IP
-   TCP
-   **UDP**


Which queuing algorithm has only a single queue and treats all packets equally?
-   WFQ
-   **FIFO**
-   CBWFQ
-   LLQ


Voice packets are being received in a continuous stream by an IP phone, but because of network congestion the delay between each packet varies and is causing broken conversations. What term describes the cause of this condition?
-   buffering
-   **jitter**
-   queuing
-   latency


When QoS is implemented in a converged network, which two factors can be controlled to improve network performance for real-time traffic? (Choose two.)
-   **jitter**
-   packet routing
-   packet addressing
-   link speed
-   **delay**


Refer to the exhibit. R1 and R3 are connected to each other via the local serial 0/0/0 interface. Why are they not forming an adjacency?
![[Pasted image 20221130132752.png]]
-   The connecting interfaces are configured as passive.
-   **They are in different subnets.**
-   They have different routing processes.
-   They have different router IDs.


Which troubleshooting tool would a network administrator use to check the Layer 2 header of frames that are leaving a particular host?
-   CiscoView
-   knowledge base
-   **protocol analyzer**
-   baselining tool


Refer to the exhibit. Why are routers R1 and R2 not able to establish an OSPF adjacency?​
![[Pasted image 20221130132954.png]]
-   **The serial interfaces are not in the same area.**
-   A backbone router cannot establish an adjacency with an ABR router.
-   The process numbers are not the same in both routers.
-   The router ID values are not the same in both routers.


What is the principle that is applied when a network technician is troubleshooting a network fault by using the divide-and-conquer method?
-   Testing is performed at Layer 7 and at Layer 1, then at Layers 6 and 2, and so on, working towards the middle of the stack until all layers are verified as operational.
-   Once it is verified that a component in a particular layer is functioning properly, testing can then be performed on any other layer.
-   Testing is performed at all layers of the OSI model until a non-functioning component is found.
-   **Once it is verified that components in a particular layer are functioning properly, it can then be assumed that components in the layers below it are also functional.**


Refer to the exhibit. A network administrator has configured OSPFv2 on the two Cisco routers as shown. PC1 is unable to connect to PC2. What should the administrator do first when troubleshooting the OSPFv2 implementation?
![[Pasted image 20221130133115.png]]
-   **Test Layer 3 connectivity between the directly connected routers.**
-   Turn off OSPFv2.
-   Implement the **network 192.168.255.0 0.0.0.3** **area 0** command on router R1.
-   Disconnect the serial link between router R1 and R2.


What are two reasons to create a network baseline? (Choose two.)  
-   **to identify future abnormal network behavior**
-   to determine what kind of equipment to implement
-   to evaluate security vulnerabilities in the network
-   to design a network according to a proper model
-   **to determine if the network can deliver the required policies**
-   to select a routing protocol


As the network administrator you have been asked to implement EtherChannel on the corporate network. What does this configuration consist of?
-   providing redundant links that dynamically block or forward traffic
-   **grouping multiple physical ports to increase bandwidth between two switches**
-   providing redundant devices to allow traffic to flow in the event of device failure
-   grouping two devices to share a virtual IP address


In which step of gathering symptoms does the network engineer determine if the problem is at the core, distribution, or access layer of the network?
-   Determine ownership.
-   Document the symptoms.
-   Gather information.
-   Determine the symptoms.
-   **Narrow the scope.**


What is a definition of a two-tier LAN network design?
-   **distribution and core layers collapsed into one tier, and the access layer on a separate tier**
-   access and distribution layers collapsed into one tier, and the core layer on a separate tier
-   access, distribution, and core layers collapsed into one tier, with a separate backbone layer
-   access and core layers collapsed into one tier, and the distribution layer on a separate tier


The command **ntp server 10.1.1.1** is issued on a router. What impact does this command have?
-   determines which server to send system log files to
-   identifies the server on which to store backup configurations
-   ensures that all logging will have a time stamp associated with it
-   **synchronizes the system clock with the time source with IP address 10.1.1.1**


What is the function of a QoS trust boundary?
-   **A trust boundary identifies which devices trust the marking on packets that enter a network.**
-   A trust boundary identifies the location where traffic cannot be remarked.
-   A trust boundary only allows traffic to enter if it has previously been marked.
-   A trust boundary only allows traffic from trusted endpoints to enter the network.


What is the benefit of deploying Layer 3 QoS marking across an enterprise network?
-   Layer 3 marking can be used to carry non-IP traffic.
-   Layer 3 marking can carry QoS information on switches that are not IP aware.
-   Layer 3 marking can be carried in the 802.1Q fields.
-   **Layer 3 marking can carry the QoS information end-to-end.**


Refer to the exhibit. The network administrator enters these commands into the R1 router:  
  ![[Pasted image 20221130133409.png]]
R1# **copy running-config tftp**  
Address or name of remote host [ ]?  
  
When the router prompts for an address or remote host name, what IP address should the administrator enter at the prompt?

-   192.168.10.2
-   **192.168.11.252**
-   192.168.10.1
-   192.168.11.254
-   192.168.9.254


A computer can access devices on the same network but cannot access devices on other networks. What is the probable cause of this problem?
-   **The computer has an invalid default gateway address.**
-   The cable is not connected properly to the NIC.
-   The computer has an incorrect subnet mask.
-   The computer has an invalid IP address.


What are two approaches to prevent packet loss due to congestion on an interface? (Choose two.)
-   Prevent bursts of traffic.
-   **Increase link capacity.**
-   Decrease buffer space.
-   Disable queuing mechanisms.
-   **Drop lower-priority packets.**


What configuration scenario would offer the most protection to SNMP get and set messages?
-   **SNMPv3 configured with the auth security level**
-   SNMPv1 with out-of-band management in a private subnet
-   SNMPv2 for in-band management with read-write community strings
-   SNMP community strings


What type of traffic is described as traffic that requires at least 30 Kbps of bandwidth?
-   video
-   data
-   **voice**