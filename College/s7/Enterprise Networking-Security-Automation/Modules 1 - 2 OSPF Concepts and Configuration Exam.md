[EXAM ANSWER](https://itexamanswers.net/ccna-3-v7-modules-1-2-ospf-concepts-and-configuration-exam-answers.html)


What command would be used to determine if a routing protocol-initiated relationship had been made with an adjacent router?

-   ping
-   show ip protocols
-   show ip interface brief
-   **show ip ospf neighbor**

Refer to the exhibit. Suppose that routers B, C, and D have a default priority, and router A has a priority 0. Which conclusion can be drawn from the DR/BDR election process?​
![[Pasted image 20221211062837.png]]
-   If a new router with a higher priority is added to this network, it will become the DR.
-   Router A will become the DR and router D will become the BDR.​
-   **If the DR fails, the new DR will be router B.**
-   If the priority of router C is changed to 255, then it will become the DR.


By default, what is the OSPF cost for any link with a bandwidth of 100 Mb/s or greater?
-   100000000
-   100
-   **1**
-   10000


Refer to the exhibit. Which three statements describe the results of the OSPF election process of the topology that is shown in the exhibit? (Choose three.)
![[Pasted image 20221211063134.png]]
-   The router ID on R2 is the loopback interface.
-   R1 will be elected BDR.
-   **R3 will be elected BDR.**
-   **The R4 router ID is 172.16.1.1.**
-   The R4 FastEthernet 0/0 priority is 128.
-   **R2 will be elected DR.**


Refer to the exhibit. A network administrator has configured OSPFv2 on the two Cisco routers but PC1 is unable to connect to PC2. What is the most likely problem?
![[Pasted image 20221211063303.png]]
-   Interface s0/0 has not been activated for OSPFv2 on router R2.
-   Interface Fa0/0 is configured as a passive-interface on router R2.
-   **Interface Fa0/0 has not been activated for OSPFv2 on router R2.**
-   Interface S0/0 is configured as a passive-interface on router R2.


Refer to the exhibit. What is the OSPF cost to reach the router A LAN 172.16.1.0/24 from B?
![[Pasted image 20221211063403.png]]
-   782
-   74
-   128
-   **65**


Refer to the exhibit. On which router or routers would a default route be statically configured in a corporate environment that uses single area OSPF as the routing protocol?
![[Pasted image 20221211063436.png]]
-   ISP and R0-A
-   R0-B and R0-C
-   ISP, R0-A, R0-B, and R0-C
-   R0-A, R0-B, and R0-C
-   **R0-A**
-   ISP


Refer to the exhibit. Which command did an administrator issue to produce this output?
![[Pasted image 20221211063513.jpg]]
-   R1# show ip ospf neighbor
-   **R1# show ip ospf interface serial0/0/1**
-   R1# show ip ospf
-   R1# show ip route ospf


What are the two purposes of an OSPF router ID? (Choose two.)
-   to facilitate the establishment of network convergence
-   to enable the SPF algorithm to determine the lowest cost path to remote networks
-   to facilitate the transition of the OSPF neighbor state to Full
-   **to uniquely identify the router within the OSPF domain**
-   **to facilitate router participation in the election of the designated router**


Refer to the exhibit. If no router ID was manually configured, what would router Branch1 use as its OSPF router ID?
![[i254793v1n2_254793.gif]]
-   10.0.0.1
-   10.1.0.1
-   **192.168.1.100**
-   209.165.201.1


Which command is used to verify that OSPF is enabled and also provides a list of the networks that are being advertised by the network?​
-   show ip route ospf
-   show ip ospf interface
-   show ip interface brief
-   **show ip protocols**


What will an OSPF router prefer to use first as a router ID?
-   the highest active interface that participates in the routing process because of a specifically configured **network** statement
-   **any IP address that is configured using the **router-id** command**
-   the highest active interface IP that is configured on the router
-   a loopback interface that is configured with the highest IP address on the router


Refer to the exhibit. If the switch reboots and all routers have to re-establish OSPF adjacencies, which routers will become the new DR and BDR?
![[i255836v1n1_255836.gif]]
-   **Router R4 will become the DR and router R1 will become the BDR.**
-   Router R2 will become the DR and router R3 will become the BDR.
-   Router R1 will become the DR and router R2 will become the BDR.
-   Router R4 will become the DR and router R3 will become the BDR.


An OSPF router has three directly connected networks; 172.16.0.0/16, 172.16.1.0/16, and 172.16.2.0/16. Which OSPF network command would advertise only the 172.16.1.0 network to neighbors?
-   router(config-router)# network 172.16.1.0 0.0.255.255 area 0
-   router(config-router)# network 172.16.0.0 0.0.15.255 area 0
-   **router(config-router)# network 172.16.1.0 255.255.255.0 area 0**
-   router(config-router)# network 172.16.1.0 0.0.0.0 area 0


What is the recommended Cisco best practice for configuring an OSPF-enabled router so that each router can be easily identified when troubleshooting routing issues?
-   Use the highest IP address assigned to an active interface participating in the routing process.
-   Use a loopback interface configured with the highest IP address on the router.
-   Use the highest active interface IP address that is configured on the router.
-   **Configure a value using the **router-id** command.**


Which step in the link-state routing process is described by a router inserting best paths into the routing table?
-   load balancing equal-cost paths
-   declaring a neighbor to be inaccessible
-   executing the SPF algorithm
-   **choosing the best route**


What is the reason for a network engineer to alter the default reference bandwidth parameter when configuring OSPF?
-   to increase the speed of the link
-   **to more accurately reflect the cost of links greater than 100 Mb/s**
-   to force that specific link to be used in the destination route
-   to enable the link for OSPF routing


Which task has to be performed on Router 1 for it to establish an OSPF adjacency with Router 2?
-   **Change the subnet mask of interface FastEthernet 0/0 to 255.255.255.0.**
-   Add the network 10.0.1.0 0.0.0.255 area 0 command to the OSPF process.
-   Remove the passive interface command from interface FastEthernet 0/0.
-   Issue the **clear ip ospf process** command.


What is the format of the router ID on an OSPF-enabled router?
-   a unique router host name that is configured on the router
-   **a 32-bit number formatted like an IPv4 address**
-   an 8-bit number with a decimal value between 0 and 255
-   a character string with no space
-   a unique phrase with no more than 16 characters


In an OSPFv2 configuration, what is the effect of entering the command **network 192.168.1.1 0.0.0.0 area 0** ?
-   It changes the router ID of the router to 192.168.1.1.
-   It enables OSPF on all interfaces on the router.
-   **It tells the router which interface to turn on for the OSPF routing process.**
-   It allows all 192.168.1.0 networks to be advertised.


After modifying the router ID on an OSPF router, what is the preferred method to make the new router ID effective?
-   HQ# **copy running-config startup-config**
-   HQ# **clear ip ospf process** -----------------------------------------------------------
-   HQ# **clear ip route ***
-   HQ# **resume**


