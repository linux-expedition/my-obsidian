###### Monday
07.50 - 09.30 Algoritma **S1**

08.00 - 09.30 Tionghoa **S3**
09.30 - 12.00 Struktur Data **S3**
14.40 - 16.20 P.Struktur Data **S3**

07.50 - 10.20 Rekayasa Perangkat Lunak **S5** Saccarum officinarum
13.00 - 15.30 Interaksi Manusia dan Komputer **S5** Saccarum officinarum

07.00 - 09.30 Indonesia **S3E**
10.20 - 12.00 E-Commerce **S7E**


###### Tuesday
07.50 - 09.30 Kalkulus **S1**
13.00 - 14.40 Probabilitas & Statistik **S1**
14.40 - 17.10 Pengantar Teknologi Informasi **S1**

07.50 - 10.20 Sistem Basis Data **S3**
10.20 - 12.00 P.Sistem Basis Data **S3**
15.30 - 17.10 Metode Numerik **S3**

07.50 - 09.30 Data Mining **S7E** Phycocyanin

13.00 - 14.30 Tionghoa **S3E**
15.30 - 17.00 Tionghoa **S5E**
10.20 - 12.00 Perilaku Organisasi **S7E**



###### Wednesday
17.50 - 09.30 Kalkulus **S1**
13.00 - 15.30 Logika Digital **S1**
15.30 - 17.10 P.Logika Digital **S1**
09.30 - 11.10 Probabilitas & Statistik **S1**

13.00 - 15.30 Pancasila **S3**

09.30 - 12.00 Sistem Berbasis Pengetahuan **S5** Elais guinensis
13.00 - 15.30 Pengolahan Citra Digital **S5E** Chlorophyll

13.00 - 15.30 Pemrograman Desktop **S7E**
07.50 - 09.30 Etika Profesi **S7E**



###### Thursday
07.50 - 09.30 Komunikasi Data **S3**
09.30 - 11.10 P.Komunikasi Data **S3**
14.40 - 17.10 Sistem Operasi **S3**

07.50 - 10.20 Riset Operasi **S5E** Malus domestica

07.50 - 09.30 Kapita Selekta **S7** Phycocyanin
10.20 - 13.00 Jaringan Komputer Lanjut **S5** Sizygium aromaticum
13.30 - 15.30 Interfacing **S5** Theobroma cacao

10.30 - 12.00 Tionghoa **S3E**
13.00 - 14.40 Basic Electronic Circuit **S7E**


###### Friday
08.00 - 09.30 English **S1**
09.30 - 11.10 Tionghoa **S1**

07.50 - 10.20 Pemrograman Web **S5** Lab siputri
13.00 - 15.30 Keamanan Komputer **S5** Sauropus androgynus
15.30 - 17.10 P.Pemrograman Web **S5** lab siputri

10.30 - 12.00 Tionghoa **S5E**
07.50 - 10.20 Pemrograman Robotika **S7E**







Permisi, ijin share survey
Dalam rangka mengetahui lebih dalam mengenai ilmu human computer interaction, berkenankah untuk meluangkan waktu 2 menit untuk mengisi survey di link berikut?
http://bit.ly/survHCI
Data yang diberikan akan sangat membantu di pembelajan kami. 
Mohon maaf waktunya dan terima kasih banyak





Dana