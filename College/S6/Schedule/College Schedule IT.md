
Senin  
07.50 - 10.20 Arsitektur & Organisasi Komputer s2 
10.20 - 12.00 Bahasa Tionghoa 2 s2  
13.00 - 15.30 Pendidikan Agama s2

07.50 - 10.20 Basis Data Lanjut s4
-13.00 - 15.30 Fuzzy Logic s4
15.30 - 17.10 Dasar Komputer Grafik s4

07.50 - 09.30 Metodologi Penelitian s6
13.00 - 14.40 Bahasa Tionghoa 6 s6



Selasa  
07.50 - 09.30 Bahasa Inggris 2 s2
13.00 - 15.30 Pemrograman s2
15.30 - 17.10 Praktikum Pemrograman s2

07.50 - 09.30 Bahasa Tionghoa 4 s4
09.30 - 12.00 Kriptografi s4  

-07.50 - 10.20 Machine Learning s6  
10.20 - 12.00 Cloud Computing s6  

13.00 - 14.40 Bahasa Tionghoa 4 s6  
13.00 - 15.30 Teknik Optimasi s6  





Rabu  
-07.50 - 10.20 Aljabar Linear  S2
10.20 - 12.00 Bahasa Tionghoa 2 s2  
-13.00 - 15.30 Kecerdasan Buatan s2  

07.50 - 10.20 Pemrograman Berorientasi Objek s4  
10.20 - 12.00 Praktikum Pemrograman Berorientasi Objek s4  
10.20 - 12.00 Bahasa Tionghoa 4 s4
13.00 - 15.30 Pendidikan Kewarganegaraan s4
15.30 - 17.10 Otomata s4

13.00 - 15.30 Administrasi Jaringan s6
13.00 - 14.40 Bahasa Tionghoa 6 s6
07.50 - 09.30 Deep Learning s6
09.30 - 12.00 Scientific Computing s6




Kamis  
07.50 - 09.30 Bahasa Inggris 2 s2   
09.30 - 12.00 Matematika Diskrit s2  

07.50 - 09.30 Bahasa Tionghoa 4 s6  
07.50 - 09.30 Technopreneurship s6
10.20 - 12.00 Internet Programming s6  
13.00 - 15.30 Pemrograman Mobile s6 

09.30 - 12.00 Pemodelan Simulasi s4  
13.00 - 14.40 Jaringan Komputer s4
14.40 - 16.20 Praktikum Jaringan Komputer s4


  
Jumat  
07.50 - 09.30 Bahasa Tionghoa 4 s4
07.50 - 10.20 Embedded System s6
07.50 - 09.30 Bahasa Tionghoa 2 s2

13.00 - 14.40 Bahasa Tionghoa 4 s4
13.00 - 15.30 Internet of Things s6

Jumat 13.00 2 - wajib & pilihan

- feedback
Senin
Selasa
Rabu
Kamis
Jumat



Selamat malam 

Sehubungan dengan adanya kebutuhan data untuk peningkatan kualitas universitas ma chung, khususnya prodi IF. Kami ingin mengundang teman-teman angkatan 2017 (yang masih aktif) dan 2018 untuk menghadiri forum evaluasi yang akan diselenggarakan pada

Hari Selasa
Jam 13.00 - 14.00 WIB
Tempat Online (Teams STIF channel General)

Demikian yang bisa kami sampaikan. Terima kasih atas perhatian dan kerja samanya