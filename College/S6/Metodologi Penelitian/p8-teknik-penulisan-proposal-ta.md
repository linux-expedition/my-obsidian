# Sistematika penulisan
## Judul
Cover: Judul serta identitas
Judul:
	- Harus dibuat singkat dan jelas (5-15 kata)
	- Menunjukan bidang permasalahan yang akan dibahas (tidak menimbulkan kemungkinan penafsiran yang beraneka ragam)
	- Memuat variable (-variable) yang diteliti
### Contoh
- Identifikasi daun tanaman obat dengan **convolution neural network** berbasis android
- implementasi **algoritma genetika** untuk aransemen nada sopran, alto, tenor, dan basis
- sistem pakar mengidentifikasi hama dan penyakit pada tanaman kentang dengan pendekatan bayesian network berbasis web
- implementasi background substraction untuk deteksi objek bergereak pada aplikasi monitoring ruangan menggunakan raspberry pi
- rancang bangun aplikasi catatan kuliah berbasis android dengan fitur pengenalan karakter


## Abstraksi
Intisari dari proposal yang terdiri dari maksimal 200 kata dan diketik dalam 1 spasi.
Abstraksi mencakup:
	1. Sekilas mengenai bidang topik yang dibahas. Masalah utama yang akan diteliti dan ruang liungkupnya.
	2. Intisari dari penelitian yang akan dilakukan dan mengandung metode / teknik / alat bantu yang digunakan.
	3. Hasil yang diharapkan

# Isi Proposal
## Latar Belakang
- Berisi dasar-dasar pemikiran yang menjadi inspirasi pengangkatan topik yang akan dikaji.
- Menjelaskan tentang **SEBAB** dipilihnya suatu topik/judul penelitian.
- Berawal dari mengemukakan suatu fakta, masalah dalam kenyataan dan fenomena dalam ilmu pengetahuan, temuan penelitian terdahulu, karya ilmiah yang dapat dipertanggungjawabkan keabsahannya.
- Merupakan justifikasi mengenai pentingnya judul penelitian yang dipilih tersebut atau mengapa penelitian dengan judul tersebut perlu dilakukan.
- Relevan dengan perumusan masalah
- Menjadi landasan sebelum memasuki perumusan masalah.
- **BUKAN** asumsi / pendapat pribadi tanpa pertanggungjawabkan secara ilmiah.
- Jika latar belakang berasal dari suatu tugas akhir atau penelitian yang sudah ada, maka perlu dicantumkan secara singkat apa yang dilakukan pada penelitian tersebut dan bandingkan dengan yang akan dilakukan pada tugas akhir.

### Steps
1. Buat poin - poin
2. Poin" menjadi deskriptif / jabarkan
3. paragraf per poin

### Contoh
Judul: implementasi algoritma genetika untuk aransemen nada sopran, alto, tenor, basis
latar belakang:
1. harmoni atau keselarasan nada merupakan elemen utama dalam menghasilkan sebuah karya musik
2. pembagian suara tersebut perlu diperhatikan dalam menciptakan harmonisasi nada yang baik
3. tidak semua orang mampu meng aransemen
4. aransemen nada punya banyak varias, jadi mungkin pake algoritma pencarian dgn teknik optimasi
5. algoritma genetika dapat menjadi salah satu pilihan dalam melakukan proses aransemen karena algoritma ini menghasilkan solusi optimal (wulandari, 2007)

## Identifikasi masalah
~ sebagai bagian dari proses penelitian dapat dipahami sebagai upaya mendefinisikan problem dan membuat definis tersebut dapat diukur (measurable) sebagai langkah awal penelitian
> Penilitian harus bisa diukur. Dari ini dapat terlihat sejauh mana permasalahan itu dapat diukur
### Contoh









#### Key Terms
Tugas Akhir
Karya ilmiah yang disusun oleh mahasiswa berdasarkan
hasil penelitian suatu masalah yang dilakukan secara seksama
dengan bimbingan dosen pembimbing

Proposal Tugas Akhir
Usulan yang berisi g
ambaran umum yang bisa dijadikan sebagai **kontrak** untuk menyusun Tugas Akhir