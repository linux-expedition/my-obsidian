- [gas sensor](https://robu.in/mq-series-gas-sensor/)
- [Assessing the accuracy of commercially available gas sensors for the measurement of ambient ozone and nitrogen dioxide](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6324576/)
- [UNDERSTANDING A GAS SENSOR](https://jayconsystems.com/blog/understanding-a-gas-sensor)
- [Detection of Gas Leaks Using The MQ-2 Gas Sensor on the Autonomous Mobile Sensor](https://ieeexplore.ieee.org/document/8949597)
- [A Mobile Gas Detector with an Arduino Microcontroller](https://www.researchgate.net/publication/281410457_A_Mobile_Gas_Detector_with_an_Arduino_Microcontroller)
- [Design and Development of Gas Leakage Monitoring System using Arduino and ZigBee](https://journal.portalgaruda.org/index.php/EECSI/article/view/404)
- [Perangkat Sistem Pengukuran Konsentrasi Gas Metana (Ch4) pada Biogas dari Hasil Fermentasi Enceng Gondok (Eichornia Crassipes) Berbasis Sensor Tgs 2611](https://www.neliti.com/id/publications/172270/perangkat-sistem-pengukuran-konsentrasi-gas-metana-ch4-pada-biogas-dari-hasil-fe)
- [Pengukuran Gas Metana (CH4) dan Karbondioksida (CO2) yang Dihasilkan oleh Sedimen Danau Situ Gunung, Sukabumi Jawa Barat pada Skala Laboratorium](http://repository.ut.ac.id/2519/)
- [Sistem Pengukuran Konsentrasi Gas Metana Dengan Sensor Tgs2611](https://openlibrarypublications.telkomuniversity.ac.id/index.php/engineering/article/view/9783)
- [PERANGKAT SISTEM PENGUKURAN KONSENTRASI GAS METANA (CH4) PADA BIOGAS DARI HASIL FERMENTASI ENCENG GONDOK (EICHORNIA CRASSIPES) BERBASIS SENSOR TGS 2611](https://jurnal.fkip.uns.ac.id/index.php/prosfis1/article/view/3737)
- [CO2 Gas Concentration Defined](https://www.co2meter.com/blogs/news/15164297-co2-gas-concentration-defined)

reaktor bio gas, macam / jenis, permasalahan, 

Reaktor biogas adalah sebuah alat yang berfungsi untuk mengolah limbah organik menjadi metana (C4)