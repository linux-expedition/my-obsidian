# Data kualitatif
> Tidak ada angkanya, tidak bisa kita nilai
- data mentah, berwujud uraian, kutipan atau dokumentasi
- bersifat panjang lebar
# Data kuantitatif
- membakukan pengalaman responden ke dalam kategori baku peneliti.
- Sistematis
- making sense of number
# Test
- panjang lintasan jalan tol X adalah 12,8 km (kuantitatif)
- Suhu badan penderita penyakit demam berdarah itu 41 C (kuanti)
- Bunga A lebih harum dari bunga B (kuali)
- Air di sumur A lebih jernih dari air di sumur B (kuali)
> jernih itu berapa angkanya
- Pegawai di perusahaan X terdiri atas 160 laki-laki & 70 perempuan (kuanti)
- Jeruk A lebih manis dari jeruk B (kuali)
- tumbuhan A lebih cepat tumbuh dari tumbuhan B (kuali)
> lebih cepatnya berapa? misal 2 kali lebih cepat, maka jadi kuanti

# Proses Analisis Data Kuantitatif
Menggunakan:
- Deskriptif
- Inferensial (parametris dan non-parametris)

## Deskriptif
- Menggambarkan data yang telah terkumpul.
- Tidak bertujuan untuk menyimpulkan atau menggeneralisasi
- Mengklasifikasikan suatu data variabel berdasarkan kelompoknya masing-masing dari semula belum teratur dan mudah diinterpretasikan maksudnya oleh orang yang membutuhkan informasi tentang keadaan variable tersebut
- Statistik deskriptif dilakukan terhadap populasi (bukan sample tapi keseluruhan)

## Mengubah ke statistik deskriptif
> Data mentah lalu ada mean, median, sum. Tidak berarti apa-apa. 
> Oleh karena itu, diolah agar mempunyai arti

misal pada penilaian mata kuliah
Data mentah, diolah dengan memberi presentase per grade
Sehingga tahu matakuliah sulit jika grade tinggi dan presentase dikit yang lulus

## Istilah - istilah dasar pada statistik deskriptif
- Sum
	Total keseluruhan
- Mean
	Rata-Rata
	> Tidak menggambarkan apapun.
- Median
	Nilai tengah ketika diurutkan dari kecil ke besar
- Range
- Variance (untuk menghitung standard deviation)
	Ukuran variasi - atau seberapa besar variasi data dari rata-rata?
	Jika nilai besar, maka datanya bervariasi
	Rumus
		$\sum_{} (x+M)^2/(n-1)$
		Selisih data ke-1 dgn rata-rata -> dikuadratkan
		ditotal, dan dibagi n atau n-1
	![[p9-variancepng.png]]
- Standard Deviation
	Seberapa besar data menyimpang dari median
	Simpangan terhadap data populasi
	Akar dari varian
	Contoh berikut tinggi karena, kisaran 200-600 terdapat 147 dari 400 -> 253-547
	ada anjing yang tinngi dan rendah
	![[p9-standard-deviasi.png]]


## Statistik deskriptif vs inferensial
- Deskriptif: memaparkan data dalam bentuk tertentu agar mudah diinterpretasikan.
- Inferensial: upaya untuk mengadakan penarikan kesimpulan

## Statistik inferensial
- Bertujuan untuk menghasilkan suatu temuan yang dapat digeneralisasikan secara lebih luas ke dalam wilayah populasi.
- Dibagi menjadi dua bagian:
	- Korelasional (mencari hubungan antara 2 buah variabel)
	- Komparasi dan / atau eksperimen

## 1. Korelasional
- Ada 2 buah variable: 
	bebas  (tidak terpengaruh variable lain)
	& terikat (dipengaruhi variable lain)
- Dipergunakan untuk melihat ada tidaknya hubungan antara dua variable bebas (X) dan variable terikat (Y)
- Contoh penelitian korelasional:
	- Hubungan antara penghasilan orang tua dan motivasi belajar anak.
	- Pengaruh tayangan media televisi terhadap minat belajar anak.
	- Hubungan antara nilai KB dengan nilai UAS.

> Akan dapat 1 angka. Menunjukan seberapa kuat hubungan antara 2 variable
- Bernilai positif jika nilai bariable naik secara bersamaan
- Bernilai negatif jika pada variable turun secara bersamaan
![[p10-ex-correlation.png]]

## Ex Correlation 
![[p10-slide-35.png]]
![[p10-slide-36.png]] 
![[p10-slide-37.png]]
a = suhu - rata" suhu
b = penjualan - rata" penjualan
0.9575 -> mendekati 1 dan positif = berkorelasi positif


## 2. Komparasi
- Membandingkan kondisi antara 2 kelompok atau lebih.
- Hipotesis komparatif:
	- Perbedaan prestasi belajar mahasiswa antara mahasiswa dengan metode X dengan metode Y
		- Variable bebas: metode X dan metode Y
		- Variable terikat: prestasi belajar
	- Apakah orang usia lanjut berolahraga lebih sering dibandingkan dengan anak muda?
		- Variable bebas:
		- Variable terikat:
### Metode student t-test
- Statistik inferensial selalu berhubungan dengan hipotesis nihil **(H0)**
- H0 - "Apakah ada perbedaan yang signifikan ...."
- t-test akan menguji apakah H0 diterima atau ditolak
- Contoh:
	- H0 - Tidak ada perbedaan antara ...
	- H1 - Ada perbedaan besar antara ...
#### Contoh 1
Membandingkan mean sample terhadap populasi
![[p10-slide-41.png]]
![[p10-slide-42.png]]
![[p10-slide-43.png]]
![[p10-slide-44.png]]
- t < -2.093 = menolak H0 -> memiliki IQ 
- -2.093 < t < 2.093 = menerima H0 -> tidak jauh berbeda
![[p10-slide-45.png]]

#### Contoh 2
Membandingkan 2 sample mean
![[p10-slide-46.png]]
![[p10-slide-47.png]]
