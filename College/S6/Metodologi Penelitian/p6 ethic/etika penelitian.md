

Rekan kerja yang baik adalah seseorang yang selalu menghargai kerja keras sesama rekan kerjanya. Karena dengan menghargai, secara tidak langsung

Dalam berinteraksi dengan rekan kerja, diharuskan untuk dapat saling menghormati satu sama lainnya. Sebagaimana yang disebutkan di MU academic and professional employee code of ethic https://www.muni.cz/en/about-us/official-notice-board/mu-academic-and-professional-employee-code-of-ethics. Dengan menghormati sesama rekan kerja, secara tidak langsung kita juga mengakui secara penuh bahwa individu tersebut terjaga dalam pelanggaran hak asasi manusia, bebas akan paksaan. Menghargai rekan kerja juga termasuk menghargai semua hak dan martabatnya. Karena dengan menghargai rekan kerja juga berarti mengakuinya memiliki kapasitas yang diperlukan dalam memilih keputusan. Begitu juga dengan pengakuan dan akreditasi bisa disebut dalam menghargai sesorang.












Selamat pagi mahasiswa sekalian. Kuliah kita pagi ini tidak dengan metode tatap muka via meeting melainkan diskusi via chat. Topik yang akan kita bahas adalah Etika Akademik, khususnya yang berkaitan dengan etika dalam penelitian. Perhatikan beberapa poin terkait dengan etika dalam penelitian berikut. Selanjutnya saya akan membuka conversation untuk setiap poin dan tugas Anda adalah merespon dengan mereply setiap pertanyaan dari saya. Kehadiran hari ini dihitung berdasarkan kelengkapan reply Anda. Dalam memberikan jawaban silakan dapat didukung dengan mencari literatur tambahan dengan browsing secara mandiri. Tujuan akhir kita adalah Anda memahami hal-hal yang boleh dan tidak boleh, yang sebaiknya dilakukan dan yang sebaiknya dihindari selama melakukan penelitian Tugas Akhir. Jika semua mahasiswa sudah mereply, saya akan memberikan pembahasan di setiap poin.






**Poin 1 : Kejujuran**. Uraikan pendapat Anda mengenai konsep kejujuran dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Menurut saya kejujuran merupakan hal penting dalam sebuah penelitian. Kejujuran tersebut bisa dimulai dengan tidak sekedar copy paste tetapi melainkan mencantum daftar pustaka dari pengetahuan yang digunakan atau materi. Memang akan terlalu penuh dengan daftar pustaka apabila setiap pengetahuan selalu ada rujukannya. Tapi justru hal tersebut adalah contoh penulisan penelitian yang baik. Apalagi jika pengetahuan yang disebutkan masih terbilang baru atau tidak diketahui oleh masyarakat umum, maka wajib menambahkan rujukan. Apapun jika tidak jujur dalam segi sumber pengetahuan, pendasaran, paramater, data set atau bahkan hasilnya. Lalu lolos terpublikasi. Maka, bisa penelitian tersebut tidak akan tahan lama. Jika sumber yang sama ditemukan bisa tercap duiplikasi dan jika hasil nya yang tidak jujur akan runtuh atau bahkan hilang karena hasil akhir tidak konsisten atau tidak sesuai dengan yang disebut.
Contohnya: memberikan rujukan terhadap quote atau bahan(algoritma, alat, dll) yang diberikan dalam penelitian

**Poin 2 : Objektifitas**. Uraikan pendapat Anda mengenai konsep Objektif dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Tanpa adanya objektifitas dalam sebuah penelitian, dapat menghasilkan hasil yang tidak akurat dan tidak konsisten. Karena keobjektifan dalam sebuah penelitian, dapat membantu pemahaman para pembaca bahkan membantu penelitian mereka juga. Sehingga perlu untuk selalu menerapkan sikap objektif meski hasilnya sesuai ataupun tidak sesuai harapan.

**Poin 3 : Integritas**. Uraikan pendapat Anda mengenai konsep integritas dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
https://embassy.science/wiki/Resource:Fd9bacbe-8ea2-47eb-bdf0-8833988c0907#:~:text=Why%20is%20this%20important%3F,that%20may%20impact%20people's%20lives%22.
research integrity is vital because it creates trust, and trust is at the heart of the research process.
integritas memang memiliki kemiripan dengan kepercayaan. Namun, menurut saya integritas dapat membawa dampak apakah penelitian akan berdampak pada masyarakat atau tidak. Seberapa cepat dibaca atau bahkan diterapkan juga tergantung seberapa tinggi integritas yang dimiliki. Karena kemungkinan terburuk atas turunnya integritas adalah tidak sampai pada masyarakat atau bahkan dilarang oleh nasional dan internasional research community. Sebagaimana yang terdapat pada artikel https://phys.org/news/2015-12-integritywhat-important.html
Contohnya: Tidak ditemukan adanya duplikasi atau pemalsuan dalam penelitian yang akan berdampak pada tingkat kepercayaan antar peneliti

**Poin 4 dan 5 : Ketelitian dan Keterbukaan**. Kita bahas kedua poin ini bersama-sama karena saling terkait. Uraikan pendapat Anda mengenai konsep teliti dan terbuka dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Detail dan spesifik merupakan hal penting dalam penulisan penelitian tugas akhir. Di mana setiap paramater, tempat, waktu, jumlah, atau bahkan kondisi akan mempengaruhi hasil dari suatu penelitian. Oleh karena itu, dapat dipastikan bahwa setiap penelitian akan selalu bersifat unik atau pasti ada perbedaan dari penelitian yang satu dengan yang lainnya.
Sehingga apabila sebuah penelitian bersifat tertutup dan berbayar, maka banyak orang yang kesulitan dalam mengaksesnya. Hal tersebut juga menyulitkan orang-orang yang memiliki tujuan merevisi, debunk, atau mempelajarinya. Lain halnya dengan wikipedia. Di mana justru banyak orang yang merujuk ke website tersebut dikarenakan daftar pustaka yang digunakan wikipedia selalu penelitian yang terbuka.



**Poin 6 : Penghargaan Terhadap HAKI**. Uraikan pendapat Anda mengenai konsep penghargaan terhadap HAKI dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!. Perhatikan beberapa dokumen berikut untuk mendukung jawaban Anda.
Artinya pemilik hak cipta haruslah mendapatkan penghargaan. Di karenakan kekayaan hak cipta, paten, atau merek, juga merupakan kekayaan yang sejenis layaknya ekonomi sebuah bangsa. Oleh karena itu, adanya peraturan hak dan undang-undang begitu juga dengan pasal-pasalnya mengenai hukum dan tindak pidana. Hak cipta juga akan mendapatkan kehormatan di mana meskipun pencipta tersebut telah meninggal, karyanya akan tetap tercantum nama penciptanya.


**Poin 7 : Penghargaan Terhadap Responden**. Uraikan pendapat Anda mengenai konsep penghargaan terhadap responden dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Keberhasilan sebuah penelitian tidak serta merta hanya disebabkan oleh penciptanya. Melainkan segala hal yang terlibat dalam proses pembuatan penelitian tersebut. Salah satunya adalah dalam penelitian yang membutuhkan pendekatan empiris, biasanya membutuhkan responden. Sehingga, untuk penghargaan dari responden tersebut haruslah seorang responden dijaga kehormatan, keselamatan, dan juga anonimitynya. Karena juga berperan penting dalam keberhasilan penilitan.

**Poin 8 : Penghargaan Terhadap Rekan Kerja**. Uraikan pendapat Anda mengenai konsep penghargaan terhadap rekan kerja dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
-

**Poin 9 : Tanggung Jawab Sosial**. Uraikan pendapat Anda mengenai konsep tanggungjawab sosial dalam kaitannya dengan penelitian Tugas Akhir, hal ini terkait dampak dari teknologi yang dikembangkan terhadap kehidupan masyarakat. Fokus di bidang IT saja. berikan satu contoh!
Penelitian haruslah sebuah solusi untuk menyelesaikan masalah. Masalah ini dimana dengan tujuan memberikan kebermanfaatan terhadap kemajuan masyarakat. Sehingga, tidak diperbolehkan seorang peneliti melakukan penelitan untuk kehancuran masyarakat. Memang banyak ditemui tentang penyalagunaan penelitian. Namun, selama niat, tujuan, dan usaha peneliti tersebut digunakan untuk kebaikan masyarakat. Menurut saya sudah tidak apa-apa.

**Poin 10 : Menghindari diskriminasi**. Uraikan pendapat Anda mengenai konsep diskriminasi dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Untuk memberikan penelitian yang baik, diperlukan juga seorang peneliti untuk menghindari diskriminasi dari proses sampai hasil. Karena dalam penelitian haruslah objektif dalam semua proses yang dilakukan. Dari asal diskriminasi tersebut bisa ditemui campur konflik, politik, atau ego di mana semua hal tersebut harus dihindari dalam rangka didapatkan hasil yang murni akan data yang didapat. Sehingga tidak adanya pengklasifikasian data yang tidak sesuai. Seperti misalnya warna kulit tertentu mengindikasikan tingkat kecerdasan seseorang.


**Poin 11 : Kompetensi**. Uraikan pendapat Anda mengenai konsep kompetensi dalam kaitannya dengan penelitian Tugas Akhir, berikan satu contoh!
Sebuah penelitian haruslah dilakukan dengan orang yang memiliki kapasitas yang sesuai dengan penelitian yang dilakukan. Sehingga, hasil yang didapat dapat dipertanggung jawabkan. Contohnya orang dengan latar belakang teknik informatika dan ahli di bidangnya melakukan penelitian tentang teknologi website yang baik untuk digunakan.


**Poin 12 : Legalitas**. Uraikan pendapat Anda mengenai konsep legalitas dalam kaitannya dengan penelitian Tugas Akhir. Hal ini berkenaan dengan proses administrasi yang diterapkan di kampus utamanya pada Prodi Teknik Informatika dan Fakultas Sains dan Teknologi. Berikan satu contoh!
Tentu sebuah penelitian haruslah legal. Legal yang dimaksud bisa berupa tentang judul, experimen, etika, publikasi, atau bahkan regulasi. Misalnya, dalam mensitasi haruslah mensitasi semua sumber yang telah digunakan agar tidak mendapatkan sanksi dari peraturan yang telah berlaku.

**Poin 13 (terakhir) : Mengutamakan Keselamatan**. Uraikan pendapat Anda mengenai konsep mengutamakan keselamatan dalam kaitannya dengan penelitian Tugas Akhir. Anda dapat mengkaitkan juga dengan kondisi pandemi saat ini. Berikan satu 
Penelitian tugas akhir memang haruslah selalu tetap mengutamakan keselamatan. Karena kecil besarnya persentase kemungkinan resiko yang terjadi, memiliki biaya yang jauh lebih mahal daripada terjaminnya keselamatan rohani dan jasmani.
Contohnya: Misalkan saja adanya berita korban jiwa tersambar petir karena penelitian yang dilakukan tidak menerapkan safety first, atau juga dari berita berikut https://www.sciencealert.com/the-chilling-story-of-the-demon-core-and-the-scientists-who-became-its-victims

