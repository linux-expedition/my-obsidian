>Invention/ kreasi merupakan tingkat pencapaian tertinggi dari proses kognitif seseorang, perwujudan dari sesuatu yang baru yang belum pernah diketahui orang. Penemuan dapat berupa menemukan hukum, dalil, teorema, aksioma, teori, formula, algoritma, metode, model, SW, HW, atau sistem. 

>Contoh (bila belum pernah ditemukan): Sistem Rekomendasi Promosi Produk Berdasarkan Segmentasi Konsumen dengan Pendekatan Fuzzy C-Means dan Apriori, Alat Bantu Reverse Engineering untuk Observasi Java Script, Sistem Evaluasi Adaptif Berbasis Item Response Theory, dan sebagainya.


