>Reverse Engineering Research: merupakan upaya abstraksi dari produk, sistem, atau prototipe yang sudah ada menjadi blue print, formula, atau model, atau pada tahapan-tahapan pendek rekayasa, misal dari bentuk rancangan ke bentuk rencana saja.

>Penelitian dilakukan mulai dari abstraksi yang lebih rendah menuju ke setingkat atau beberapa tingkat lebih tinggi untuk megeksplorasi suatu sistem atau produk yang sudah ada (explanatory research).
