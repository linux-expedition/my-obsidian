[[#Footnote]] [[lecture Metodologi Penelitian]] for navigation

### Jenjang Penelitian
- [[Pemahaman]]
- [[Penerapan]]
- [[Pemutakhiran]]
- [[Penemuan]]

### Paradigma Penelitian
- [[Teori]]
- [[Pemodelan]]
- [[Produk-Sistem]]

### Bentuk Penelitian
- [[Forward Engineering]]
- [[Reverse Engineering]]
- [[Re-engineering]]

### Footnote
---
Hello

-----
