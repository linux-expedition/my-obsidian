>Merupakan pendekatan yang berlandasakan pada metode perancangan atau eksperimen 
>
>Hasilnya adalah model, prediksi, metode atau prototipe 
>
>Contoh : pembuatan model pendugaan pertumbuhan dan produksi jati cepat tumbuh (menggunakan simulasi)

>Hipotesis -> Kerangka Teoritis -> Pembuatan -> Rancangan Eksperimen -> Pengujian dan Pengumpulan Data -> Analisis Hasil