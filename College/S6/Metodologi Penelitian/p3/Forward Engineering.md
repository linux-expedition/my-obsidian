>Forward Engineering Research: yang dilakukan mulai dari identifikasi masalah, pengumpulan data, penyusunan model, pengujian model, pembangunan, evaluasi, dan validasi.

>Penelitian dilakukan mulai dari abstraksi yang lebih tinggi menuju ke setingkat atau beberapa tingkat lebih rendah, sehingga dapat digunakan untuk menguji teori/ model/ formula (confirmatory research).

