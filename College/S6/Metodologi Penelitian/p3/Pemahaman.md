>Merupakan awal proses penelitian. Peneliti perlu memahami Current State of The Art terkait hukum, dalil, teorema, aksioma, teori, formula, algoritma, metode, model, SW, HW, atau sistem yang menjadi fokus penelitiannya. 

>Contoh proses pemahaman: menginterpretasikan suatu metode baru, mengklasifikasikan model- model asosiasi, membandingkan algoritma- algoritma optimasi, menjelaskan perilaku model transportasi, dan sebagainya.

