>Merupakan pemanfaatan hukum, dalil, teorema, aksioma, teori, formula, algoritma, metode, model, SW, HW, atau sistem yang sudah ada guna pemecahan suatu masalah. 

>Misal Penerapan Fuzzy untuk Kendali Motor, Kohonen untuk Pemetaan Gaya Belajar, dan Metode Image Watermarking berbasis FWHT-DCT. Penerapan dapat tergolong Pemutakhiran, atau Penemuan bila belum ada penelitian tentang hal tersebut sehingga merupakan pengetahuan baru.

