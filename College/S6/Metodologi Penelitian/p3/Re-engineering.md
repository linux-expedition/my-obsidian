>Re-engineering merupakan: pengubahan dan pengorganisasian kembali komponen-komponen sistem yang dapat dilakukan terhadap hasil desain atau implementasi saja atau pada keseluruhan tahapan/ abstraksi sistem, tanpa menghilangkan keseluruhan komponen lama agar diperoleh metode, formula, model, prototipe, sistem, atau tools dengan tingkat kesempurnaan dan standar yang lebih tinggi.

