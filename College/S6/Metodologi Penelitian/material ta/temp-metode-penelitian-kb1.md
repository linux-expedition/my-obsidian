1. Hari ke-1 : Perhatikan kejadian-kejadian dalam keseharian Anda. Temukan 5 masalah yang menurut Anda dapat diselesaikan dengan teknologi, khususnya teknologi komputer. Tuliskan temuan Anda pada kolom dibawah ini!
- disinformasi
- membantu communication disability stroke
- sync beberapa akun kalender atau platform menjadi satu kalender
- mengenali tingkat ke sensifitas-an pada suatu teks dalam bahasa indonesia
- suara ke text bahasa jawa

2. Hari ke-1 : Dari 5 masalah yang telah Anda tuliskan pada poin 1, pilih satu saja yang menurut Anda urgent untuk diberikan solusinya. Kemudian uraikan alasan Anda mengapa hal tersebut menjadi urgent. Dukung dengan referensi, boleh dari berita di media massa, majalah ilmiah populer, artikel jurnal dan lain sebagainya. Berikan link dari referensi-referensi yang Anda gunakan!
	-> Sejak adanya pandemi yang menyebabkan sebagian besar orang-orang untuk berada di rumah, maka semakin banyak juga orang-orang yang mengakses internet. Entah untuk kerja, belajar, atau hiburan. Sehingga dari media sosial lebih ramai dari biasanya. Maka, menurut saya disinformasi merupakan masalah penting untuk diselesaikan. Karena dengan banyaknya pengguna internet, semakin banyak juga potensi penyebaran maupun penerimaan disinformasi. Sebagaimana [topik](https://www.unicef.org/indonesia/id/coronavirus/cerita/melawan-infodemi-di-tengah-pandemi) yang disebutkan oleh unicef. Atau dengan referensi-referensi berikut.
	- https://www.brookings.edu/research/how-to-combat-fake-news-and-disinformation/
	- https://www.disinfo.eu/face/algorithms-not-designed-to-have-quality-information-in-their-objective-functions-will-naturally-favour-disinformation/
	- https://data.algotransparency.org/
	- https://www.scientificamerican.com/article/information-overload-helps-fake-news-spread-and-social-media-knows-it/

3. Hari ke-2 : Cari dan temukan teknologi (dalam bidang informatika) yang dapat Anda tawarkan untuk menyelesaikan masalah yang telah Anda pilih pada poin 2. Dukung penawaran Anda dengan referensi yang sesuai, yang menunjukkan bahwa teknologi tersebut kemungkinan besar dapat menyelesaikan masalah tersebut. Berikan link dari referensi-referensi yang Anda gunakan.
	-> Sebagai batasan masalah, pemecahan masalah disinformasi adalah pengenalan mengenai judul artikel pada internet. Sehingga tidak mencakup suatu hal yang sudah menggunakan algoritma atau teknologi seperti algoritma di media sosial facebook atau deepfake.
	Pertama, teknologi yang akan dikembangkan haruslah bisa memahami teks tersebut. Lalu, dapat mengklasifikannya ke jenis-jenis disinformasi. Lalu menentukan apakah teks tersebut termasuk disinformasi atau tidak. Sehingga referensi-referensi teknologi yang bisa digunakan adalah sebagai berikut.
	- https://www.tableau.com/learn/articles/natural-language-processing-examples
	- https://www.techtarget.com/searchenterpriseai/definition/natural-language-processing-NLP#:~:text=Natural%20language%20processing%20(NLP)%20is,in%20the%20field%20of%20linguistics.
	- https://openai.com/blog/openai-api/
	- https://www.mendeley.com/catalogue/1b5af44a-190e-3052-838c-097026055a19/
	- https://www.mendeley.com/catalogue/370bac57-69ae-374b-90dd-061fa01a0c60/
	- https://www.mendeley.com/catalogue/da02e497-c883-3090-8df4-0556dc3ae826/
	- https://www.mendeley.com/catalogue/dd0db45a-80f2-346f-810f-1729b196e21d/

4. Hari ke 2 : Dari apa yang telah Anda temukan pada poin 2, termasuk ke dalam isu perkembangan bidang Teknologi Informasi yang manakah teknologi tersebut? Jelaskan!. Cocokkan dengan 10 daftar pada gambar berikut!![[ecc283e6-8595-4720-a597-270e3be01575.png]]
	Termasuk big data and augmentend analytics, AI, dan Cloud and edge computing
	Karena sangatlah sulit untuk memahami bahasa manusia yang sangat bervariasi dan apalagi jika komputer yang melakukannya. Sehingga dibutuhkannya kapasitas komputer untuk melakukan analisa yang mendalam terhadap data yang diberikan. Lalu sebuah komputer juga diharapkan dapat belajar, karena bahasa akan juga dapat berubah-ubah apalagi dengan pengaruh media sosial atau trend. Sehingga perlu cloud untuk penyimpanan datanya atau letak algoritma disimpan, dilatih, dan dijalankan.

5. Hari ke-3: Pelajari lebih lanjut terkait dengan teknologi yang Anda tawarkan pada poin 3. Ceritakan desain umum (versi Anda) bagaimana teknologi tersebut dapat diterapkan untuk menyelesaikan masalah pada poin 2. Desain meliputi penentuan input dan output serta susunan komponen-komponen sistem dalam memproses input menjadi output.
	Teknologi tersebut bisa berupa hal yang sangat sederhana dan mudah untuk digunakan, yaitu plugin browser. Input berupa teks yang ada di internet. Dan seorang user yang akan memicu teknologi tersebut dijalankan atau tidak.
	Hasil bisa berupa presentase keyakinan bahwa teks tersebut fakta atau hoax.
	Jika fakta akan memberikan poin terhadap website tersebut juga memberikan daftar artikel yang mendukung.
	Jika hoax akan mengurangi poin terhadap webisite tersebut juga memberikan daftar artikel yang menyangkal kebenarannya.
	Poin yang didapat dari sumber domain yang dikumpulkan akan membantu penilaian integritas sumber website dan menjadikannya salah satu daftar artikel yang akan direkmendasikan saat menampilkan fakta atau hoax.


6. Hari ke-4: Dari apa yang telah Anda kerjakan pada poin 1 s.d. 4, jelaskan siapa target pemakai teknologi yang Anda kembangkan, jelaskan pula apa manfaat yang dapat diperoleh masyarakat/pihak yang menggunakan teknologi tersebut!
	Target adalah semua orang yang menggunakan internet.
	Membantu para pengguna untuk melakukan fact-check dan mendukungnya untuk tidak melihat dari satu artikel saja.



7. Hari ke-5: Evaluasi tingkat penguasaan dan pemahaman Anda terhadap seluruh Mata Kuliah yang telah Anda tempuh hingga saat ini. Dari kondisi tersebut, tuliskan setidaknya 3 hal yang dapat mendukung Anda dalam menyelesaikan TA dan tuliskan 3 hal yang mungkin dapat menjadi kendala Anda dalam menyelesaikan TA dengan topik yang telah Anda paparkan pada poin 1 s.d. 5.
	Hal yang mendukung:
	- Adanya fasilitas konsultasi untuk membantu pemilihan keputusan benar atau salah
	- Adanya matakuliah yang materinya akan membantu
	- Email machung akan dapat berguna apabila ada suatu teknologi berbayar namun ada student pack nya.
	Hal yang menghambat:
	- Sebagian besar teknologi yang dibutuhkan mengarah ke penjurusan sistem cerdas. Sehingga kurang yakin apakah penjurusan sistem komputer mampu untuk menyelesaikannya.
	- Cloud computing akan berbayar apabila kapasitas penyimpanan atau pengaksesnya lebih dari standard gratis yang sudah ditentukan.
	- Masih banyak algoritma yang belum terpahami: cnn, ann, npl, dll

8. Dari 7 dosen Prodi Teknik Informatika berikut, pilih 3 dosen yang menurut Anda kepakarannya paling cocok untuk dapat diajak berdiskusi terkait dengan ide TA Anda tersebut!
  
Dr. Eng. Romy Budhy Widodo, M.T.

Windra Swastika, Ph.D.

Dr. Kestrilia Rega P, M.Si.

Hendry Setiawan, M.Kom.

Paulus Lucky Tirma I, M.T.

Oesman Hendra Kelana, M.Cs.

M. Subianto, M.Cs.


9. Cenderung termasuk kedalam paradigma penelitian informatika yang manakah ide TA Anda?
Teori

- Pemodelan

- Produk/Sistem


10. Dari berbagai bentuk kebaruan terkait TA berikut, manakah yang menurut Anda dapat Anda penuhi melalui ide TA Anda?

Teknologi/metode yang diusulkan belum pernah ada sebelumnya

Teknologi/metode yang diusulkan belum pernah diterapkan untuk menyelesaikan masalah yang diangkat

Teknologi/metode yang diusulkan sudah pernah diterapkan untuk menyelesaikan masalah yang diangkat namun pada dataset yang berbeda (dapat berbeda lokasi, waktu atau objek)

Teknologi/metode yang diusulkan sudah pernah diterapkan untuk menyelesaikan masalah yang diangkat namun ada perubahan pada pengaturan beberapa komponen/parameternya

Sama sekali tidak ada kebaruan, mengulangi saja apa yang sudah pernah dilakukan mahasiswa lain