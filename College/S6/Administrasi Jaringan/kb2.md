1.Di bawah ini mode wireless yang tidak termasuk ke dalam interface WLAN1 adalah:
Station
**AP-Bridging**
Bridge
Wireless AP
Station PseudoBridge Clone


2.Berikut ini Opsi konfigurasi yang tidak terdapat pada menu konfigurasi System > Reset Configuration adalah:
No Default Configuration
Do Not Backup
Keep User Configuration
Semua benar
**Backup Configuration Before Reset**


3.Berikut ini adalah contoh dari media transmisi yang menggunakan media fisik, **kecuali**:
Fiber Optic
**Wireless**
Coaxial
LAN


4.Berikut ini adalah fungsi dari Firewall yang tepat adalah:
Penghubung antara dua jaringan sekaligus
Penghubung antara dua jaringan yang berbeda
**Mengatur dan mengontrol lalu lintas jaringan**
Penghubung antara dua jaringan ke internet dengan menggunakan satu alamat IP Address
Program yang berfungsi untuk mendapatkan konten dari internet atau intranet


5.Apakah winbox sudah bisa berjalan di Sistem Operasi Linux?
**Sudah tapi harus menggunakan Wine**
Belum
Tidak bisa
Perlu Emulator
Bisa

6.Status apa yang ditampilkan pada status DHCP Client, ketika berhasil mendapatkan IP address dynamic di bawah ini:
**Bound**
Bounding
Success
Start
End


7.Perkembangan mikrotik semakin lama semakin pesat, banyak juga penggunanya. Mikrotik dapat berperan menjadi perangkat apa saja dalam jaringan mulai dari Router, Bridge, Switch, dan Access Point masih banyak lagi. Berikut ini adalah fitur sever pada mikrotik adalah:
**Hotspot Radius**
Firewall
Routing
Bridging
Semua benar

8.Berikut ini kelebihan mikrotik adalah:
Harga yang cukup Terjangkau
Banyak Fitur
Mudah penggunaannya
Dapat berperan sebagai Router
**Semua benar**

9.User dalam Mikrotik dikategorikan ke dalam beberapa group sesuai hak aksesnya, di bawah ini yang **tidak** termasuk ke dalam group user tersebut adalah:
**Root**
User
Read
Full
Write

10.Subnet Mask yang dapat digunakan pada IP kelas B di bawah ini adalah:
255.0.0.0
**255.255.0.0**
255.255.255.248
255.255.255.0
Semua benar

11.Fitur pada DNS yang berfungsi untuk membuat sebuah alamat DNS lokal pada Mikrotik adalah:
DNS Request
Dynamic Servers
**Static**
Cache
Allow Remote Request


12.Parameter apakah pada konfigurasi NAT yang memiliki fungsi untuk menambahkan informasi Address-List secara dinamis:
Add Source to address list
Jump to address list IP
**Add Src & Dst to Address List**
Srcnat to address list
Jump to Add Src


13.Merupakan protokol pada Mikrotik yang berfungsi untuk membangun sebuah Network Tunnel antar MikroTik router di atas sebuah koneksi TCP/IP di bawah ini adalah:
**PPoE**
DNS
PPTP
EoIP
VPN Tunnel


14.Kekurangan dari Mikrotik adalah:
**Mikrotik belum mampu menangani sebuah jaringan komputer yang berskala besar**
User Friendly
Responsive
Murah
Semua benar


15.Ketika kita melakukan reset konfigurasi Mikrotik, routerboard akan di-reboot, setelah proses reboot selesai berapakah IP Address Ether 1?
**Dynamic**
192.168.100.1
192.168.1.1
Kosong
Sama seperti Ether 2


16.Alat yang berfungsi untuk menghubungkan dua jaringan dengan subnet yang berbeda adalah:
Switch
Modem
Access Point
**Router**
Hub


17.Proses pengaksesan Mikrotik ke router berdasarkan pengaturan user dan group, user dan group Mikrotik dapat ditentukan melalui menu:
Terminal > User
Tool > User
Mikrotik > User
**System > User**
Semua benar


18.PPP Secret adalah metode pengamanan pada Mikrotik, PP Secret dapat digunakan sebagai Tunneling berikut, **kecuali**:
PPPoe
SSTP
**OVPN**
L2TP
Semua Benar


19.Perbedaan mendasar aplikasi remote access Telnet dan SSH adalah terletak pada aspek:
**Keamanan**
Responsif
Mudah
Ringan
Kecepatan


20.Fungsi dari Winbox adalah sebuah software atau utility yang digunakan untuk me-remote sebuah server Mikrotik ke dalam mode:
**GUI (Graphical User Interface)**
CLI
Terminal
OS
Semua Benar