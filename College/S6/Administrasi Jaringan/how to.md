- Connect laptop to the router with a cable, plug it in any of LAN ports (2-5)  
- Disable other interfaces (wireless) on your laptop  
- Make sure that Ethernet interface is set to obtain IP configuration automatically (via DHCP)  
To connect to the AP you have to:  
• Remove the wireless interface from the bridge interface (used in default configuration)  
sb_bridge->tab_ports, remove wlan1  
• Configure DHCP client to the wireless interface  
sb_ip-dhcp client->tab dhcp client, new dhcp client  
interface = wlan1  
check use peer dns  
check use peer ntp  
blank DHCP options  
default routes = yes  
default route distance = 1  
• Create and configure a wireless security profile  
sb_wireless->tab_security profiles, new security profile  
mdoe dynamic keys  
auth type: wpa psk, wpa2 psk  
unicast: aes ccm, tkip  
group: aes ccm, tkip  
wpa wifi/ap pass  
wpa2 wifi/ap pass  
• Set the wireless interface to station mode  
sb_wireless->tab_interfaces, click wlan1  
mode = station  
security profiles = your last created profile's name  
MikroTik-F5A641 change to your ap ssid name  
• And configure NAT masquerade  
sb_ip-firewall->tab_nat, new nat rule  
chain = srcnat  
out.interface = wlan1  
action on action tab = masquarade  

  
system-packages, to updates  
files, to check downloaded updates



First time configuration
https://help.mikrotik.com/docs/display/ROS/First+Time+Configuration













import
