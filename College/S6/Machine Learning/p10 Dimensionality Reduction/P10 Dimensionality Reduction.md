

# Dimensionality Reduction
##### intro
Generasi model yang baik = apapun data test nya memiliki akurasi menyamai dengan data training.
Feature = kolom, Object = baris
	feature yang terlalu banyak menyulitkan model yang memiliki kemampuan akurasi yang baik

#### Introductions (why it's important)
- Memiliki dimensi yang sedikit, gampang untuk divisualisasikan. Via 3D, 2D, 4D sehingga terlihat cluster
- Tujuan mengurangi feature / dimensi agar mudah divisualkan
- Semakin banyak feature, semakin besar kemungkinan mencapai overfitting. Karena ciri terlalu mendetail, sehingga kemampuan generalisasi nya menghilang
	- Contoh: Membedakan laki n perempuan. Panjang rambut, hidung, kulit, telinga, sehingga terlalu fit dengan training.
- Jika dimensi terlalu banyak, maka volume spectrum akan semakin kecil
	![[p10-0.png]]
![[p10-1.png]]

- Jarak antar data akan semakin jauh
![[p10-2.png]]
Akibatnya tidak bisa mendapatkan pattern yang diinginkan, karena jarak antar data tidak bisa diidentifikasi lagi

#### Dimensionality Reduction
- Purposes
	- Menghindari curse of dimensionality
	- Mengurangi waktu dan memori yang digunakan oleh komputer dalam menjalankan ML
	- Memudahkan visualisasi
	- Menghilangkan fitur yang tidak relevan, sehingga mengurangi noise
- Techniques
	- feature selection = remove feature manually tapi tetap paham alasan dan teorinya
		Sulit, karena kasus kadang harus dipahami terlebih dahulu
		Sehingga berisiko menghilangkan feature yang bagus karena tidak paham
	- feature engineering
		Dari fitur yang ada direkayasa, sehingga menjadi bentuk yang berbeda tp masih tetap mewakili informasi yang dimuat oleh fitur yang ada
		- Linear: PCA, Factor Analysis, LDA (Linear Discrimnant Analysis)
		- Non-Linear: MDS, Isomap, LLE, HLLE, Laplacian Eigenmap, t-SNE

#### Feature Selection
Cara menghilangkan feature
- Feature Redundant
	Ex: Tingkat pendapatan dan jumlah pengeluaran, 
- Irrelevant feature
	ex: menghitung kesejahteraan tapi ada feature asupan kalori, di mana tidak relevan
- Techniques
	1. Brute-force approach
		Try all possible feature subsets as input to data mining algorithm
	2. Embedded approaches
		Feature selection occurs naturally as part of the data mining algorithm
	3. Filter approaches
		Features are selected before data mining algorithm is run
	4. Wrapper approaches
		Use the data mining algorithm as a black box to find best subset of attributes
#### PCA (Principal Component Analysis)
Meringkas data dengan fitur yang sangat banyak, menjadi 2-3 fitur saja. 
>Jadi, goal-nya menemukan proyeksi dari fitur yang kita gunakan yang itu bisa men-capture informasi utama di dalam dataset kita. Informasi utama = variasi data

Ex: dataset dengan feature BB(Berat Badan|x2) dan TB(Tinggi Badan|x1) menjadi variable e yang mewakili BB dan TB. Sehingga dataset menjadi memiliki 1 feature saja yaitu e
Keunggulannya kita tidak membuang x1 dan x2. Tapi merekayasanya menjadi e.
Kelemahannya kita clueless e itu apa? Kehilangan kemampuan penjelasan arti dari e itu.
Dideskripsikan memakai statistik deskriptif dengan table dan grafik. Bisa melalui clustering, lalu menelusuri kembali ke fitur awal.
![[P10-3.png]]

#### The Algebra of PCA
1. Calculating the covariance matrix
	>Buat covariance matrix

	penting karena engineering dataset adalah merubahnya menjadi dataset baru yang mengambil variasi maksimum dari kumpulan dataset. Menghitung variasi dari seluruh dataset, adalah dengan kovarian matriks

2. Calculating the eigenvalues and eigenvector
	>Buat eigenvalues; dari eigenvalues, hitung eigenvector

	Adalah mencari e atau komponen utama. **Variable e**
	![[P10-4.png]]
	- Eigenvalues akan memberitahu kita, komponen mana yang punya varian lebih besar dibandingkan komponen yang lain.
		Ex: Dari 30 fitur, kita minta reduksi menjadi 10 fitur
		Eigenvalues akan memandu kita dalam memilih dari 10 fitur, mana yang variasi datanya ditangkap paling banyak. 
		Biasanya eigen 1 adalah komponen yang memuat variasi paling tinggi. Jadi, maksudnya eigen 1 paling banyak mengandung informasi dari keseluruhan fitur dataset kita. Semakin ke bawah, semakin menurun. Sehingga eigen 10 biasanya komponen yang memiliki variasi paling kecil.
		![[p10-6.png]] Dari hasil 10 fitur itu, apakah kita gunakan 10" nya? Atau separuhnya? Atau hanya 3 aja seluruh variasi itu sudah bisa diambil?
		Ternyata dari 1-4 saja 95% variasi dari dataset kita terambil, maka tidak perlu lambda 5-10 tidak perlu. Sehingga hanya menggunakan 4 eigenvector.

3. Forming Principal Components 
	>Dari eigenvector, kalikan dengan datanya sehingga terbentuk (PC)Principal Component
	
	Membentuk nilai PC1 dan PC2
	![[p10-7.png]] Dengan cara dari eigenvalues menjadi eigenvector, eigenvector dikalikan dengan datanya, maka menjadi principal component.

4. Projections into the new feature space
	> Proyeksikan ke dalam space yang baru (2D, 3D, selama masih bisa divisualisasi)

	![[p10-8.png]] Misal dari 30 fitur menjadi 2 fitur, sehingga dapat diproyeksikan. Sehingga didapati 2 PCA


#### How to
![[p10-9.png]]
- Pertama, punya data training yang berisi semua fitur. Jika tujuannya klasifikasi, maka tidak ada kelas, semua kolomnya adalah x1, x2, x3, dst sampai x13
- Hitung matriks kovarian; ukurannya 13x13 karena memiliki 13 fitur
- Butuh input an seberapa banyak jumlah eigen; Kebetulan input 2 sehingga mendapatkan 2 nilai eigenvalues
- eigenvector diperoleh dari perkalian dari eigenvalues (perhitungan tertentu). Eigenvalues 1 akan membentuk eigenvector 2. Dengan jumlah data masing" adalah 13 atau sesuai banyaknya fiturnya
- eigenvector dikalikan dengan datanya sehingga menghasilkan principal component-nya. Sehingga dataset sebelumnya telah diwakili oleh data baru yaitu tabel principal component


#### References
- https://github.com/Eshan-Agarwal/PCA-on-Boston-House-price-Data-Set/blob/master/PCA_BOston.ipynb
>Code python pada link tersebut sudah sesuai dengan penjelasan saya terkait 4 tahap membentuk PCA, perhatikan komen yang diberikan penulis di setiap baris code. Sudah dimulai dari standarisasi data lalu masuk ke 4 tahapan membentuk PC

- [Machine Learning - Dimensionality Reduction - Feature Extraction & Selection](https://www.youtube.com/watch?v=AU_hBML2H1c)
- [StatQuest: Principal Component Analysis (PCA), Step-by-Step](https://www.youtube.com/watch?v=FgakZw6K1QQ)
- [A beginner’s guide to dimensionality reduction in Machine Learning](https://medium.com/towards-data-science/dimensionality-reduction-for-machine-learning-80a46c2ebb7e)
- [PCA using Python (scikit-learn)](https://medium.com/towards-data-science/pca-using-python-scikit-learn-e653f8989e60)

#### KK
- Masing-masing memilih dataset yang diinginkan. Pastikan semua variabel yang digunakan adalah kuantitatif bukan kategorikal
	- Dikarenakan reengineering nya berjalan secara matematis atau penjumlahan. Jika kategorikal 1, 2, 3 ditambahkan, maka tidak tepat.
- Lakukan preprosesing data seperti untuk menangani data hilang dan juga untuk menyamakan besaran dari setiap variabel (bisa dengan normalisasi atau standarisasi)
- Lakukan reduksi dimensi dengan PCA, boleh direduksi menjadi 2 atau 3 PC
- Visualkan hasil reduksi dimensi yang telah Anda buat dalam koordinat kartesius
- Pilih salah satu algoritma dalam Teknik ML, bisa klasifikasi, regresi ataupun clustering
- Terapkan algoritma ML yang Anda pilih ke dataset asli dan ke dataset hasil reduksi dimensi
- Bandingkan akurasinya dan laporkan pekerjaan Anda dalam bentuk pdf dan link colab


##### Key Terms
Matrix covariance
	Matrix yang menangkap variasi bersama dari seluruh fitur yang kita gunakan
Eigenvalues
	Adalah suatu nilai yang akan membuat eigenvector 1, 2, 3, ... dst. 
Eigenvector
	Adalah data hasil dari perhitungan eigenvalues -> e1, e2, e3, .... dst.
	![[p10-5.png]]

