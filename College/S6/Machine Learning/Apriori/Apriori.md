### Association Rules Apriori
[[#Footer]]
![[#Footer]]

### Definition
- Adalah teknik data mining u/ menemukan aturan di antara suatu kombinasi item
- Contohnya: Mengetahui kemungkinan pelanggan membeli roti bersamaan dengan susu
	Sehingga membantu pengaturan tempat atau pengaturan marketing dgn adanya discount dari kombinasi barang tertentu
- Karenanya, asosiasi ini terkenal dengan istilah market basket analysis

### The rules
Frequent itemset = frekuensi kemunculan kombinasi item = theta?symbol?!!

#### Support
$$
\begin{align}
2\text{ itemset} = {\text{jumlah transaksi mengandung A} \over \text{total transaksi}} \\
3\text{ items set} = {\text{jumlah transaksi mengandung A} \over \text{total transaksi}}
\end{align}
$$

#### Confidence

#### Example
Ex: {roti, mentega} -> {susu} (support = 40%, confidence = 50%)
- 50% dari transaksi di database yang memuat item roti dan mentega juga memuat item susu. Sedangkan 40% dari seluruh transaksi yang ada di database memuat ketiga item itu.
- Seorang konsumen yang membeli roti dan mentega punya kemungkinan 50% untuk juga membeli susu. Aturan ini cukup signifikan karena mewakili 40% dari catatan transaksi selama ini.
> 100% transaksi `[roti, mentega]`, 50% nya adalah `[roti, mentega, susu]`
> di support dengan dari semua transaksi yang ada, ada 40% yang sudah terjadi 3 item itu terbeli




### Footer
From pdf file [[asosiasi rules apriori.pdf]]