




# Observation
When it's S/R then the probability of Bob getting H/G is ...
- S > H = 0.8
- S > G = 0.2
- R > H = 0.4
- R > G = 0.6
# Weather Transition
If today is S/R then the probability of tomorrow being S/R is ...
- S > S = 0.8
- S > R = 0.2
- R > R = 0.6
- R > S = 0.4

![[p13-a.png]]
Observation state = what we see
	emission probabilites - observation are emitted from the hidden state
Hidden state = observe based of observations
	weather - transition probability



Q
1. How did we find these probabilites
2. What's the probability that a random day is sunny or rainy
3. if bob is happy today, what's the probability that it's Sunny or Rainy
4. if for three days Bob is happy, Grumpy, happy, what was the weather

# how did we find these probabilities
Calculate probability using data from the past
![[p13-b.png]]
![[p13-c.png]]
![[p13-a.png]]


# What's the probability that a random day is sunny or rainy
![[p13-d.png]]
Conclude, what's the weather today? S=2/3 R=1/3

# if bob is happy today, what's the probability that it's Sunny or Rainy

Berdasarkan probabilitas yang sudah ditemukan
S = 2/3 -> H=0.8 || G=0.2
R = 1/3 -> H=0.4 || G=0.6

Jika ada 15 hari
5 hari S dengan 4/5 nya H dan 1/5 nya G
5 hari S dengan 4/5 nya H dan 1/5 nya G
5 hari R dengan 2/5 nya H dan 3/5 nya G

Jika H maka,
	P(S|H) = 4+4+2 / 10 = 8/10 => banyak H n cuaca S / dari total hari dengan mood H
	P(R|H) = 2 / 10 => banyak H n cuaca R / dari total hari dengan mood H
Jika G maka,
	P(S|G) = 1+1 / 5 = 2/5 => banyak G n cuaca S / dr total hari dengan mood G
	P(R|G) = 3 / 5 => banyak G n cuaca R / dr total hari dengan mood G


# if for three days Bob is happy, Grumpy, happy, what was the weather

Jadi, ada 4 skenario kemungkinan setiap harinya
![[p13-e.png]]
H -> S = **2/3**  ||  S->H = **0.8**
	S -> S = **0.8**  ||  S->G = **0.2**
		S-S = **0.085**   =>   (0.67 x 0.8 x 0.8 x 0.2)
	S -> R = **0.2**  ||  R->G = **0.6**
		S-R = **0.064**   =>   (0.67 x 0.8 x 0.2 x 0.6)
Jika 3 hari berturut" maka ada 8 skeneraio maka S-S-S


# Namun, jika 4 hari berturut" maka 16 skenario?
Viterbi algorithm
Jika, dari observasi emosi bob adalah H-H-G-G-G-H, maka bagaimana urutan cuacanya?
![[p13-f.png]]
Day 1 - Happy
P(S) x P(S|H) = 0.67 x 0.8 = 0.533
P(R) x P(R|H) = 0.33 x 0.4 = 0.133
![[p13-f1.png]]


Day 2 - Happy
Today Sunny
	Kemarin - Sekarang = S-S
		P(S kemarin) x P(S-S) x P(S|H) = 0.533 x 0.8 x 0.8
		**Lebih besar ini, maka ambil ini yaitu 0.341**
	Kemarin - Sekarang = R-S
		P(R Kemarin) x P(R-S) x P(S|H) = 0.133 x 0.4 x 0.8
![[p13-f2.png]]
Today Rainy
	Kemarin - Sekarang = S-R
		P(S kemarin) x P(S-R) x P(R|H) = 0.533 x 0.2 x 0.4
		**Lebih besar ini, maka ambil ini yaitu 0.043**
	Kemarin - Sekarang = R-R
		P(R Kemarin) x P(R-R) x P(R|H) = 0.133 x 0.6 x 0.4
![[p13-f3.png]]

![[p13-g.png]]
![[p13-h.png]]
![[p13-i.png]]

