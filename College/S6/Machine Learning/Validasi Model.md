#### navigation
[[#Footnote]]
---

### Latar Belakang
Underfitting
	Akurasi kurang dari 90%
Overfitting
	Kebanyakan tidak sadar
	Terjadi ketika akurasi training lebih tinggi daripada test nya
	Solusi dengan: Tuning parameter atau memakai beberapa metode train test

### Provides the generalization ability of a trained model 
Untuk pengambilan kesimpulan umum
	bagaimana model dapat secara general, melihat pola basic terhadap kumpulan data trainingnya
	Sehingga ketika diberikan data baru, akan bertolak dari pola umum
	Agar tidak hanya mengenali pola khusus saja
Tidak ada metode validation yang works untuk semua scenarios
	Penting untuk mengetahui kita sedang berurusan dgn groups, time-indexed data, atau leaking data in your validation procedure
	Tergantung dari kasus yg sdg dikerjakan
#### Solution
Kasih se variasi mungkin
	ciri2 lebih detail untuk per kategori

#### key term
- Generalisasi

### Tahapan umum pengembangan model
1. Data cleansing and wrangling
pembersihan data dan persiapan data
dikumpulkan, ditata, berbentuk baris dan kolom agar mudah dibaca dengan software yg digunakan

2. Split the data into training and test data sets
Menerapkan teknik [[#Method Validation]]. Tetapi masih tahap ini masih belum masuk tahap mengevaluasi. Masih fokus cara memisahkan

3. Define the metrics for which model is getting optimized
Mendefinisikan metric, **METRIC** = indikator kinerja
Contoh:
- kalau klasifikasi, pake confussion matric / akurasi
- bs ditambahkan dgn precision, recall, f1 score
- kalo regression pake mse, mae
> harus paham algoritmanya, pake metric apa

4. get quick initial metrics estimate
Definisikan dulu harapan Anda
Bisa menaruh standard
Misal, berharap model dapat mengklasifikasikan dengan benar, lebih dari 80%
Untuk flashback apakah model sudah memenuhi harapan atau harus dilakukan sesuatu

5. Feature engineering to optimize the metrics. (Skip this during first pass)
~ untuk optimasi matriknya
biasanya tidak dilakukan di tahap pertama
> Tahap pertama itu maksudnya: punya data, model, langsung run
> karena belum tuning, ... , blm evaluasi

feature engineering dilakukan ketika kita dapat hasilnya. Jika dengan fitur yang ada akurasi sudah 90%, maka tidak perlu melakukan tahap ini
Tapi kalo akurasi masih rendah, bisa dilihat lagi dari sini
bisa jadi, feature nya tidak pas |
>misal feature pendapatan, diukur secara kuantitatif
>sehingga banyak variasinya tidak tertangani oleh model
>karena dari 0 - miliayaran
> melakukan feature engineering=> solusinya misal diubah menjadi kategorikal rendah,sedang, dst.

6. Data pre-processing
Case:
Misal pengelompokan dengan k-means. Supaya datanya adil, maka harus dipastikan skalanya sama. Jangan sampai pendapatan (jutaan) dengan jumlah anak (1-6).
data pre-processing => jadi, bisa normalisasi, standardisasi

7. Feature selection
Membuang feature

8. Model selection
Bisa random forest, nayes bayes, fcm, dst

9. Model validation
Membanding-bandingkan indikator dari setiap model untuk bisa dipilih mana yang terbaik

10. Interpret the results
Interpretasi hasilnya

11. Get the best model and check it against test data test
Menerapkan data test lalu ke data sesungguhnya

### Method Validation
- Train/test split
- k-Fold Cross-Validation
- Leave-one-out Cross-Validation
- Leave-one-group-out Cross-Validation
- Nested Cross-Validation
- Time-series Cross-Validation

### Train/test split
Simple split test
Param
Splitting into
	- Train
	- Test
	- Holdout: optional, dipakai ketika memilih model terbaiknya ??


### k-Fold Cross-Validation
splitting multiple times
and comparing between splitred data

Jika per split selisihnya tinggi, maka ada yang salah dengan datanya
Generalisasi akan balance jika selisihnya sedikit

### Leave-one-out Cross-Validation


### Leave-one-group-out Cross-Validation


### Nested Cross-Validation
validation inside k-fold cross validation
and use the best tune of each k-fold based on validation inside of it

### Time-series Cross-Validation


### Uji Statistic


### Wilcoxon signed-rank test
Untuk membandingkan 2 sample/model berbeda secara signifikan atau tidak
p-value < 0.05 => if true -> H1 model 2 != model 2


### McNemar's test
Khusus untuk klasifikasi
membedakan model 1 dengan model 2 sama atau tidak


### 5x2CV paired t-test
split train test 50:50 secara random
lalu juga reverse test train
Dilakukan 5 kali
jika  > 0.05 = sama
, < 0.05 = beda
**Perbedaan means**


### 5x2CV combined F test
**Perbedaan variance**



### Footnote
source: [[Model Validation.pdf]]
recommended source: [Validating your Machine Learning Model](https://towardsdatascience.com/validating-your-machine-learning-model-25b4c8643fb7)
[[#navigation]]
[example k-fold validation](https://medium.datadriveninvestor.com/k-fold-cross-validation-6b8518070833)
