``` mermaid
graph LR

run --80%--> run
run --5%---> rest
run --15%---> boxa((push up))
rest --75%---> run
rest --20%--> rest
rest --5%---> boxa((push up))
boxa((push up)) --38%---> run
boxa((push up)) --2%---> rest
boxa((push up)) --60%--> boxa((push up))
```
```pdf
{
	"url" : "/College/S6/Machine Learning/Attachments/Markov Chain.pdf",
	"page" : 9
}
```
(Observasi) = run, pu,pu,pu, rest, rest
maka probability nya ...

# Probability Tree
```pdf
{
	"url" : "/College/S6/Machine Learning/Attachments/Markov Chain.pdf",
	"page" : 14
}
```
```pdf
{
	"url" : "/College/S6/Machine Learning/Attachments/Markov Chain.pdf",
	"page" : 15
}
```

# Pendekatan Matriks Probabilitas
'awal jalan' J(1) = probabilitas jalan pada periode ke 1, jika pada periode ke-1 jalan
'awal rusak' R(3) = probabilitas rusak pada periode ke 3, jika periode ke-1 rusak

- $J(1) = 0$ dan $R(1) = 0$
- dalam vektor maka, $(J(1) R(1)) = (1 0)$

```pdf
{
	"url" : "/College/S6/Machine Learning/Attachments/Markov Chain.pdf",
	"page" : 19
}
```
> Karena sama dst. maka sudah stabil

