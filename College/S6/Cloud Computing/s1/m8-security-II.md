# Module 8: Security II
## **Module purpose**
The purpose of this module is to continue to develop your understanding of cloud security. The module covers the differences between AWS Shield and AWS WAF. You will learn what a distributed denial of service (DDoS) attack is. You will also learn about Amazon Inspector and AWS Artifact.

## **Module description**
In this module, you will be given scenarios related to the AWS Cloud. You will also determine whether security best practices are being followed and recommend steps to fix any security lapses.

## **Technology terminology**
[[AWS Terminology#Module 8 m8-security-II]]

## **Background and misconceptions**

Four areas of security must be addressed for cloud computing:

-   Data: Protecting the information that is stored and processed in the cloud
-   Permissions: Regulating who has access to the resources and data in the cloud
-   Infrastructure: Protecting the machines and hardware that run, store, and process data in the cloud
-   Assessment: Inspecting the infrastructure, permissions, and data to make sure that they are secure

In this module, we will address infrastructure and assessment.

-   Shield and AWS WAF are services that address attacks on infrastructure, primarily the network used to access cloud resources.
-   Amazon Inspector addresses assessment by investigating how well the cloud resources we use, such as our EC2 instances, are being protected. It also investigates whether these resources are following best practice guidelines.

The nature of the cloud makes it susceptible to cyberattacks that can make websites, applications, and processes stop working. When a billion-dollar company such as Amazon relies on the cloud, the company needs to know that it is protected against attacks.

One major type of cyberattack is called a DDoS. A DDoS occurs when attackers set up programs that send thousands or millions of requests to an app, website, or service at the same time. This spike in traffic can consume resources to the point where the website or app is no longer accessible for legitimate users.

DDoS attacks can be done for many reasons including competition, political motivation, or economic motivation. Whatever the reason, AWS has services that minimize these threats for cloud users.

**Shield** works in conjunction with Elastic Load Balancing, Amazon CloudFront, and Amazon Route 53 to protect against DDoS attacks. There are two tiers of service:

1.  AWS Shield Standard is available to all AWS users at no extra cost. It protects users from the most common DDoS attacks. This protection is applied automatically and transparently to any ELB resources, CloudFront distributions, and Route 53 resources.
2.  AWS Shield Advanced provides additional DDoS mitigation capability for volumetric attacks, intelligent attack detection, and mitigation for attacks at the application and network layers. Users get 24/7 access to the DDoS Response Team (DRT) for custom mitigation during attacks. Users also get advanced real-time metrics and reports, and DDoS cost protection to guard against bill spikes in the aftermath of a DDoS attack. Shield Advanced is available at an additional cost.

**AWS WAF** is another defensive tool provided by AWS. It helps protect web applications from exploits that might affect availability or security or consume resources. AWS WAF can monitor an application's web traffic and decide which traffic to let through based on the specific request being made. AWS users can create their own set of rules to direct what traffic is allowed by AWS WAF down to specific IP addresses.

**Amazon Inspector** does not actively protect your AWS services. Instead, it monitors the services and gives you updates on any vulnerabilities or any place where you are not following best practices. This can be useful to experts to make sure they are meeting security compliance standards and for new users who can learn about best practices.

Amazon Inspector works by running an assessment on your EC2 instances; the assessment checks for several predetermined best practices. After performing an assessment, Amazon Inspector produces a detailed list of security findings prioritized by level of severity. These findings can be reviewed directly or as part of detailed assessment reports that are available through the Amazon Inspector console or API.

Amazon Inspector security assessments help you check for unintended network accessibility of your EC2 instances and for vulnerabilities on those EC2 instances. Amazon Inspector assessments are offered to you as predefined rules packages mapped to common security best practices and vulnerability definitions. Examples of built-in rules include checking for access to your EC2 instances from the internet, remote root login being activated, or vulnerable software versions installed. AWS security researchers regularly update these rules.

**AWS Artifact** is a centralized resource for compliance-related information. Different organizations require cloud service providers (CSPs) to meet many different certifications and rules to host their data or process requests. Organizations that handle sensitive data such as bank information, personal information, or medical records must ensure that their cloud service meets certain security standards. AWS Artifact lists and gives details about the different compliance standards they meet.

## ****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **What might motivate someone to initiate a cyberattack against a company? What might attackers have to gain? Include an example of a company or a type of business and a kind of cyberattack it might be a victim of.**
2.  **Do you think there should be different security standards for the cloud based on the type of data that is being stored or processed? Why do you think that? Give an example. How do you think security differs between data stored in the cloud and data stored on premises?**
3.  **What character traits do you think a successful cloud security administrator would need? Why? Would this be a role that you would be interested in?**

## 

****Activity 1: AWS Cloud Security Basics****

****Overview****  
In this activity, you will take notes on Shield and AWS WAF. You will also learn about DDoS attacks and how a customer can use Amazon Inspector to help secure cloud services. Finally, you will recommend the most useful security tool for a given scenario.

**Objectives**

-   Compare the uses of Shield and AWS WAF.
-   Explain functions and features of Amazon Inspector and AWS Artifact.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Compare and contrast Shield and AWS WAF, giving at least one similarity and one difference.**

## 

**Activity 2: AWS Artifact and Compliance Hunt**

**Overview**  
In this activity, you will learn about AWS Artifact. You will also choose a compliance program and summarize the details. Finally, you will discuss your findings with the class.

****Objective****

-   Explain functions and features of Amazon Inspector and AWS Artifact.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **What industry did you read about? Why does that industry use a compliance program to secure their cloud services?**

## 

****Optional connection****

**More compliance programs**

1.  Navigate to “AWS Compliance Programs” ([https://aws.amazon.com/compliance/programs/](https://aws.amazon.com/compliance/programs/)).
2.  Select a different program than the one you read about in class.
    1.  Read about the compliance program.
    2.  Summarize the details.
    3.  Explain why the program is important to that industry’s cloud security.

**Unplugged option**

1.  Think of an industry that interests you.
2.  Write down why it would be important for that industry to secure its cloud services.
3.  List three rules that you think would be part of that industry’s compliance program.

## 

**Additional connections**

-   Amazon Inspector FAQs ([https://aws.amazon.com/inspector/faqs/](https://aws.amazon.com/inspector/faqs/))
-   AWS Shield FAQs ([https://aws.amazon.com/shield/faqs/](https://aws.amazon.com/shield/faqs/))
-   AWS WAF FAQs ([https://aws.amazon.com/waf/faqs/](https://aws.amazon.com/waf/faqs/))
-   AWS Best Practices for DDoS Resiliency ([https://d1.awsstatic.com/whitepapers/Security/DDoS_White_Paper.pdf](https://d1.awsstatic.com/whitepapers/Security/DDoS_White_Paper.pdf))
-   AWS Artifact FAQs ([https://aws.amazon.com/artifact/faq/](https://aws.amazon.com/artifact/faq/))