# Module 3: AWS Console
## 

**Module purpose**

In this module, you will learn how to access and navigate to some of the most common Amazon Web Services (AWS) services in the console. You will also learn about some of the real-world applications of these services.

## 

**Module description**

This module includes an educator-led activity to introduce you to AWS core services. You will then work in the AWS console to practice navigating to different services. Finally, you will complete a research activity to learn about how these services are used in different industries.

## **Technology terminology**



## 

**Background and misconceptions**

This module introduces a lot of new vocabulary. Following are some good introductory videos to become familiar with.

Amazon EC2: [Introduction to Amazon EC2 - Elastic Cloud Server & Hosting with AWS](https://youtu.be/TsRBftzZsQo)[](http://www.youtube.com/watch?v=TsRBftzZsQo)

Amazon S3: [Introduction to Amazon S3](https://youtu.be/_I14_sXHO8U)

Amazon EBS: [Amazon Elastic Block Store (EBS) Overview](https://youtu.be/77qLAl-lRpo)

Amazon RDS: [Understanding Amazon Relational Database Service (RDS)](https://youtu.be/eMzCI7S1P9M)

[](https://youtu.be/eMzCI7S1P9M)[](https://youtu.be/sI-zciHAh-4)

DynamoDB: [What is Amazon DynamoDB?](https://youtu.be/sI-zciHAh-4)

Amazon Redshift: [Introduction to Data Warehousing on AWS with Amazon Redshift](https://youtu.be/_qKm6o1zK3U)[](https://www.youtube.com/watch?v=_qKm6o1zK3U)

CloudWatch: [Amazon CloudWatch: Complete Visibility of Your Cloud Resources and Applications](https://youtu.be/a4dhoTQCyRA)[](https://youtu.be/a4dhoTQCyRA)

CloudTrail: [AWS CloudTrail: Simplify Security Analysis, Resource Change Tracking, and Troubleshooting](https://youtu.be/mXQSnbc9jMs)

AWS Cloud services include a host of different tools that work together to cover all the computing needs of a user, completely in the cloud.

Amazon VPC is the virtual network you define where you launch AWS resources. This virtual network closely resembles a traditional network that you operate in your own data center, with the benefits of using the scalable infrastructure of AWS.

Here are a few differences between the services:

Amazon S3 and Amazon EBS are both forms of data storage. There are a few key differences:

1.  Amazon EBS can only be used when attached to an EC2 instance, and Amazon S3 can be accessed on its own.
2.  Amazon EBS cannot hold as much data as Amazon S3.
3.  Amazon EBS can only be attached to one EC2 instance, whereas data in an S3 bucket can be accessed by multiple EC2 instances.
4.  Amazon S3 experiences more delays than Amazon EBS when writing data.

Amazon RDS, Amazon Redshift, and DynamoDB are all related to databases, but there are differences:

1.  Amazon RDS is the classic relational database that uses SQL Server, Oracle Database, Amazon Aurora, or other similar database systems. Think of this as a gradebook where each student is a row, and they all have the same number of assignments (columns) that they are attached to. Businesses can use code to search for specific data based on the information in the rows and columns. Amazon RDS is useful for companies that store a moderate amount of data that is uniform in structure, meaning each unique ID, like student name, is attached to the same number of data points (grades).
2.  Amazon Redshift is a relational database like Amazon RDS, but it is specifically made for huge amounts of data. It is a data-warehousing tool that is good for users working with big data.
3.  DynamoDB is a nonrelational database, meaning that you can’t use traditional systems like SQL Server or Aurora. Each item in the database is stored as a key-value pair or JavaScript Object Notation (JSON). This means that each row could have a different number of columns. The entries do not all have to be matched in the same way. This permits flexibility in processing that works well for blogging, gaming, and advertising.

CloudTrail and CloudWatch are both cloud monitoring services, but they perform different functions:

CloudTrail monitors all the actions that users have taken in a given AWS account. This means that any time someone uploads data, runs code, creates an EC2 instance, changes an S3 drive type, or any other action that can be done on AWS, CloudTrail will keep a log of it. This is very useful for security reasons so that administrators can know who is using their account and what they are doing. If anything goes wrong or if a security issue arises, CloudTrail will be the best evidence to figure out what happened.

CloudWatch monitors what all the different services are doing and what resources they are using. If CloudTrail is the people monitor, CloudWatch is the service monitor. CloudWatch is great for making sure that your cloud services are running smoothly and not using more or fewer resources than you expect, which is important for budget tracking. CloudWatch is great for making sure all your different resources are running, which can get tricky if a large company is using hundreds of different machines and drives. Monitors and alarms can be set up through CloudWatch to automatically initiates an alert when a metric hits a specific limit.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1. **What is a cloud service that you use regularly? What benefit does it provide you? Is there any downside to using this cloud service?**

Following are brief descriptions of IaaS, PaaS, and SaaS to help you answer the question.

**Infrastructure as a Service (IaaS):** These services contain the basic building blocks of the cloud. They provide access to computers, both physical and virtual, as well as to network features and storage space. Think of IaaS like renting a kitchen. You can use all the different appliances (mixers, blenders, sinks), and you can rent a kitchen with better appliances if you need them.

-   Examples: Amazon EC2, Rackspace, Google Compute Engine

**Platform as a Service (PaaS):** These services are the tools needed to manage the underlying hardware and launch applications. They include programming environments, application testing platforms, and application launchers. Think of PaaS as going to a restaurant. You are not managing the appliances in the kitchen, but you can ask the waiter or chef to make things however you want.

-   Examples: AWS Elastic Beanstalk, Microsoft Azure, Google App Engine

**Software as a Service (SaaS):** These services are the actual applications and software provided over the internet. You are not responsible for managing or installing the software; you simple access and use it. Think of SaaS as eating at an all-you-can-eat buffet. You have access to whatever food is being served. You don’t control what is made or how, but you can use as much as you want.

-   Examples: Dropbox, Slack, Spotify, YouTube, Microsoft Office 365, Gmail

2. **Most of you have used a SaaS type of cloud service. In the future, how might you use a PaaS or IaaS cloud service? How can the services help you in a career or accomplish a goal that you have?**

3. **What experience, if any, do you have with the AWS console and services? Which ones have you used, what have you created, are there any that you want to know more about?**

## 

****Activity 1: Learning the AWS Core Services****

****Overview****  
You will take notes on the core services provided by AWS. As you do so, you will navigate the AWS console to find each service.

**Objectives**

-   Identify features and functions of commonly used AWS services.
-   Access and navigate to commonly used AWS services.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Describe one of the real-world uses of an AWS service that you learned about. How does the service help the company or industry that uses it?**
-   **Which service do you think is the most important? Which service do you think you will likely use in your planned career area?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity1+worksheet.docx.pdf)

## 

**Activity 2: AWS Service Case Studies**

**Overview**  
You will be given a real-world industry example in which you will describe the AWS services being used and identify what benefit they are providing.

**Objective**

-   Analyze how AWS services are used in real-world industries.

### 

**Activity instructions**

1.  Your educator will assign you (or your group) a case study.
2.  Read the assigned case study and answer the following questions:
    1.  What is the company and what does the company create or what service do they provide?
    2.  What AWS cloud services does the company use and what do they use the AWS services for?
    3.  What benefits are the cloud services providing to the company?
3.  You can use your notes and any other resources you are familiar with to complete the task.
4.  When you are ready, be prepared to share your findings with the class.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **What surprised you about the case study you read?**
-   **Was any part of the cloud service usage confusing?**
-   **How do you think the company could have addressed the problem without using cloud services? Is this solution as beneficial to the company? Why or why not?**
-   **What, if any, questions do you still have pertaining to the case study you read?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity+2+worksheet.docx.pdf)

## 

**Optional connection**

Follow your educator’s instructions for completing this activity

**Further case studies**

a. Follow this link to the case studies: [https://aws.amazon.com/solutions/case-studies/all/](https://aws.amazon.com/solutions/case-studies/all/)

b. Select another business and complete another case study worksheet for that business.

**Unplugged option**

a. Do the same activity as previously described with the case studies provided by your educator.

## 

**Additional connections**

-   The [AWS Cloud Practitioner Essentials course](https://www.aws.training/Details/eLearning?id=60697) has videos that cover all the AWS services.
-   Amazon Virtual Private Cloud: [https://aws.amazon.com/vpc/](https://aws.amazon.com/vpc/)[](https://www.pcmag.com/article/256563/what-is-cloud-computing)