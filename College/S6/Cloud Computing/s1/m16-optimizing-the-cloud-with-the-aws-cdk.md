# Module 16: Optimizing the Cloud with the AWS CDK

Lesson 1 of 1

## **Module purpose**

In this module, you will explore the AWS Cloud Development Kit (AWS CDK) and how it integrates with services within the AWS Management Console. You will develop a computational artifact that demonstrates your understanding.

## **Module description**

In this module, you will follow an educator-led discussion of the AWS CDK and complete a hands-on task to apply the learning. You will then install and configure the AWS CDK and create your first AWS CDK application.

## **Technology terminology**

AWS Cloud Development Kit (AWS CDK)

Click to flip

An open-source software development framework that models and provisions cloud application resources

Click to flip

## **Background and misconceptions**

**AWS CDK**

AWS CDK is an open-source software development framework that models and provisions cloud application resources using familiar programming languages.

Provisioning cloud applications can be a challenging process that requires users to perform manual actions, write custom scripts, maintain templates, or learn domain-specific languages. AWS CDK uses the familiarity and expressive power of programming languages for modeling applications. It provides users with high-level components that preconfigure cloud resources with proven defaults, so they can build cloud applications without needing to be an expert. AWS CDK provisions resources in a safe, repeatable manner through AWS CloudFormation. It also lets users compose and share their own custom components that incorporate their organization's requirements, helping them start new projects faster. AWS CDK reduces the work of defining and configuring network resources compared to using Amazon CloudFront alone.

**Benefits**

AWS CDK accelerates onboarding to Amazon Web Services (AWS) because there are very few new things to learn. With the AWS CDK, you can use existing skills and tools, and apply those to the task of building cloud infrastructure. It also provides high-level components that preconfigure cloud resources with proven defaults, helping users build on AWS without needing to be an expert.

AWS CDK gives users the expressive power of programming languages for defining infrastructure. Familiar features such as objects, loops, and conditions accelerate the development process. Users can also use AWS CDK with their integrated development environment (IDE) to take advantage of existing productivity tools and testing frameworks.

With AWS CDK, users can design their own reusable components that meet their organization’s security, compliance, and governance requirements. As with any other software library, users can share components around their organization, letting them rapidly start up new projects with best practices by default.

AWS CDK lets users build a cloud application without leaving their IDE. Users can write their runtime code and define their AWS resources with the same programming language. Managing infrastructure as code provides great benefits and is often a stepping-stone for a successful application of DevOps practices. In this way, instead of relying on manually performed steps, both administrators and developers can automate provisioning of compute, storage, network, and application services required by their applications using configuration files.

For example, defining your infrastructure as code makes it possible to:

-   Keep infrastructure and application code in the same repository.
-   Make infrastructure changes repeatable and predictable across different environments, AWS accounts, and AWS Regions.
-   Replicate production in a staging environment to permit continuous testing.
-   Replicate production in a performance test environment used only for the time required to run a stress test.
-   Release infrastructure changes using the same tools as code changes so that deployments include infrastructure updates.
-   Apply software development best practices to infrastructure management such as code reviews or deploying small changes frequently.

  

![Diagram of AWS Cloud Development Kit. Use preconfigured application components: Download preconfigured components from a package manager or artifact repository. Model your application: Model your application logic and infrastructure in a programming language. Provision your application with AWS CloudFormation: Provision your application code and supporting infrastructure with AWS CloudFormation.](https://awsacademy.contentcontroller.com/vault/71a2ba5a-ff9a-4e62-b92a-2b3da0bd09f1/courses/25e81234-4285-4d40-9feb-6892db27d707/0/assets/n2bcg2MsnXiFk7wH_OsgWrDmg-xtp-aRh.png)

Image source: [https://aws.amazon.com/cdk/](https://aws.amazon.com/cdk/)

**Constructs**

Constructs are cloud components that encode configuration detail, boilerplate, and glue logic for using one or multiple AWS services. AWS CDK provides a library of constructs that cover many AWS services and features, letting users define their applications' infrastructure at a high level. Additionally, constructs are adjustable and composable. Users can quickly change any of the parameters or encode their own custom construct.

AWS CDK also provides low-level constructs called _CFN Resources_, which directly represent base-level CloudFormation resources and provide a way to define CloudFormation with a programming language. CFN Resources provide complete coverage of CloudFormation resources and are available shortly after a CloudFormation resource is updated or newly available.

With AWS CDK, anyone can customize, share, and reuse constructs within their organization or community, as with any other software library. This lets users build constructs that help them get started faster and incorporate best practices by default.

AWS CDK lets users define their infrastructure with code and provision it through CloudFormation. They get all the benefits of CloudFormation, including repeatable deployment, rapid rollback, and drift detection.

AWS CDK lets users model application infrastructure using TypeScript, Python, Java (developer preview), and .NET (developer preview). With AWS CDK, developers can use existing IDE, testing tools, and workflow patterns. By using tools such as autocomplete and in-line documentation, AWS CDK lets users spend less time switching between service documentation and their code.

AWS CDK lets users reference their runtime code assets in the same project with the same programming language. For example, they can include their AWS Lambda runtime code or Docker container image in an AWS CDK project and, when they deploy their application, the AWS CDK framework automatically uploads and configures the AWS service with their runtime assets. When the AWS CDK deployment is complete, they will have a fully functional application.

The AWS CDK command line interface (CLI) lets users interact with their AWS CDK applications and facilitates functionality such as synthesizing a CloudFormation template, showing the differences between the running stack and proposed changes, confirming security-related changes before deployment, and deploying multiple stacks across multiple environments.

![Diagram of AWS CDK Application. One construct consists of Amazon SQS and AWS Lambda and another construct consists of Amazon S3 and Amazon DynamoDB.  An arrow points from this stack to a box labeled CloudFormation Template, which contains code. Another arrow points from this box to a box labeled AWS CloudFormation. Three arrows point from the AWS CloudFormation box to Resources.](https://awsacademy.contentcontroller.com/vault/71a2ba5a-ff9a-4e62-b92a-2b3da0bd09f1/courses/25e81234-4285-4d40-9feb-6892db27d707/0/assets/u8Y84gsxxz48oWb9_wQDMGuCSxtzLBZBA.png)

Image source: [](https://aws.amazon.com/cdk/)[https://aws.amazon.com/blogs/aws/aws-cloud-development-kit-cdk-typescript-and-python-are-now-generally-available/](https://aws.amazon.com/blogs/aws/aws-cloud-development-kit-cdk-typescript-and-python-are-now-generally-available/)

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **When have you used a template to create a document or a presentation? Why did you use a template instead of creating a new document from scratch?**
2.  **Companies often have a basic product that they customize for different needs. For example, all baseball gloves are similar, but they can be customized for different needs such as to wear on the left or right hand. AWS CDK empowers developers to rapidly customize existing infrastructure. What is an example of an application that might need to be customized for different users?**
3.  **AWS CDK lets you implement code using the programming language of your choice. Why might this be valuable to both novice developers and more advanced users?**
4.  ****How is using AWS CDK different than using CloudFormation to create a stack? How does this compare to building a house using raw materials compared to using prefabricated components?****

## 

****Activity 1: AWS CDK Infomercial****

****Overview****  
In this activity, you will interact with media and print to explore the infrastructure of AWS CDK.

**Objective**

-   Explain the infrastructure of the AWS CDK.

### 

**Activity instructions**

1. Your educator will break the class up into small groups and explain the activity.

2. View AWS New York Summit 2019: AWS Cloud Development Kit ([https://www.youtube.com/watch?v=bz4jTx4v-l8](https://www.youtube.com/watch?v=bz4jTx4v-l8)).

-   Write down notes of important features you want to highlight.

3. Read the technical literature your educator hands out or read it online here: AWS Cloud Development Kit FAQs ([https://aws.amazon.com/cdk/faqs/](https://aws.amazon.com/cdk/faqs/)).

-   Underline or highlight important information you want to use in your infomercial.

4. Develop your infomercial advertising AWS CDK and present it to the class. Your group should consider:

-   **The audience:** A group of developers interested in AWS CDK
-   **Purpose:** To inform the developers of the benefits of using AWS CDK
-   **Presentation style:** You can act out your infomercial in front of the class.
-   **Information required:** Benefits of using AWS CDK, features, potential uses of AWS CDK

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   ****What would you identify as the **most important feature of** AWS CDK?****

## 

**Activity 2: AWS CDK Demo**

**Overview**  
In this activity, you will view a recorded demo of how to use AWS CDK to create an application. You will use prior knowledge to gain context on the application creation and reflect on your learning through collaborative discussion questions.

**Objective**

-   Use the AWS CDK to create an application.

### 

**Activity instructions**

1.  Watch the video AWS re:Invent 2018: Infrastructure Is Code with the AWS Cloud Development Kit (DEV372) ([https://www.youtube.com/watch?v=Lh-kVC2r2AU](https://www.youtube.com/watch?v=Lh-kVC2r2AU)), from 12:00 to 31:44.
    1.  As you watch, note the steps shown in the demonstration, such as creating a directory, initializing a template, and deploying the application.
2.  After the video ends, share your noted steps with the class. Which steps were commonly identified? Which steps were less commonly noticed?
3.  As a class or in smaller groups (as your educator prefers), engage in collaborative talks about the following reflection questions.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   ****What can we do with AWS CDK?****
-   **What are some basic steps to create an application with AWS CDK?**
-   **What is the benefit of using AWS CDK over creating applications without anything that already exists?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**AWS CDK constructs**

Consider the following idea: _Constructs are the basic building blocks of AWS CDK applications. A construct represents a_ cloud component _and encapsulates everything that CloudFormation needs to create the component._

A construct can represent a single resource, such as an Amazon Simple Storage Service (Amazon S3) bucket, or it can represent a higher-level component consisting of multiple AWS CDK resources. Examples of such components include a worker queue with its associated compute capacity, a scheduled job with monitoring resources and a dashboard, or even an entire application spanning multiple AWS accounts and Regions.

1.  Use this information to write a scenario in which a developer would use constructs to build a schoolwide cloud service. What steps would the developer take within AWS CDK? What would the cloud service accomplish for the school?

## 

**Additional connections**

-   Constructs ([https://docs.aws.amazon.com/cdk/latest/guide/constructs.html](https://docs.aws.amazon.com/cdk/latest/guide/constructs.html))
-   Getting started with the AWS CDK ([https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html))