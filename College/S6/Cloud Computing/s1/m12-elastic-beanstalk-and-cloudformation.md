#   Module 12: Elastic Beanstalk and CloudFormation

Lesson 1 of 1

## **Module purpose**
The purpose of this module is for you to understand the purpose of and be able to use AWS Elastic Beanstalk and AWS CloudFormation.

## **Module description**
In this module, you will create an application using Elastic Beanstalk and build a CloudFormation template.

## **Technology terminology**
[[AWS Terminology#Module 12 m12-elastic-beanstalk-and-cloudformation]]

## **Background and misconceptions**

**Elastic Beanstalk**

Elastic Beanstalk is an easy-to-use service for deploying and scaling web applications and services developed with Java, .NET, PHP, Node.js, Python, Ruby, Go, and Docker on familiar servers such as Apache, Nginx, Passenger, and IIS.

You upload your code and Elastic Beanstalk automatically handles the deployment, from capacity provisioning, load balancing, and automatic scaling to application health monitoring. At the same time, you retain full control over the AWS resources powering your application and can access the underlying resources at any time.

Benefits of Elastic Beanstalk:

1.  Fast and simple to begin
    1.  Elastic Beanstalk is the fastest and simplest way to deploy your application on AWS.
2.  Developer productivity
    1.  Elastic Beanstalk provisions and operates the infrastructure and manages the application stack (platform) for you, so you don't have to spend the time or develop the expertise.
3.  Impossible to outgrow
    1.  Elastic Beanstalk automatically scales your application up and down based on your application's specific need using easily adjustable automatic scaling settings.
4.  Complete resource control
    1.  You have the freedom to select the AWS resources, such as Amazon Elastic Compute Cloud (Amazon EC2) instance type, that are optimal for your application.

**CloudFormation**

CloudFormation provides a common language for you to describe and provision all the infrastructure resources in your cloud environment. CloudFormation lets you use programming languages or a simple text file to model and provision, in an automated and secure manner, all the resources needed for your applications across all AWS Regions and accounts.

Benefits of CloudFormation

1.  Model it all.
    1.  CloudFormation lets you model your entire infrastructure with a text file or programming languages.
2.  Automate and deploy.
    1.  CloudFormation provisions your resources in a safe, repeatable manner, letting you build and rebuild your infrastructure and applications, without having to perform manual actions or write custom scripts.
3.  It’s code.
    1.  Codifying your infrastructure lets you treat your infrastructure as code.

![[m12-pic-0.jpg]]
Diagram showing how AWS CloudFormation works. The first step is to code your infrastructure from scratch with the CloudFormation template language, in either YAML or JSON format, or start from many available sample templates. The next step is to check out your template code locally, or upload it into an S3 bucket. The next step is to use AWS CloudFormation via the browser console, command line tools, or APIs to create a stack based on your template code. Finally, AWS CloudFormation provisions and configures the stacks and resources you specified on your template.

Image source: [https://aws.amazon.com/cloudformation/](https://aws.amazon.com/cloudformation/)

**How Elastic Beanstalk differs from CloudFormation**

These services are designed to complement each other. Elastic Beanstalk provides an environment to easily deploy and run applications in the cloud. It is integrated with developer tools and provides a one-stop experience for you to manage the life cycle of your applications. CloudFormation is a convenient provisioning mechanism for a broad range of AWS resources. It supports the infrastructure needs of many different types of applications such as existing enterprise applications, legacy applications, and applications built using a variety of AWS resources and container-based solutions (including those built using Elastic Beanstalk).

To be clear, Elastic Beanstalk is like running a .bat file and CloudFormation is like writing a .bat file. Elastic Beanstalk lets developers upload and run their code; it then does all the behind-the-scenes cloud setup such as launching EC2 instances and attaching elastic block storage. With CloudFormation, you are basically setting up a template for all of the cloud resources you want to run so that it can all be done at once and in a repeatable way.

CloudFormation supports Elastic Beanstalk application environments as one of the AWS resource types. This lets you, for example, create and manage an application hosted by Elastic Beanstalk, along with an Amazon Relational Database Service (Amazon RDS) database to store the application data. In addition to RDS DB instances, any other supported AWS resources can be added to the group as well.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Elastic Beanstalk is a service that lets developers upload their applications and automatically provision all of the needed resources for the application to run smoothly and efficiently. How do you think this process differs from traditional application deployment (without the cloud)? Why is this style of deployment beneficial?**
2.  **What things do you picture or think of when you hear the name _Elastic Beanstalk_? Why do you think the AWS Cloud service that provides the necessary resources for an uploaded application is called _Elastic Beanstalk_?**
3.  **CloudFormation is a service that lets you create a template to deploy any number of cloud resources at any time. What are some other industries or processes that use a template to build or create something quickly? Why is this process beneficial?**

## 

****Activity: What Are Elastic Beanstalk and CloudFormation?****

****Overview****  
In this activity, you will take notes on Elastic Beanstalk and CloudFormation and use case studies to apply your learning.

**Objective**

-   Describe features and uses of Elastic Beanstalk and CloudFormation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **How would a new company benefit from CloudFormation and Elastic Beanstalk?**

## 

****Lab: Using Elastic Beanstalk and CloudFormation****

**Overview**  
In this lab, you will create a simple application and upload it to run with Elastic Beanstalk. You will then use a template to set up a virtual private cloud (VPC) with CloudFormation.

**Objectives**

-   Create an application using Elastic Beanstalk.
-   Use a template and CloudFormation to build a VPC.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **When you created the environment using Elastic Beanstalk, it also created a stack in CloudFormation. How do you think these two services work together?**
-   **Provide an example of how this process of using Elastic Beanstalk or CloudFormation would be used in a real-world setting.**
-   **How might Elastic Beanstalk or CloudFormation be useful to you when you are using AWS Cloud services?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Build your own CloudFormation template using Designer**

1.  Go to the AWS Academy Learner Labs environment.
    1.  Go to the CloudFormation console.
    2.  Select **Create stack** and select **With new resources (standard)**.
    3.  Select **Create template in Designer**.
2.  In a new tab, navigate to this AWS Walkthrough ([https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/working-with-templates-cfn-designer-walkthrough-createbasicwebserver.html](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/working-with-templates-cfn-designer-walkthrough-createbasicwebserver.html)).
3.  Complete the steps within **Step 1: Add and connect resources**to design a stack that will create a basic web server.
    1.  If you want a challenge, continue past **Step 1**.

**Unplugged option**

1.  Think of an application or a website that you use often.
2.  Make a list of the AWS services that you think would be required to run that application or website.
3.  Draw a visualization of how the services that you listed would interact in the cloud.

## 

**Additional connections**

-   AWS CloudFormation ([https://aws.amazon.com/cloudformation/](https://aws.amazon.com/cloudformation/))
-   AWS Elastic Beanstalk ([https://aws.amazon.com/elasticbeanstalk/](https://aws.amazon.com/elasticbeanstalk/))