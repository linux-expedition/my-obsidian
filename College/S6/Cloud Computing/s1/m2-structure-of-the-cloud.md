# Module 2: Structures of the Cloud

Lesson 1 of 1

## 

**Module purpose**

The purpose of this module is to learn about the three different types of cloud services: infrastructure as a service (IaaS), platform as a service (PaaS), and software as a service (SaaS). You will also learn about the geographical layout of the Amazon Web Services (AWS) Cloud infrastructure, which includes Regions, Availability Zones, and edge locations.

## 

**Module description**

This module will consist of note-taking and research. You will also be drawing diagrams of the AWS Global Infrastructure. You will find real-world examples of each type of cloud computing service.

## 

**Technology terminology**

Availability Zone

Click to flip

One or more data centers that house many servers. Each Region has multiple, isolated locations known as Availability Zones. Each Availability Zone is isolated, but the Availability Zones in a Region are connected through low-latency links. An Availability Zone is represented by a Region code followed by a letter identifier, for example, us-east-1a.

Click to flip

Edge location

A site where data can be stored for lower latency. Often, edge locations will be close to high-population areas that will generate high-traffic volumes.

Infrastructure as a service (IaaS)

A model in which virtual machines and servers are used for customers to host a wide range of applications and IT services are provided.

Latency

The delay before a transfer of data begins after the data has been requested.

Platform as a service (PaaS)

A model that provides a virtual platform for customers to create custom software.

Region

An area where data is stored. Data storage in a Region closest to you is one of the reasons it can be accessed at lightning speed.

Software as a service (SaaS)

A model that provides applications using the internet that are managed by a third party.

## 

**Background and misconceptions**

The AWS Global Cloud Infrastructure is the most secure, extensive, and reliable cloud platform, offering over 200 fully featured services from data centers globally. That infrastructure is made up of many different components including Regions, Availability Zones, and edge locations. For visual representations of these components, see the [Global Infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/) page.

The differences between the components of the infrastructure can be confusing because they are all interconnected and related to the physical layout of the AWS Cloud. It is good to have a concrete visual example.

Region > Availability Zone > edge location

You can find a resource for the types of cloud services on the AWS [Types of Cloud Computing](https://aws.amazon.com/types-of-cloud-computing/) site[](https://aws.amazon.com/types-of-cloud-computing/).

-   **IaaS:** These services contain the basic building blocks of the cloud. They provide access to computers—physical and virtual—and to network features and storage space. Think of IaaS like renting a kitchen. You can use all the different appliances (mixers, blenders, sinks), and you can rent a kitchen with better appliances if you need them.
    -   Examples: Amazon Elastic Compute Cloud (Amazon EC2), Rackspace, Google Compute Engine
-   **PaaS:** These services are the tools needed to manage the underlying hardware and launch applications. They include programming environments, application testing platforms, and application launchers. Think of PaaS as going to a restaurant. You are not managing the appliances in the kitchen, but you can ask the waiter or chef to make things however you want.
    -   Examples: AWS Elastic Beanstalk, Microsoft Azure, Google App Engine
-   **SaaS:** These services are the actual apps and software provided over the internet. You are not responsible for managing or installing the software; you just access and use it. Think of SaaS as eating at an all-you-can-eat buffet. You have access to whatever food is being served. You don’t control what is made or how, but you can use as much as you want.
    -   Examples: Dropbox, Slack, Spotify, YouTube, Microsoft Office 365, Gmail

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **How does your computer get information from the internet? When you open a website, where does the website come from? Who provides the data? Use what you have learned about computer science and cloud computing in your answer.**
2.  **What is a program or an app that you use that runs entirely in the cloud, meaning you don’t have to store anything on your computer or device? What do you use the program to do? How do you think the program is provided to you at little or no cost?**
3.  **More and more programs and apps are being moved from being stored locally on individual computers to being in the cloud. For example, many people now use internet-based word processing instead of software such as Microsoft Word, and Spotify instead of CDs and MP3 players. What is another program or service that you think will move into the cloud? Why do you think technology is moving in the direction of cloud computing? Give reasoning for your ideas based on what you have learned previously about cloud computing.**

## 

****Activity 1: Visualizing the AWS Global Infrastructure**  
**

**Objectives**

-   Explain the purpose of a Region, Availability Zone, and edge location.
-   Identify connections among Regions, Availability Zones, and edge locations.

**Overview**  
In this activity, you will learn about the AWS infrastructure and its purpose. You will take notes on what Regions, Availability Zones, and edge locations are and what they are used for. You will create your own diagrams of the AWS infrastructure to help make it more concrete in your minds.

-   **Region:** Areas where data is stored. Data storage in a Region closest to you is one of the reasons it can be accessed at lightning speed.
-   **Availability Zone:** A data center that houses many servers. Each Region has multiple, isolated locations known as Availability Zones. Each Availability Zone is isolated, but the Availability Zones in a Region are connected through low-latency links. An Availability Zone is represented by a Region code followed by a letter identifier, for example, us-east-1a.
-   **Edge location:** A site where data can be stored for lower latency. Often, edge locations will be close to high-population areas that will generate high-traffic volumes.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

Here are some sites that might help you with this activity.

-   [Regions and Availability Zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)[](https://aws.amazon.com/about-aws/global-infrastructure/)
-   [AWS Global Infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/)
-   [AWS Global Cloud Infrastructure](https://www.youtube.com/watch?v=RPis5mbM8c8)

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Compare your diagram with that of another student. Are there any differences? What is the same?**
-   **How are Regions, Availability Zones, and edge locations connected?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity1+worksheet.docx.pdf)

## 

**Activity 2: Types of Cloud Services**  
  

**Objectives**

-   Recognize the types of cloud computing.
-   Compare the types of cloud computing.

**Overview**  
You will learn about the three different types of cloud services. We will discuss the benefits of using these cloud services over traditional models.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **A pediatrician with a private practice has so many patient files, that she is running out of room in her filing cabinets. For this reason, she wants to move her data into the cloud. She wants to be sure the data is secure, but also wants her patients to be able to access their medical records and communicate with her online in a secure way. Describe one way that you can use each type of cloud service and how it would benefit her business.**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

Create a rubric where you rank the factors for a business to consider when choosing a Region to locate their cloud services. Then explain why those factors would be most (or least) important when choosing a Region.

Factors to consider:

-   AWS cost
-   Availability of services
-   Speed or latency
-   Resiliency of AWS components
-   Data rights
-   Audience

## 

**Additional connections**

-   [Types of Cloud Computing](https://aws.amazon.com/types-of-cloud-computing/): This resource provides descriptions of IaaS, PaaS, and SaaS as they relate to AWS.