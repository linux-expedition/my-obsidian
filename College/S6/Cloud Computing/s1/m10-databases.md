# Module 10: Databases
## **Module purpose**

In this module, you will learn about the Amazon Relational Database Service (Amazon RDS), Amazon DynamoDB, and data warehousing with Amazon Redshift. You will also compare relational and nonrelational databases and online transaction processing (OLTP) and online analytic processing (OLAP).

## **Module description**

In this module, you will recommend a relational or nonrelational database depending on a given scenario. You will create an RDS DB instance. You will also learn about and discuss appropriate usage of relational and nonrelational database systems.

## **Technology terminology**
[[AWS Terminology#Module 10 m10-databases]]


## **Background and misconceptions**

### **OLTP and OLAP**

Many different types of databases are available. To decide which type of database you need, it is important to know how the data will be processed. There are two types of data processing: online transaction processing (OLTP) and online analytic processing (OLAP).

**OLAP** operations are primarily read-only; that is, they read the data and perform various types of aggregation such as sum, group, and sort. Relational database management systems have built-in functions for performing these types of operations. Because they are built in, they are done efficiently. In a nonrelational database, the values must be extracted from the key-values pairs, which can be a time-intensive process.

OLAP systems are often used where the system is required to process a lot of related data, perhaps to generate business reports. Companies often need to analyze a lot of data points that have occurred over a long period of time to determine trends and predict behaviors.

This type of system doesn’t necessarily need to be a real-time system—it can run as a background process. For example, in an ecommerce system, an OLAP system could run in the background without impacting the user experience. Today, it’s more common to see relational databases (especially large-scale, columnar data stores) rather than nonrelational databases being used for OLAP.

**OLTP** operations, however, need to update the database in addition to reading it. Updating can involve adding, changing, or deleting values. Updating can become complex because many of the tables in a relational database are virtual. That is, the tables need to be combined in real time from nonvirtual tables. Consider the following example.

_A department store database has tables that contain information about customers and products. The customer table has data relating only to customers such as name and address. The product table has data relating only to products such as name and price. To record information about purchases, a purchase table must be created that has a combined primary key that includes both customer-ID and product-ID, showing how much of a certain product a particular customer purchased._

_To display a complete readout of the purchase, the customer table and the product table must be combined in real time with the purchase table to show things such as customer name, product name, how much of the product was purchased, and the cost of the sale. The type of operation that combines tables in real time is called a JOIN. The result of a JOIN is a virtual table and, in most cases, it cannot be updated directly._

OLTP systems are often used where the system is required to handle large volumes of transactions at a high rate. Many ecommerce systems, such as shopping carts, sell a large number of items during the checkout process while simultaneously removing the items from the inventory table. When the integrity of the entire transaction is critical, and when processing needs to happen in near-real time, companies should consider OLTP systems.

OLTP systems are not exclusively relational databases, even though there are relationships in the data. It’s becoming more common for nonrelational databases to enforce constraints and enable transactions, so that these databases can be used as OLTP systems.

Finally, integrity considerations must be handled in a relational database. In the example, if a product needs to be deleted from the product table, there must be rules to make sure that references to the product are also handled. These types of rules are known as integrity and consistency rules.

**Comparison of OLTP and OLAP**

OLTP | OLAP
---- | ----
Handles recent operational data |  Handles all historical data
Size is smaller, typically ranging from 100 MB to 10 GB | Size is larger, typically ranging from 1 TB to 100 PB
Goal is to perform day-to-day operations | Goal is to make decisions from large data sources
Uses simple queries | Uses complex queries
Faster processing speeds | Slower processing speeds
Requires read/write operations | Requires only read operations


**Applications of OLTP**

-   Entering orders online
-   Processing purchases
-   Storing customer details

**Applications of OLAP**

-   Analyzing shopping patterns to make recommendations
-   Tracking purchasing trends for targeted advertisement
-   Analyzing seasonal buying trends to make sure items are in stock

### **AWS database services**

**Amazon RDS** is the classic relational database that uses SQL, Oracle, Aurora, or other similar database systems. Think of this as a gradebook in which each student is a row and all students are attached to the same number of assignments (columns). Businesses can use code to search for specific data based on the information in the rows and columns. Amazon RDS is useful for companies that are storing a moderate amount of data that is uniform in structure, meaning each unique ID (such as student name) is attached to the same number of data points (grades).

Amazon RDS is primarily used for OLTP because it has better methods for maintaining the integrity and consistency of the database when processing data.

**DynamoDB** is a nonrelational database, meaning that you can’t use traditional systems such as SQL or Aurora. Each item in the database is stored as a key-value pair or a JavaScript Object Notation (JSON) file. This means that each row can have a different number of columns. The entries do not all have to be matched in the same way. This permits flexibility in processing that works well for blogging, gaming, and advertising. 

**Aurora** is a relational database engine that is specifically made to work with the AWS Cloud. Aurora is up to five times faster than standard MySQL databases and three times faster than standard PostgreSQL databases. It is designed to provide the security, availability, and reliability of commercial databases at one-tenth the cost. Aurora is fully managed by Amazon RDS, which automates time-consuming administrative tasks such as hardware provisioning, database setup, patching, and backups.

**Amazon Redshift** is a fast, fully managed data warehouse that makes it efficient and cost effective to analyze all your data using standard SQL and your existing BI tools.

## ****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**

1.  **This module covers different types of databases or tables that store data entries. What are some real-world uses of databases? Why are they useful? When have you used or seen a database in your own life?**
2.  **NoSQL databases like the ones used in DynamoDB store a set of values with a key in what is called a key-value pair. A key-value pair is a set of two linked data items: a key, which is an identifier for the item of data, and the value, which is the identity or location of the data. Can you think of anything else that is generally found in a key-value pair? Why is the key-value pairing a useful way to organize ideas or data points? If you were creating key-value pairs to sort your music, picture, or video libraries, what would be some of the values you would want to store?**
3.  **Amazon Redshift is a data warehousing service. A data warehouse is a central repository of information that can be analyzed to make better-informed decisions. It is a database specially designed for data analytics, which involves reading large amounts of data to understand relationships and trends across the data. A database is used to capture and store data, such as recording details of a transaction. What types of businesses do you think would benefit from a data warehousing service and how would they use data warehousing to improve their business decisions?**

## **Activity: Database Engineers**

****Overview****   
In this activity, you will role-play as the new database engineer for a company and report information about Amazon RDS and DynamoDB.

**Objectives**

-   Compare relational and nonrelational databases.

### **Activity instructions** 

Follow your educator’s instructions to complete the activity.

### **Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Why do you think more companies are migrating to the cloud? What do you think is holding some companies back from migrating more quickly?**

## **Lab: Creating an Amazon RDS Database Instance**

**Overview**  
In this activity, you will create an Amazon RDS database (DB) instance that maintains data used by a web application.

**Objective**

-   Compare relational and nonrelational databases.

### **Lab instructions**

Follow your educator’s instructions to complete the lab.

### **Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **Why do you think the default security setting for a database does not allow public connections to the instance?**
-   **How might the relational database that you created be used by a real-world company?**
-   **How might you use the process you learned in this lab in your life outside school?**
-   **What challenges, if any, did you run into during this process? What advice would you give to a new Amazon Web Services (AWS) user who wanted to create a relational database?**

## **Optional connection**

Follow your educator’s instructions for completing this activity.

**Practice with SQL**

1.  Go to the SQL practice website that your educator provides.[](https://noads.sqlzoo.net/)
2.  Spend 30 minutes learning about how SQL works to query a database.

**Unplugged option**

1.  Think about the types of actions that a customer might perform on the AnyCompany Crafting web application.
    1.  List these actions.
2.  For each action, think about what type of query would be made to the AnyCompany Crafting database. Write down as many ideas as you can think of.
    1.  Example: When a customer adds an item to their cart, the product database might be queried for price or availability.

## **Additional connections**

-   Amazon Relational Database Service ([https://aws.amazon.com/rds/](https://aws.amazon.com/rds/))
-   Amazon Redshift ([https://aws.amazon.com/redshift/](https://aws.amazon.com/redshift/))
-   Amazon Aurora ([https://aws.amazon.com/rds/aurora/](https://aws.amazon.com/rds/aurora/))