# Module 14: Billing and Support

Lesson 1 of 1

## **Module purpose**

In this module, you will learn about the AWS Support plans. You will also learn about the Amazon Web Services (AWS) Simple Monthly Calculator. Lastly, you will understand why AWS Organizations and consolidated billing are beneficial to businesses using cloud computing.

## **Module description**

In this module, you will use the Simple Monthly Calculator to analyze value propositions. You will look at real-world case studies to make recommendations for an AWS Support plan. Lastly, you will determine the benefits of using Organizations and consolidated billing.

## **Technology terminology**

AWS Simple Monthly Calculator

Click to flip

Provides an estimated monthly bill based on the storage and compute requirements of the user.

Click to flip

AWS Support plan

Support plans are designed to give you the right mix of tools and access to expertise so that you can be successful with AWS while optimizing performance, managing risk, and keeping costs under control.

AWS Organizations

Helps you centrally manage billing; control access, compliance, and security; and share resources across your AWS accounts.

Consolidated billing

With the consolidated billing feature in Organizations, you can consolidate billing and payment for multiple AWS accounts. Every organization in Organizations has a management (payer) account that pays the charges of all the member (linked) accounts.

Technical Account Manager (TAM)

This dedicated support person and cloud advisor for enterprise-level AWS accounts answers support questions, monitors your cloud account, and gives recommendations for optimization. 

## **Background and misconceptions**

AWS provides a number of useful billing and support services that help cloud users to make the most efficient use of their resources. These services include a calculator that will estimate monthly costs, billing dashboards to visualize expenditures, and a range of support plans with differing prices and services. To make it easier for large businesses with many accounts to pay for services, Organizations permits consolidated billing, which lets one account pay for all other accounts in an organization.

This table shows the most important similarities and differences between AWS Support plans.

Criteria | Basic | Developer | Business | Enterprise | 
 --- | --- | --- | --- | --- 
Cost | Free | Greater of $29 per month | Greater of $100 per month
 |     |     | or                      | or                                             | Greater of $15,000 per month
 | --- | --- | ----------------------- | ---------------------------------------------- |
 |     |     | 3% of monthly AWS usage | 10% of monthly AWS usage for the first $0–$10K |
 |     |     |                         | 7% of monthly AWS usage from $10K–$80K         |
 |     |     |                         | 5% of monthly AWS usage from $80K–$250K        |
 |     |     |                         | 3% of monthly AWS usage over $250K             |
 |     |     |                         |
 |     |     |                         |                                                |

or

10% of monthly AWS usage for the first $0–$150K

7% of monthly AWS usage from $150K–$500K

5% of monthly AWS usage from $500K–$1M

3% of monthly AWS usage over $1M

Use case

Learning

Experimenting

Production use

Mission-critical use

Tech support

No

Business hours email

24/7 email, chat, and phone

24/7 email, chat, and phone

Support response time

N/A

12–24 hours during business hours

1-hour response to urgent cases

15-minute response to critical support cases

TAM

No

No

No

Yes  

Support cases

None

One person, unlimited cases

Unlimited contacts/cases

Unlimited contacts/cases

All AWS customers receive the basic level of support for at no additional cost. Notice that only Enterprise-level accounts receive the benefit of a TAM. For Enterprise-level customers, a TAM provides technical expertise for the full range of AWS services and obtains a detailed understanding of your use case and technology architecture. TAMs work with AWS Solution Architects to help you launch new projects and recommend best practices throughout the implementation lifecycle. You have a direct telephone line to your TAM, who serves as your primary point of contact for ongoing support needs.

As you can see, the price for the different support plans ranges greatly from free to $15,000 a month, so selecting the right plan for your business is important.

Organizations is a great resource with many benefits. By permitting an organization to link multiple AWS accounts under a central account, one person can:

1.  Centrally manage policies across multiple AWS accounts
2.  Govern access to AWS services, resources, and Regions
3.  Automate AWS account creation and management
4.  Configure AWS services across multiple accounts
5.  Consolidate billing across multiple AWS accounts

![Structure of a basic organization: Seven AWS accounts are organized into four organizational units (OUs) under the root. The organization also has several policies that are attached to some of the OUs or directly to accounts.](https://awsacademy.contentcontroller.com/vault/48fdca50-b958-4e1b-a9f5-b48e95c26515/courses/3e450cd3-8ee1-4ff9-b539-3586da6e94f9/0/assets/oVa9LE--yAUzpRG1_l8FNTD8-yRNxDK3m.png)

An organizational unit (OU) is a container for multiple accounts. By attaching a policy to an OU, that policy applies to all accounts in that OU.

You will learn about the AWS Simple Monthly Calculator. With the Simple Monthly Calculator, you enter your needs in relation to Amazon Elastic Compute Cloud (Amazon EC2) instances, Amazon EC2 Dedicated Hosts, Amazon Elastic Block Store (Amazon EBS) volumes, and other cloud services. The calculator then estimates the cost of running these services each month. It is a great way for businesses to get an idea of their cloud budget as they plan how they want to structure their cloud usage.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **AWS has four different levels of support, ranging in price from free to greater than $15,000 a month. Why do you think AWS offers four levels of support? What do you think the key differences are between the four support plans?**
2.  **Organizations allows one account to control users, security settings, account access, and billing for any other linked accounts. Why do you think AWS provides this feature? What types of businesses do you think could best take advantage of this feature? Why?**

## 

****Activity: AWS Support Plans and AWS Organizations****

****Overview****  
In this activity, you will review the AWS Support plans to recommend the best plan for a given scenario. You will then review and take notes about the benefits of AWS Organizations.

**Objectives**

-   Recommend the best AWS Support plan for a given situation.
-   Identify the benefits of using Organizations and consolidated billing for cost savings and easier AWS Identity and Access Management (IAM) permissions management.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Why do you think AWS offers different support plans?**
-   **Do you think that the different levels of support AWS offers are adequate? Are there other things that customers might look for?**
-   **Do you think that using Organizations and consolidated billing is the best solution for all customers? For whom does it work best?**

## 

****Lab: AWS Simple Monthly Calculator****

**Overview**  
In this lab, you will use the Simple Monthly Calculator to estimate the cost of different cloud architectures.

**Objectives**

-   Use the Simple Monthly Calculator to estimate the cost of a cloud architecture.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

## 

**Additional connections**

-   AWS Organizations terminology and concepts ([https://docs.aws.amazon.com/organizations/latest/userguide/orgs_getting-started_concepts.html](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_getting-started_concepts.html))
-   AWS Organizations ([https://aws.amazon.com/organizations/](https://aws.amazon.com/organizations/))
-   Compare AWS Support Plans ([https://aws.amazon.com/premiumsupport/plans/](https://aws.amazon.com/premiumsupport/plans/))