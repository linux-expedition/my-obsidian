# Module 7: Security I
## **Module purpose**
In this module, you will get an overview of cloud security in relation to AWS Identity and Access Management (IAM). This includes information about best practices, roles, users, policies, and security groups.

## **Module description**
The activities in this module will have you taking notes on presented and gathered information. You will also have a discussion or role-play about the social impacts of cloud security. You will determine the process to resolve vulnerability in a web server.

## **Technology terminology**
[[AWS Terminology#Module 7 m7-security-I]]

## **Background and misconceptions**

Security is essential when using cloud resources for processing and storing data. Because databases, websites, and apps in the cloud can process sensitive information like banking and medical records, these cloud resources need to be restrictive of who can access them and what privileges they have while doing so.

IAM involves the application of controls to users who need access to computing resources. One company’s AWS account might have services that are being managed by dozens of different people who could be in different departments or offices, have different responsibilities or levels of seniority, and even be in different countries. To maintain a secure cloud environment with all these variables in play, it is crucial to maintain best practices for IAM.

**IAM best practices**

1.  **Lock away your AWS account root user access keys.**
    1.  The access key for your AWS account root user gives full access to all your resources for all AWS services, including your billing information. You cannot reduce the permissions associated with your AWS account root user access key. Therefore, protect your root user access key like you would your credit card numbers or any other sensitive secret.
2.  **Create individual IAM users.**
    1.  Don't use your AWS account root user credentials to access AWS, and don't give your credentials to anyone else. Instead, create individual users for anyone who needs access to your AWS account.
    2.  By creating individual IAM users for people accessing your account, you can give each IAM user a unique set of security credentials. You can also grant different permissions to each IAM user. If necessary, you can change or revoke an IAM user's permissions anytime.
3.  **Use user groups to assign permissions to IAM users.** 
    1.  Instead of defining permissions for individual IAM users, it's usually more convenient to create groups that relate to job functions (administrators, developers, accounting, etc.). Next, define the relevant permissions for each group. Finally, assign IAM users to those groups. All the users in an IAM group inherit the permissions assigned to the group. That way, you can make changes for everyone in a group in just one place.
4.  **Grant least privilege.**
    1.  When you create IAM policies, follow the standard security advice of granting least privilege, or granting only the permissions required to perform a task. Determine what users (and roles) need to do and then craft policies that allow them to perform only those tasks.
5.  **Get started using permissions with AWS managed policies.**
    1.  Providing your employees with only the permissions they need requires time and detailed knowledge of IAM policies. To get started quickly, you can use AWS managed policies to give your employees the permissions they need to get started. These policies are already available in your account and are maintained and updated by AWS.
    2.  AWS managed policies are designed to provide permissions for many common use cases. AWS managed policies make it easier for you to assign appropriate permissions to users, user groups, and roles than if you had to write the policies yourself.
6.  **Validate your policies.**
    1.  It is a best practice to validate the policies that you create. You can perform policy validation when you create and edit JSON policies. IAM identifies any JSON syntax errors, while IAM Access Analyzer provides over 100 policy checks and actionable recommendations to help you author secure and functional policies.
7.  **Use customer managed policies instead of inline policies.**
    1.  For custom policies, we recommend that you use managed policies instead of inline policies. A key advantage of using these policies is that you can view all of your managed policies in one place. Inline policies are policies that exist only on an IAM identity (user, user group, or role). Managed policies are separate IAM resources that you can attach to multiple identities.
8.  **Use access levels to review IAM permissions.**
    1.  To improve the security of your AWS account, you should regularly review and monitor each of your IAM policies. Make sure that your policies grant the least privilege that is needed to perform only the necessary actions.
9.  **Configure a strong password policy for your users.** 
    1.  If you allow users to change their own passwords, require that they create strong passwords and that they rotate their passwords periodically.
    2.  You can use the password policy to define password requirements, such as minimum length, whether it requires non-alphabetic characters, how frequently it must be rotated, and so on.
10.  **Enable MFA.**
    1.  Require multi-factor authentication (MFA) for all users in your account. With MFA, users have a device that generates a response to an authentication challenge. Both the user's credentials and the device-generated response are required to complete the sign-in process. If a user's password or access keys are compromised, your account resources are still secure because of the additional authentication requirement.
11.  **Use roles for applications that run on Amazon EC2 instances.**
    1.  Applications that run on an EC2 instance need credentials to access other AWS services. To provide credentials to the application in a secure way, use IAM roles. A role is an entity that has its own set of permissions, but that isn't a user or user group.
    2.  Roles also don't have their own permanent set of credentials the way IAM users do. In the case of Amazon EC2, IAM dynamically provides temporary credentials to the EC2 instance, and these credentials are automatically rotated for you.
12.  **Use roles to delegate permissions.**
    1.  Don't share security credentials between accounts to allow users from another AWS account to access resources in your AWS account. Instead, use IAM roles. You can define a role that specifies what permissions the IAM users in the other account are allowed. You can also designate which AWS accounts have the IAM users that are allowed to assume the role.
13.  **Do not share access keys.**
    1.  Access keys provide programmatic access to AWS. Do not share these security credentials between users in your AWS account. For applications that need access to AWS, configure the program to retrieve temporary security credentials using an IAM role.
14.  **Rotate credentials regularly.**
    1.  Change your own passwords and access keys regularly, and make sure that all IAM users in your account do as well. That way, if a password or access key is compromised without your knowledge, you limit how long the credentials can be used to access your resources. You can apply a password policy to your account to require all your IAM users to rotate their passwords. You can also choose how often they must do so.
15.  **Remove unnecessary credentials.**
    1.  Remove IAM user credentials (passwords and access keys) that are not needed.
16.  **Use policy conditions for extra security.**
    1.  Define the conditions under which your IAM policies allow access to a resource. For example, you can write conditions to specify a range of allowable IP addresses that a request must come from. You can also specify that a request is allowed only within a specified date range or time range. You can also set conditions that require the use of SSL or MFA (multi-factor authentication). For example, you can require that a user has authenticated with an MFA device in order to be allowed to terminate an Amazon EC2 instance.
17.  **Monitor activity in your AWS account.**
    1.  You can use logging features in AWS to determine the actions users have taken in your account and the resources that were used. The log files show the time and date of actions, the source IP for an action, which actions failed due to inadequate permissions, and more.

**AWS identities**

When thinking about IAM in AWS, there are roles, identities, and groups, all of which are governed by policies.

At the highest level is the _root user_. This is the identity that created the AWS account. The root user has access to every aspect of AWS and acts as a universal administrator. The root user credentials should never be given out, and it is not even recommended for the account creator to do everyday tasks as the root user. Instead, the root user account should be used to make an administrator account. Only a few tasks must be done as the root user, such as changing the AWS support plan or closing an account.

An _IAM user_ is an entity created in AWS. It represents the person using the AWS services and gives people the ability to sign in to AWS. A user will be assigned a name and password to access the AWS console. When creating a user, it is considered a best practice to assign them to a _group_ that has the appropriate permissions _policy_.

A _group_ is a collection of _IAM users_. You can use groups to specify permissions for a collection of users, which can make those permissions easier to manage for those users. For example, you could have a group called Admins and give that group the types of permissions that administrators typically need. Any user in that group automatically has the permissions that are assigned to the group. If a new user joins your organization and needs administrator privileges, you can assign the appropriate permissions by adding the user to that group. Similarly, if a person changes jobs in your organization, instead of editing that user's permissions, you can remove him or her from the old groups and add him or her to the appropriate new groups.

_IAM roles_ are similar to _users_ in that they are identities with permission policies that determine what the identity can and cannot do in AWS. However, a role does not have any credentials (password or access keys) associated with it. Instead of being uniquely associated with one person, a role is intended to be assumable by anyone who needs it. An IAM user can assume a role to temporarily take on different permissions for a specific task. Roles are useful in instances where a mobile app is accessing your AWS data. You don’t want every person who uses that app to have credentials for your AWS account, but they do need some access to the data when using the app. By assigning a role when the user logs in, they are granted temporary access with some permissions, but not permanent credentials.

Earlier, a _policy_ was mentioned in association with the permissions that a _group_ can be assigned. A policy, when attached to a _user_, _role_, or _group_, defines their permissions. Policies are stored in AWS as JSON documents. It is best practice to assign _policies_ to _groups_ and then assign each _user_ and _role_ to a group when created. This way, you can quickly remove or change permissions without having to modify each individual _user_ or _role_.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **What three things do you own that are most valuable to you? How do you secure each one? How can you secure something in multiple ways? How do you determine how secure something needs to be?**
2.  **What are some examples of places that have different levels of access based on who you are? What are some things that people can use to prove that they have access to places? Why are certain places restricted based on a person’s access level?**
3.  **Have you or someone you know ever had something stolen or broken into? How did it feel? Did it change how safe you felt you or your things were? How so? Did it change how you managed your security? How so?**

## 

****Activity 1: Overview of IAM****

****Overview****   
In this activity, you will find information related to IAM best practices and the meaning of roles, users, groups, and policies.

**Objectives**

-   Recognize best practices for IAM.
-   Differentiate among a role, user, and policy in cloud security.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Which best practice do you think is most important? Why?**

## 

**Activity 2: Cybersecurity and Society**

**Overview**   
You will find an article related to cybersecurity and society. Summarize it and discuss.

**Objective**

-   Analyze the cultural and societal impacts of cloud security.

### 

**Activity instructions**

1.  Navigate to a cybersecurity news website and find an article about cloud security and its impacts. Skim articles until you find one that is interesting and relevant to your learning.
2.  Read the article and note the title, author, and date and write a summary.
    1.  Answer the following questions:
        -   What was the impact (or what could have been the impact) of the cyberattack? Does the attack have an impact on society?
        -   Could the cyberattack have been avoided by following best practices? Which ones?
        -   How does the article that you selected relate to what you learned about IAM?
3.  After you have finished your research, share your findings with the class.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **How does the article that you selected relate to what you learned about IAM?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity+2+worksheet.docx.pdf)

## 

**Lab: Introduction to IAM**

**Overview**

In this lab, you will explore users, groups, and policies in the AWS Identity and Access Management (IAM) service.  

**Objective**

-   Differentiate among a role, user, and policy in cloud security.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Cloud security in society**

1.  Find a current event article about a cybersecurity attack related to stolen credentials or identity theft.
2.  Summarize the article.
3.  Determine what vulnerability permitted the attack to take place.
4.  Describe what could have been done to prevent the attack.

**Unplugged option**

1.  Select something that is valuable to you (offline).
2.  Make a list of five best practices for securing this valuable thing.

## 

****Additional connections****

-   AWS Cloud Security overview: [https://aws.amazon.com/security/](https://aws.amazon.com/security/)