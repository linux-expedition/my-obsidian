# Module 4: Virtual Servers
## **Module purpose**
In this module, you will learn how to create an Amazon Elastic Compute Cloud (Amazon EC2) instance and use it to host a website. You will also learn the purpose of access keys, a Domain Name System (DNS), Amazon Route 53, and Virtual Private Clouds.

## **Module description**
The activities in this module include hands-on work creating an EC2 instance, attaching an access key, accessing the instance using the command console, and creating a simple website and hosting it on the EC2 instance using an Amazon Simple Storage Service (Amazon S3) bucket.

## **Technology terminology**
[[AWS Terminology#Module 4 m4-virtual-servers]]

## **Background and misconceptions**

AWS Elastic Compute Cloud (EC2) service is one of the most basic and commonly used AWS cloud services. Fundamentally, it provides users with computing power in the cloud that can be used to complete tasks: anything from machine learning, to running applications, querying databases, and streaming video.

There are two types of websites: static and dynamic. Static websites require no actions to take place on the server. Dynamic websites require interactions with the server to respond to requests made on the client machines. In this module, you will need to create an S3 bucket, which is like a folder, to store your static website. You will be making your bucket public so that anyone can view the contents. This makes sense for a public website, but you will want to consider the privacy controls for different types of data. For instance, if you are storing a private journal in the cloud, you would not want to give the public access.

The object that controls the security and access permissions in AWS is called a policy. Policies are written in a programming language called JSON. You do not have to know how to write JSON for this module; you will be copying and pasting the code directly. You should be aware that JSON is the programming language used for AWS policies and works by allowing or denying actions or permissions.

You will use a text editor such as Notepad to create an index.html file that will serve as the code for your website. Similar to the bucket policy, you do not need to know HTML code because you will be copying and pasting. If you have experience with HTML, you can add code to customize your website.

To register a domain name for your website, you can use Route 53. Registering a domain name will cost money, so this will be optional, but you can still navigate to the service to see how it works.

You will also launch an EC2 instance that you can later use as a server to host your dynamic website.

In general, it is recommended that users create EC2 instances and S3 buckets within a VPC. The VPC is a private network through which the S3 buckets or EC2 instances will be accessed. Using a VPC allows you to strictly control who has access to your resources and from where they can get access. VPCs can be broken down into subnets to give even more granular control over access and security. A VPC is Region based.

![[m4-pic-0.png]]
![At the center of this diagram are two instances. Each instance is contained within a locked subnet. One subnet is labeled Availability Zone 1, and the other subnet is labeled Availability Zone 2. Both Availability Zones are contained within a VPC. The VPC is specific to a Region within AWS.](https://awsacademy.contentcontroller.com/vault/3b16dc09-d198-4a5f-bcb7-80d319c784ed/courses/7da8a56b-f46a-4284-bc92-cce658b6a998/0/assets/J0wgJRZGnarqFX1-_deHxVXUF8GQvbW-u.png)


![[m4-pic-1.jpg]]
![This diagram shows an EC2 instance within a VPC. The EC2 instance is connected to a router located outside the VPC but within AWS. The router is also connected to an internet gateway located at the border of AWS. This internet gateway is connected using the internet to other computers located outside AWS.](https://awsacademy.contentcontroller.com/vault/3b16dc09-d198-4a5f-bcb7-80d319c784ed/courses/7da8a56b-f46a-4284-bc92-cce658b6a998/0/assets/0ibhgmn79xW3Hvd-_7i71vt3bRxUvUh4L.jpg)

---

## ****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**

1.  ****Often, the purpose of a website (or app) is different to a user than to the creator.** For example, Google’s search engine provides a service to users by bringing them fast and effective search abilities. For Google, however, searches provide data about users that Google can analyze to present users with targeted advertisements. Think about a website (or app) that you use often. What is the purpose of the website (or app) to the user and the creator? Are these purposes similar or different?**
2.  **A website’s domain name is often our first impression of a website, even before we look at the content. Names like Wikipedia, Twitter, and Facebook evoke ideas for how they will be used. However, names like Google and Amazon don’t tell you much about what they are for. What factors do you think are important when naming a website and why? How does a website’s name impact the user’s experience and impressions of the website? When naming your own website, what are at least two factors that will be most important to you?**
3.  **Many websites store data about your usage of the website on your computer (called cookies) or on the website (called session variables). This data allows the website to not only personalize your usage, but also to learn about your patterns and history of usage. This means that websites can give you better recommendations and quickly auto-complete forms. However, it also means they can sell your information to advertisers. This can mean easier and more efficient access at the cost of privacy. When it comes to this type of data gathering, do you think the trade-off is worth it? Why or why not? Should websites have to be more transparent about what types of data they are gathering? Should you be able to opt out?**

---

## **Optional connection**

Follow your educator’s instructions for completing this activity.

1.  Brainstorm ideas for a personal website:
    1.  Topic ideas
    2.  Issues that interest you
    3.  Hobbies
    4.  Audience
    5.  Themes
    6.  Styles
2.  Create an outline for the content that would be on the website.
3.  Draw a rough draft of the website’s layout.

---
## ****Additional connections****

-   Amazon EC2: [https://aws.amazon.com/ec2/](https://aws.amazon.com/ec2/) 
-   Amazon S3: [https://aws.amazon.com/s3/](https://aws.amazon.com/s3/)
-   Uploading, Downloading, and Working with Objects in Amazon S3: [https://docs.aws.amazon.com/AmazonS3/latest/user-guide/upload-download-objects.html](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/upload-download-objects.html)
-   Tutorial: Configuring a Static Website on Amazon S3: [https://docs.aws.amazon.com/AmazonS3/latest/dev/HostingWebsiteOnS3Setup.html](https://docs.aws.amazon.com/AmazonS3/latest/dev/HostingWebsiteOnS3Setup.html)
-   What Is Amazon EC2?: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
-   Host a Static Website: [https://aws.amazon.com/getting-started/projects/host-static-website/faq/](https://aws.amazon.com/getting-started/projects/host-static-website/faq/)