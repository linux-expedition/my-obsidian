# Module 13: Emerging Technologies in the Cloud

Lesson 1 of 1

## **Module purpose**

In this module, you will research emerging technologies in the cloud with a focus on machine learning (ML) and its impacts on society, business, and technology.

## **Module description**

In this module, you will discuss the societal impacts of machine learning, read case studies about how machine learning is being used, and explore your own emerging technology and provide a use case for it.

## **Technology terminology**
[[AWS Terminology#Module 13 m13-emerging-technologies-in-the-cloud]]


## **Background and misconceptions**

As the programs that we create become more and more complex, they begin to resemble the inner workings of our own brains. With ML and deep learning, we can begin to develop software that learns, reasons, shows creativity, and even creates novel solutions to problems. These solutions can range from self-driving cars to making novel economic models.

For businesses, ML and AI can mean getting a head start on business intelligence and future-proofing a company. It can also be used to improve security, data analytics, and income projections.

There are multiple methods to approaching ML.

-   **Supervised ML:** Supervised ML starts with training data that includes the required output to adjust the ML algorithm. Supervised ML algorithms are divided into two categories, **classification** and **regression**:
    -   **Classification:** Classification algorithms examine an input and choose a response from specific preset choices. For example, an algorithm might be trained to classify emails as spam or not spam.
    -   **Regression:** Regression algorithms are trained to assign a value, or a number, to an input. For example, a weather prediction regression might be trained to give a predicted temperature for a given date in the future.
-   **Unsupervised ML:** Unsupervised machine learning starts with training data that does not include the desired output. Unsupervised machine learning algorithms can examine input and group related items together into groups called clusters. The clusters are not predefined or labeled, but are inferred by the algorithm during the training process.
-   **Semisupervised ML:** Semisupervised ML algorithms combine some features of supervised ML with some features of unsupervised ML. Usually semisupervised ML algorithms start training with a relatively small amount of labeled data and then analyze large amounts of unlabeled data to improve accuracy.
-   **Reinforcement ML:** With reinforcement learning, the algorithm receives feedback to guide it to the best outcome. Reinforcement learning allows the algorithm to develop complex behaviors by refining the output through trial and error. An example of reinforcement machine learning in action is an autonomous robot that learns to stay within a driving lane by receiving a reward for proceeding in the right direction or staying inside lane markers.

In its simplest form, machine learning is finding patterns in data and making predictions on future data based on those patterns. Then, it measures the accuracy of those predictions and repeats this thousands or millions of times to more accurately predict things like weather, media recommendations, and sports outcomes.

## ****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**

1.  **This module is about ML. What do you think of when you hear the words machine learning? What images does the name bring to mind? What do you predict ML is used for?**
2.  **ML is a subset of AI. Where have you heard references to AI being used in media? What are some real-world uses for AI?**
3.  **ML is a subset of AI in which a computer algorithm can modify its own behavior. AWS provides access to a service named SageMaker that supports ML. Why do you think the cloud is beneficial for ML and AI? What cloud services that you learned about previously do you think will be beneficial for ML and why?**

[](https://www.wired.com/2011/06/internet-a-human-right/)

## ****Activity 1: AI, Cloud Computing, and Society****

****Overview****  
In this activity, you will review resources, such as an article or video, about how AI currently or potentially could affect society.

**Objectives**

-   Define ML.
-   Discuss the impact of ML on cloud computing.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **What was the main idea or purpose of the article that you read or video you watched?**
-   **What is one example that your article or video gave for how AI can be used?**
-   **What was one positive impact and one negative impact that you read or watched about AI?**
-   **What is a rule or regulation that you would recommend that could help counter a negative impact?**
-   **How do you think AI will impact or be impacted by cloud computing?**

## 

**Activity 2: Emerging Technologies and the Cloud**

****Overview****  
In this activity, you will choose an emerging technology and research it with the goal of writing a movie pitch related to that technology. You will then write a short outline of the technology, including a use case and how it relates to or could be connected with cloud computing.

**Objective**

-   Identify potential use cases for emerging technology in the cloud.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **What emerging technology do you think will have the greatest impact? Why?** 
-   **What are some other future uses for the technologies you learned about?**
-   **Do any of the emerging technologies create problems or conflicts? For example, what if medical advances or lab-grown organs can save lives, but most people can’t afford them? Or what if people lose jobs because now robots are able to do them?** 

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**AWS SageMaker deep dive**

1.  Go to the AWS Academy Learner Labs environment.
2.  Navigate to the SageMaker service on the services menu.
3.  Follow the steps in the link to deploy your first ML model
    1.  [https://aws.amazon.com/getting-started/tutorials/build-train-deploy-machine-learning-model-sagemaker/](https://aws.amazon.com/getting-started/tutorials/build-train-deploy-machine-learning-model-sagemaker/)

**Unplugged option**

1.  Write a prediction for what emerging technologies are possible in cloud computing.
    1.  Choose a name for an Amazon Web Services (AWS) service that could come out in 20 years.
2.  Write a description of the new AWS service.
    1.  What does it do?
    2.  How does it work?
    3.  What problem does it solve?

## 

**Additional connections**

-   AWS DeepRacer ([https://aws.amazon.com/deepracer/](https://aws.amazon.com/deepracer/))
-   SageMaker ([https://aws.amazon.com/sagemaker/](https://aws.amazon.com/sagemaker/))
-   AWS DeepLens ([https://aws.amazon.com/deeplens/](https://aws.amazon.com/deeplens/))