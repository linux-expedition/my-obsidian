https://www.prepostseo.com/image-to-text

###### m1
- What are the benefits of cloud computing for business? (Select THREE) 
	- It facilitates you to get off the ground for cheap and only begin paying more as your business grows.
	- You do not have to predict how much storage or compute power you will need for your business.
	- When using cloud services, you can turn on new resources instantly whenever you need them.
- Which services does Amazon Web Services (AWS) offer? (Select THREE)
	- Amazon Elastic Compute Cloud (Amazon EC2): Compute capacity in the cloud
	- Amazon Simple Storage Service (Amazon S3): Storage for the Internet
	- Amazon CloudFront: A content delivery network (CDN)
- What is the definition of cloud computing?
	- The on-demand delivery of compute power, databases, storage, applications, and other IT resources using the internet with pay-as-you-go pricing
###### m2
- Which statements about AWS cloud structure are true? (Select THREE)
	- Data storage in a nearby Region is one of the reasons data can be accessed at lightning speed.
	- Edge locations are sites where data can be stored for lower latency.
	- An Availability Zone is a data center that houses many servers.
- What is the definition of latency?
	- The delay before a data transfer begins after the data has been requested
- What is the difference between platform as a service (PaaS) and software as a service (SaaS)?
	- PaaS provides a means for creating applications. SaaS is the application itself
###### m3
In which section of the AWS Management Console would you find Amazon Simple Storage Service (Amazon S3)?
	Storage
A company wants to keep track of all their customer transactions. They also want to be able to search through the transactions to find buying patterns to improve their advertising. Which Amazon Web Services (AWS) service will be most important for them to use?
	Amazon Relational Database Service (Amazon RDS)

- AWS Identity and Access Management (IAM)
	- Involves the application of controls to users who need access to computing resources.
- Amazon Simple Storage Service (Amazon S3)
	- A service provided by AWS that stores data for users in the cloud.
- Amazon Elastic Compute Cloud (Amazon EC2)
	- A web service that provides secure, resizable compute capacity in the cloud
- Virtual private cloud (VPC)
	- A virtual network dedicated to your AWS account.
- Amazon Elastic Block Store (Amazon EBS)
	- Storage for specific EC2 instances.

- Amazon DynamoDB
	- The AWS nonrelational database service
- Amazon CloudWatch
	- monitoring service to monitor your AWS resources and the applications that you run on AWS.
- AWS CloudTrail
	- A service to monitor and log every action that is performed on your AWS account for security purposes
- Amazon Redshift
	- The AWS data-warehousing service that can store massive amounts of data
- Amazon Relational Database Service (Amazon RDS)
	- This lets developers create and manage relational databases in the cloud.
- AWS Lambda
	- Lambda lets you run code without provisioning or managing servers
###### m4
- When creating an Amazon Simple Storage Service (Amazon S3) bucket to store data, what factors should be considered? (Select THREE) 
	- **Latency**
	- **Cost** 
	- **Frequency of access** 
	- Location
- Select the answer that best describes the function of a Domain Name System (DNS).
	- Connects a website's uniform resource locator (URL) and the actual IP address of the server where the data is located
- When starting an Amazon Elastic Compute Cloud (Amazon EC2) instance to host a website, which instance type should you use
	- General purpose
- An animation company wants to archive backups of its old films; the company occasionally needs to reuse some of these files. The company wants to save money where possible, but the files need to be quickly accessible when necessary. Which Amazon S3 storage standard is best for storing this archived content?
	- S3 Standard Infrequent Access
###### m5
- What are the benefits of using a content delivery network (CDN)? (Select THREE)
	- **Removes some of the data processing load from the origin server**
	- **Caches frequently accessed data for faster delivery**
	- Prevents servers from running out of storage space due to multiplication of data
	- **Permits global access to content using hundreds of edge locations around the world**
- What is the name of the AWS CDN service?
	- Amazon CloudFront
- What is the purpose of a CDN?
	- To process requests from users for web content from the origin server 
	- To protect the security of data in a specific Availability Zone
	- **To deliver web pages and other web content to users around the world with minimum latency and transmission time,**
	- To update the content on a website with the newest data on the origin server
- What is the definition of a distribution in relation to a CDN?
	- **Instructs CloudFront where to get the information that it caches in the edge locations and how to track and manage the content delivery.**
	- Provides CloudFront with the network capacity to deliver web content and data to users around the globe
	- Divides CloudFront traffic evenly among edge locations to maximize latency and increase throughput for all users
	- Organizes CloudFront data based on the frequency of access and stores the data in the proper instance type based on that information
###### m6
- What are benefits of using Amazon Elastic Block Storage (Amazon EBS)? (Select THREE) 
	- **Data encryption**
	- **Data availability**
	- Data transparency
	- **Data persistence**
- Which are EBS volume types? (Select THREE)
	- **Cold HDD (sc1)**
	- Balanced ROR SSD (br1)
	- **Throughput-optimized HDD (st1),**
	- **General-purpose SSD (gp2)**
- What should you consider when selecting the Availability Zone for an Amazon Elastic Block Store (Amazon EBS) volume?
	- **The Availability Zone should be the same as the Amazon Elastic Compute Cloud (Amazon EC2) instance it is being attached to.**
	- The Availability Zone should be where you were when you created the EC2 instance. 
	- The Availability Zone should be the location that is closest to the majority of Amazon EC2 users.
	- The Availability Zone should be whichever is the default selection.
- What are the two major types of storage drives for EBS volumes?
	- Solid state drive (SSD) and operational drive
	- Automatic drive and hard disk drive (HDD),
	- Operational drive and automatic drive
	- **SSD and HDD**
###### m7
- Which of the following are AWS Identity and Access Management (IAM), best practices for Amazon Web Services (AWS)? (Select THREE)
	- **Enable multi-factor authentication (MFA).**
	- **Use policy conditions for extra security.**
	- **Use groups to assign permissions to IAM users.**
	- Share access keys with stakeholders.
- According to the AWS best practices, which of the following is true about the AWS account root user?
	- The root user should be used for most tasks because it has the most privileges.
	- **The root user should not be used for everyday tasks because it is a security vulnerability.**
	- The root user should be used only for administrative tasks because it is a security vulnerability.
	- The root user cannot create new users because it does not have the required privileges.
- What is the difference between a role and a user in reference to IAM?
	- A role is generally used for desktop access, whereas a user is for mobile access.
	- A role is a permanent identity, whereas a user is temporary.
	- **A role does not have credentials associated with it, whereas users do.**
	- A role is associated with one person, whereas a user is associated with many.
- What IAM feature in AWS lets you change the policy for multiple users all at once?
	- Teams
	- Credentials
	- Roles
	- **Groups**
###### m8
- Which statement best contrasts AWS Shield and AWS WAF?
	- Shield is for providing insights into best practices, whereas AWS WAF is for compliance programs.
	- Shield is for filtering specific web traffic, whereas AWS WAF is for stopping, distributed denial of service (DDoS) attacks.
	- **Shield is for stopping DDoS attacks, whereas AWS WAF is for filtering specific web traffic.**
	- Shield is for compliance programs, whereas AWS WAF is for providing insights into best practices.
- A doctor's office wants to make sure its staff meets all compliance regulations as they store sensitive patient data in the cloud. Which cloud service would best meet this need? 
	- Amazon Inspector
	- **AWS Artifact**
	- AWS WAF
	- Shield
- Which cloud service would best serve a security administrator who wants to block all traffic from a specific IP address?
	- **AWS WAF**
	- Amazon Inspector
	- Shield 
	- AWS Artifact
- What is the purpose of Amazon Inspector?
	- To assess cloud services and protect against DDoS attacks
	- To assess cloud services and automatically address security lapses
	- To assess dloud services and report on performance metrics
	- **To assess cloud services and provide reports on security vulnerabilities**
###### m9
- What is the main difference between AWS Cloud Trail and Amazon CloudWatch?
	CloudTrail is a logging service, whereas CloudWatch is mainly a monitoring service.
- Which two AWS services together send you alerts when an alarm based on performance metrics is initiated? 
	CloudWatch and Amazon SNS
- What are the steps for setting up Amazon SNS to send out messages?
	Create a topic and subscribe a user to that topic.
###### m10
- Which scenarios would best be served using OLTP? (Select TWO)
	- **An online store records the items that a user has added to their shopping cart.**
	- A company compares their shoe sales in August with sales in November, then compares those results with another location's.
	- Amazon finds patterns in customers' purchases to come up with personalized, recommendations for products.
	- **An online shopping site needs to make sure that updates and deletions to one of their tables do not make other tables not valid.**
- When deciding between relational and nonrelational databases, which features can help you decide which type of database to implement? (Select THREE)
	- **Speed of data retrieval**
	- **Speed of updates and deletions**
	- **Consistency and integrity of the data when updating**
	- Use of key-value pairs
- Which best describes the purpose of online transaction processing (OLTP)?
	- **Inserting, updating, or deleting small amounts of data in a database**
	- Selectively extracting and querying data to analyze it from different points of view 
	- Deploying and scaling web applications and services developed in a range of programming languages
	- Storing data in a way that is secure, scalable, and highly available,
- Which statement is true about relational and nonrelational databases?
	- Relational databases are best suited for analytic processing and nonrelational, databases are best suited for transaction processing,
	- Relational databases are best suited for processing extremely large datasets and nonrelational databases are best suited for small datasets.
	- **Relational databases are best suited for transaction processing and nonrelational databases are best suited for analytic processing,**
	- Relational databases do not use SQL and nonrelational databases use SQL.
###### m11
- When creating a load balancer for a website, which Amazon Web Services (AWS) service will you navigate to first?
	- Amazon DynamoDB
	- Amazon Simple Storage Service (Amazon S3)
	- **Amazon Elastic Compute Cloud (Amazon EC2)**
	- Amazon Relational Database Service (Amazon RDS)
- What are ways that you can evaluate the performance of a load, balancer? (Select THREE)
	- **Use Amazon CloudWatch to monitor the number of requests to the Amazon Elastic Compute Cloud (Amazon EC2) instance.**
	- Use Amazon Inspector to evaluate the vulnerability of the load balancer.
	- **Track the latency of the EC2 instance.**
	- **Navigate to the domain name of the load balancer and check if you see the default page of your server.**
- What are the benefits of using Amazon ElastiCache to cache data? (Select THREE)
	- **It stores data in memory for extremely fast performance.**
	- **It is fully managed so you can focus on developing the application.**
	- It instantly updates to match any changes made on the virtual instance.
	- **It automatically scales to meet the needs of the application.**
- What is the difference between scaling out and scaling up?
	- Scaling out adds more storage to existing EC2s; scaling up adds more EC2 instances
	- **Scaling out adds more EC2 instances, scaling up adds more storage to existing EC2 instances.**
	- There is no difference; they both do the same thing.
	- Scaling out reduces the number of EC2 instances; scaling up adds EC2 instances.
- What is the main benefit of using a load balancer?
	- **It automatically distributes incoming traffic across multiple targets to reduce 'server latency and increase stability.**
	- It lets you assess and evaluate the configurations of your AWS resource to reduce the chance of failure.
	- It automatically adjusts capacity to maintain steady, predictable performance at the lowest possible cost.
	- It provides a centralized resource for compliance-related information to maintain security standards.
###### m12
- What is the purpose of AWS Elastic Beanstalk?
	- To provide object storage service that offers scalability, data availability, security, and performance
	- To securely deliver data, videos, applications, and application programming interfaces (APIs) to customers globally with low latency and high transfer speeds, all within a developer-friendly environment
	- To give developers and businesses an easy way to create a collection of related Amazon Web Services (AWS) resources and provision them in an orderly and predictable fashion
	- **To automatically handle the deployment details of capacity provisioning, load balancing, automatic scaling, and application health monitoring of an existing application**
- What are the benefits of AWS CloudFormation? (Select THREE),
	- **It lets you build and rebuild your infrastructure and applications without having to perform manual actions or write custom scripts.**
	- It lets you automatically run your code without requiring you to provision or manage servers.
	- **It lets you model your entire infrastructure and application resources with a text file or programming languages.**
	- **It lets you treat your infrastructure as code.**
- What is the purpose of CloudFormation?
	- **To give developers and businesses an easy way to create a new application from a collection of related AWS resources and provision them in an orderly and predictable fashion**
	- To automatically distribute incoming application traffic across multiple targets, such as Amazon Elastic Compute Cloud (Amazon EC2) instances, containers, IP addresses, and Lambda functions
	- To help protect your web applications from common web exploits that might affect application availability, compromise security, or consume excessive resources
	- To automatically handle the deployment details of capacity provisioning, load balancing, automatic scaling, and application health monitoring of an existing application
###### m13
###### m14
###### m15
###### m16
###### semester 1



Selamat pagi pak
Iya pak, berkaitan dengan akan diadakannya MCF Offline. Ada kegiatan pengenalan prodi yang merupakan bagian dari acara fakultair pak.
Dan dari kepanitiaan menyerahkan konsepnya talk show/pameran/kegiatan khusus lainnya kepada kaprodi. Dan untuk durasi kegiatannya 1 jam setengah.
Jadi, cuma mau konfirmasi kalau nanti sewaktu fakultair, pengenalan prodinya apakah berupa talk show ya pak?