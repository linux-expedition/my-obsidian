# Module 5: Content Delivery
## **Module purpose**
The purpose of this module is to learn about the Amazon Web Services (AWS) content delivery network (CDN), Amazon CloudFront. You will learn key vocabulary related to content delivery and understand why having a CDN is important.

## **Module description**
In this module, we will discuss the importance of edge locations and caching for a CDN. You will then create a CloudFront distribution and attach it to an Amazon Simple Storage Service (Amazon) S3 bucket that contains a Hypertext Markup Language (HTML) website file.

## **Technology terminology**
[[AWS Terminology#Module 5 m5-cloud-front]]

## **Background and misconceptions**
Due to the nature of the web, certain content is requested more frequently and is expected to be delivered almost instantaneously. A website whose origin is on a server in California might host something that goes viral in Sweden. In this case, there will be a sudden rush of requests for that data, which could lead to increased latency and even crash the website if the server cannot handle the traffic.

CloudFront works with the edge locations that are part of the AWS Global Infrastructure. Together, they facilitate frequently requested data being cached in the edge locations. While the initial request prompts CloudFront to load the file into the cache, subsequent requests can be fulfilled much more quickly, and some of the work can be offloaded from the origin server.

This process can also be seen at a more local level on something like the Amazon.com front page. Products that are being shown on the front page can be cached because every person that visits the Amazon.com site will automatically request those products from the server, which could lead to a slow down. Caching permits these front-page products to be stored in edge locations for faster access. The rest of the products can remain stored on the origin server because they will be requested less often and can tolerate a slightly higher latency.

Caching in edge locations is not permanent, and all cached data has a time to live (TTL), which is the length of time that it is cached for.

## ****Focus questions****
Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**

1. **Have you ever tried to access a webpage, stream a video, or download a file, and it didn’t work or it worked too slowly? What was it? How did this make you feel? Why do you think this happens?**

2. **What does the term _net neutrality_ mean? How does this term relate to a CDN and CloudFront?**

3. **Should internet access be a human right? Why or why not? Should the government be permitted to restrict certain webpages or online content? Why or why not?**

[](http://news.bbc.co.uk/2/hi/8548190.stm)

4. **What advantages does internet access give a student over a student without internet access? Are there any advantages to not having internet access?**

## **Activity: Content Distribution**

****Overview****  
You will research information related to a CDN. Then discuss and/or present what you find.

**Objectives**

-   Recognize benefits of a CDN.
-   Explain the uses of a CDN.

### **Activity instructions**
Open the Content Distribution worksheet and follow the instructions to complete the activity.

### **Reflect**
Following your educator’s directions, prepare to ask questions about anything you did not find or did not understand in an open forum discussion with your classmates.

## **Lab: **Using CloudFront as a CDN for a Website****

**Overview**  
You will use CloudFront to create a content distribution network (CDN) for a website that is stored in Amazon S3.

**Objective**

-   Configure a CloudFront distribution and attach it to a website.

**Lab instructions**

Follow your educator's instructions to complete the lab.

### ****Reflect****

After completing the lab, be prepared to answer and discuss these questions.

-   **Were there any steps in creating an S3 bucket or attaching a CloudFront distribution that need further explanation? Where do you think you can look to find out more about that part of the process?**
-   ****Why would having a cloud distribution network like CloudFront be important for a video streaming or audio streaming company, such as Hulu or Spotify?****
-   **Based on what you have learned about cloud distribution networks, what will you think or feel when a website is responding slowly or a video is taking a long time to buffer?**
-   **What types of data do you think are most important to have cached for quick distribution?**

## **Optional connection**

Follow your educator’s instructions for completing this activity.

**CloudFront case studies**

1.  Navigate to the CloudFront case studies webpage: [https://aws.amazon.com/cloudfront/case-studies/](https://aws.amazon.com/cloudfront/case-studies/)
2.  Pick a company and write a short summary of how the company uses CloudFront.

**Unplugged option**

1.  Select a company that can benefit from using CloudFront.
2.  Then make suggestions for:
    1.  How the company would benefit from using CloudFront
    2.  What type of data the company would distribute with CloudFront

## **Additional connections**

Here are some additional resources for better understanding the material presented in this module.

-   [How to Set up an Amazon CloudFront Distribution for Your Amazon S3 Origin](https://www.youtube.com/watch?v=KIltfPRpTi4)