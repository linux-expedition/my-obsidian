# Module 9: Monitoring the Cloud
## **Module purpose**
The purpose of this module is for you to learn about the tools that Amazon Web Services (AWS) provides for monitoring cloud services. These include AWS Config, AWS CloudTrail, and Amazon CloudWatch.

## **Module description**
It is important when operating in the cloud to keep track of activities, because there is probably a cost associated with each one. AWS assists with monitoring, logging, and reporting on the usage of its services by providing tools to do so. You will explore these tools in this module.

In the lab, you will set up an alert using CloudWatch and monitor CloudTrail log files. You will also discuss the different uses of CloudTrail and CloudWatch. Lastly, you will determine which services are best for a given scenario.

## **Technology terminology**
[[AWS Terminology#Module 9 m9-monitoring-the-cloud]]

## **Background and misconceptions**
AWS provides many interconnected services that provide a complex basis with which to accomplish tasks in the cloud. As businesses grow, they can be running multiple interconnected AWS accounts. Each account might run dozens of instances that process thousands of gigabytes of data, serving millions of people and representing billions of dollars. No matter a company's size, it is essential to monitor and track your cloud services. This helps you to ensure that all of the cloud assets are running smoothly and to be aware if anything out of the ordinary occurs.

AWS provides powerful tools to monitor all of the cloud services. These tools work together to provide a suite of services that empower cloud users with knowledge.

**CloudWatch** is a monitoring service to monitor your AWS resources and the applications that you run on AWS.

**CloudTrail** and **CloudWatch** are both cloud-monitoring services, but they perform different functions:

-   **CloudTrail** monitors and logs all the actions that users have taken in a given AWS account. This means that CloudTrail logs each time someone uploads data, runs code, creates an Amazon Elastic Compute Cloud (Amazon EC2) instance, or performs any other action.
-   **CloudWatch** monitors what all the different services are doing and which resources they are using. CloudTrail logs activities, whereas CloudWatch monitors activities. CloudWatch helps you make sure that your cloud services are running smoothly. The services can also help you to not use more or fewer resources than you expect, which is important for budget tracking.

**AWS Config** is a service that lets you assess, audit, and evaluate the configurations of your AWS resources. AWS Config continuously monitors and records your AWS resource configurations and lets you automate the evaluation of recorded configurations against desired configurations.

![[m9-pic-0.jpg]]
Image source: [https://aws.amazon.com/config/](https://aws.amazon.com/config/)

Configuration change occurs in your AWS resources. AWS Config records and normalizes the changes into a consistent format. AWS Config automatically evaluates the recorded configurations against the configurations you specify. Access change history and compliance results using the console or APIs. CloudWatch Events or SNS alert you when changes occur. Deliver change history and snapshot files to your S3 bucket for analysis.

**Amazon SNS** is how AWS communicates within the cloud and with the outside world. When an event is initiated or a program alerts AWS to send out notifications, Amazon SNS sends the messages to users or other AWS services.

## ****Focus questions****
Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**
1.  **What tools do you use to stay organized and keep track of your life, work, and schedule? Why are these tools important? What kinds of tools would be helpful to monitor or keep track of your resources in the cloud?**
2.  **Have you ever missed or been late to an event you had scheduled, or forgotten an assignment? What happened? How might you have prevented the error? Do you think a similar error might happen when using cloud services with AWS? How might this be prevented?**
3.  ****A cell phone company uses AWS to let users download mobile apps that let them print remotely from their devices. What data points do you think this company needs to keep track of in their cloud services? Why?****

## ****Activity: CloudTrail, CloudWatch, and AWS Config****

****Overview****  
In this activity, you will compare CloudTrail and CloudWatch. You will also learn about AWS Config.

**Objective**

-   Compare CloudTrail and CloudWatch.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### **Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Compare and contrast CloudWatch, CloudTrail, and AWS Config. How are they similar? How are they different? Give an example of when each service would be the most useful.**

## ****Lab: Creating a CloudWatch Alarm That Initiates an Amazon SNS Message****

**Overview**  
In this lab, you will create an Amazon CloudWatch alert that sends a message to Amazon SNS to send an email or text when the account has spent over a certain amount of money.

**Objective**

-   Use CloudWatch to set up a text alert event.

### ****Lab instructions****

Follow your educator’s instructions to complete the lab.

### *Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **Why do you think it is best to create a topic and subscribe to it rather than just having the alarm send the message directly to you?**
-   **Other than billing metrics, you can also set alarms based on different AWS service metrics. What types of events do you think would initiate an alarm for EC2? How about S3?**
-   **What types of alert metrics might you set for your own website? What would you want to be aware of?**
-   **How could this process be used to make your AWS account more secure?**

## **Optional connection**

Follow your educator’s instructions for completing this activity.

**CloudTrail**

1.  Log in to your AWS account.
    1.  Find the CloudTrail service and select it.
2.  Follow these steps to create a trail on your Amazon Simple Storage Service (Amazon S3) bucket.
    1.  [https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-create-a-trail-using-the-console-first-time.html](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-create-a-trail-using-the-console-first-time.html)
    2.  This will track all of the actions that are done with that S3 bucket.

**Unplugged option**

1.  Write a personal CloudTrail of your day.
2.  Think about what you did, where you went, and when.
    1.  Be as specific as possible.
3.  Afterward, read through it or share it with a family member or friend.
    1.  Did anything surprise you?

## **Additional connections**

-   AWS Config ([https://aws.amazon.com/config/](https://aws.amazon.com/config/))
-   AWS CloudTrail ([https://aws.amazon.com/cloudtrail/](https://aws.amazon.com/cloudtrail/))
-   Amazon CloudWatch ([https://aws.amazon.com/cloudwatch/](https://aws.amazon.com/cloudwatch/))
-   Amazon Simple Notification Service ([https://aws.amazon.com/sns/](https://aws.amazon.com/sns/))


