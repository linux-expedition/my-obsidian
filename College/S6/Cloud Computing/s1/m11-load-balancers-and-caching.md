# Module 11: Load Balancers and Caching
## **Module purpose**

In this module, you will learn the purpose of Amazon ElastiCache and the benefits of caching data. You will also learn about Elastic Load Balancing.

## **Module description**

You will use your knowledge of ElastiCache and ELB to create an advertisement for the services. You will use the AWS Management Console to create and configure a load balancer for a website.

## **Technology terminology**
[[AWS Terminology#Module 11 m11-load-balancers-and-caching]]

## **Background and misconceptions**

Of the many ways to handle data on a computer, one of the most common is read-only data that needs to be presented quickly and to a large number of users, such as music or videos that are streamed to the world. This type of data is rarely updated or deleted, but there is a large volume of it, and the demand for it can fluctuate dramatically (think of a video or song that goes viral). Because the need for this type of access is becoming so popular, Amazon Web Services (AWS) provides tools for handling it. The tools are mainly ones that can retrieve data rapidly and distribute data across multiple servers in response to peaks and valleys of demand—and do it in a cost-effective way that only charges for usage.

Applications and websites often provide a wide range of data and services to users. Within this wide range of data, there is often a smaller subset of data that is requested and accessed more often. This might be the data on the front page that is shown to every visitor (think Amazon’s top 10 products of the day) or it might be a recently released piece of media that is having a spike in popularity (a new song released on Spotify).

Other applications run processes that are extremely memory intensive that might suffer from performance problems on a slower storage drive. 

For this type of heavily requested or memory-intensive data, a data caching service such as ElastiCache can help to ensure that the data can be accessed and processed extremely quickly. It works by storing the data in extremely fast but temporary memory that is faster than disk-based storage. The trade-off is that the fast memory has less storage space and does not store the data permanently.

Many companies use ElastiCache to build real-time apps, speed up ecommerce, and cache their websites.

![[m11-pic-0.jpg]]
Image source: [https://aws.amazon.com/elasticache/](https://aws.amazon.com/elasticache/)
Internet-scale applications: Real-time apps in gaming, ride hailing, media streaming, dating, and social media need fast data access. Amazon ElastiCache: Blazing fast in-memory data store for use as a database, cache, message broker, and queue. Store ephemeral data in-memory for sub-millisecond response. Use cases: real-time transactions, chat, BI and analytics, session store, gaming leaderboards, and cache.

Heavy traffic can shut down apps and websites if the server cannot handle the load. This is why AWS has ELB, which can detect when there are too many requests and automatically divert traffic into a new server to maintain speed and stability. There are three types of ELB in AWS.

**Application Load Balancer:** Application Load Balancer is best suited for load balancing of Hypertext Transfer Protocol (HTTP) and Secure HTTP (HTTPS) traffic and provides advanced request routing targeted at the delivery of modern application architectures, including microservices and containers. Operating at the individual request level (Layer 7), Application Load Balancer routes traffic to targets within Amazon Virtual Private Cloud (Amazon VPC) based on the content of the request.

Application Load Balancer balancing is done based on the content of the uniform resource locator (URL). For example, if the URL ends in _/main_, the request will be routed to one instance; if the URL ends in _blog/_, it will be routed to a different instance if the Application Load Balancer definition work has been done in advance to make this happen.

**Network Load Balancer:** Network Load Balancer is best suited for load balancing of Transmission Control Protocol (TCP), User Datagram Protocol (UDP), and Transport Layer Security (TLS) traffic where extreme performance is required. Operating at the connection level (Layer 4), Network Load Balancer routes traffic to targets within Amazon VPC and is capable of handling millions of requests per second while maintaining ultra-low latencies. Network Load Balancer is also optimized to handle sudden and volatile traffic patterns.

Because of the increased speed that can be achieved at the connection layer, the Network Load Balancer type of load balancing is more desirable when trying to avoid higher volumes of network traffic. For example, to avoid delay when interest in a website goes viral, you would choose to use Network Load Balancer balancing.

**Classic Load Balancer:** Classic Load Balancer provides basic load balancing across multiple EC2 instances and operates at the request and connection levels. Classic Load Balancer is intended for applications that were built within the EC2-Classic network.

![[m11-pic-1.png]]
Image source: [https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/introduction.html](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/introduction.html)
Three computers are accessing content in the AWS Cloud. A load balancer splits this access between Availability Zone A and Availability Zone B. Each Availability Zone has three EC2 instances, but one instance in Availability Zone A is not functioning.


## ****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### **Questions**

1.  **Is there anything you have done so often that it has become automatic for you or you can do it without thinking? What actions fit this category? How do you think this relates to data caching?**
2.  **This module is about load balancing. What strategies or tools do you use to balance your responsibilities and life? Why is it important to have a way to maintain balance?**
3.  **Data caching is crucial for parts of websites and apps that need to be processed or retrieved very quickly. Remember that because the cache is a snapshot of the data on a server, it is not updated immediately when the data changes. What are some examples of data in websites or apps that you think should be cached? Why?**

## ****Activity: ElastiCache and ELB Advertisement****

****Overview****   
In this activity, you will create a poster, slide deck, or script that advertises the benefits of ElastiCache and ELB.

**Objectives**

-   Describe features and benefits of load balancing.
-   Describe the benefits of caching data.
-   Explain the purpose of ElastiCache.

### **Activity instructions**

1.  Your advertisement must include explanations of what ElastiCache and ELB are and how they work. It should also describe the benefits of the services and how they would benefit a business. They should, of course, be persuasive as well.
2.  Your educator will give you time to research the two services. There will be an opportunity to discuss and ask questions following.
3.  Talk with your group and decide what type of advertisement you want to create. Choose from the following:
    -   Slide deck
    -   Poster
    -   A script for a commercial
    -   Perhaps another option, if approved by your educator
4.  Share your advertisement with the class.

## ****Lab: Using Load Balancers****

**Overview**  
In this activity, you will create and configure a load balancer, register a webpage as a target for the load balancer, and test the load balancer.

**Objectives**

-   Register a webpage as a target for a load balancer.
-   Evaluate the performance of a load balancer.

### **Lab instructions**

Follow your educator’s instructions to complete the lab.

### **Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **When creating a load balancer, why is it important to select multiple Availability Zones?**
-   **Why do you think an Application Load Balancer is important for highly trafficked web applications?**
-   **If you created a website, would you use an Application Load Balancer? Why or why not?**
-   **What services would let you monitor the effectiveness of your load balancer?**
-   **What is the difference between load balancing and automatic scaling?**

## **Optional connection**

Follow your educator’s instructions for completing this activity.

**ElastiCache case studies**

1.  Navigate to the Amazon ElastiCache Customers website: [https://aws.amazon.com/elasticache/customers/](https://aws.amazon.com/elasticache/customers/)
2.  Choose **Amazon ElastiCache for Redis Customers** or **Amazon ElastiCache for Memcached Customers**.
3.  Select a business that interests you, and learn about how that business uses Amazon ElastiCache.
4.  Summarize how that business uses ElastiCache.
    1.  Try to simplify the language so that someone new to AWS would understand.

**Unplugged option**

1.  Select a business that you know of or think of one that you would like to start.
2.  Write about how ElastiCache and ELB would benefit that business.

## **Additional connections**

-   Elastic Load Balancing ([https://aws.amazon.com/elasticloadbalancing/](https://aws.amazon.com/elasticloadbalancing/))
-   Caching Overview ([https://aws.amazon.com/caching/](https://aws.amazon.com/caching/))
-   Amazon ElastiCache ([https://aws.amazon.com/elasticache/](https://aws.amazon.com/elasticache/))