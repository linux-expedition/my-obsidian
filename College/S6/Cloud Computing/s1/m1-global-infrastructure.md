# Module 1: Global Infrastructure

Lesson 1 of 1

## 

**Module purpose**

The purpose of this module is to review the basics of cloud computing. It will cover the benefits of computing in the cloud and the reasons why companies have begun to switch from on-premises computing to cloud computing. It will also touch on the main services offered by cloud computing providers.

## 

**Module description**

In this module, you will take notes about the benefits of cloud computing and the services offered by cloud providers. We will discuss some of the impacts of cloud computing.

## 

**Technical terminology**

Cloud computing

Click to flip

The on-demand delivery of compute power, databases, storage, applications, and other IT resources using the internet with pay-as-you-go pricing.

Click to flip

Amazon Web Services (AWS)

A platform that provides a wide range of cloud computing services.

Cloud storage

Saving data using a cloud service provider (CSP) instead of a physical machine.

Server

A computer designed to process requests and deliver data to another computer over the internet or a local network. In the cloud, a server is hosted by an outside provider, which is accessed over the internet.

## 

**Background and misconceptions**

**What is cloud computing?**  
Any time you are working or storing information online (for example, sending an email or watching a streaming video)—as opposed to on your local computer or on a server on your local network—you are using cloud computing.

**Why do businesses use cloud computing?**  
The business benefits of cloud computing include the following:

-   Pay less to get your business started. Pay more as your business grows.
-   Services are cheaper because costs are spread across many users.
-   Your computing power and storage scales to fit what you need, so you only pay for what you use.
-   It is faster and easier to add new resources when you need them.
-   Cloud providers maintain, secure, and run the computers and facilities for cloud services.
-   It is easy to release your application or advertise anywhere in the word because everything is online.

**What types of cloud services are there?**

Type of Cloud Service | What It Does | Example
--- | ---- | -----
Infrastructure as a service (IaaS) | Compute power, networking, and storage provided over the internet | Amazon Elastic Compute Cloud (Amazon EC2), Rackspace, Google Compute Engine
Platform as a service (PaaS) | Tools provided over the internet for making programs and applications | AWS Elastic Beanstalk, Microsoft Azure, Google App Engine
Software as a service (SaaS) | Applications and programs that are accessed and provided over the internet | Dropbox, Slack, Spotify, YouTube, Microsoft Office 365, Gmail

**How did AWS get started?**

-   Origins began in 2002 when Amazon started the Amazon.com web service.
    -   Offered tools for developers to work on the Amazon product catalog
-   In 2003, Amazon realized that its infrastructure services could give them an advantage over the competition.
    -   Provided hardware power, storage, and databases along with the software tools to control them
-   In 2004, Amazon publicly announced that it was working on a cloud service.
-   In 2006, Amazon launched AWS with just a few of the services that are still around today.
    -   Amazon Simple Storage Service (Amazon S3)
    -   Amazon EC2 
    -   Amazon Simple Queue Service (Amazon SQS)
-   By 2009, AWS added more services
    -   Amazon Elastic Block Store (Amazon EBS)
    -   Amazon CloudFront – a content delivery network (CDN)
-   AWS has developed partnerships with several large companies. AWS has been growing and adding new services and tools ever since.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the focus questions.

### 

**Questions**

**1. Imagine if one of your social media accounts was hacked and all your data was made public or held for ransom. How would this make you feel? Do you think the trade-off is worth the risk to have all the cloud services at your fingertips?**

**2. What kind of information do you have stored online? What are the risks of that information being compromised or shared without your consent? What kinds of laws or regulations do you think are necessary to keep your information safe?**

****3. What are some ways that the internet has made your life easier? What are some ways that the internet has made your life more difficult? What is one thing you wish you could do online, but the technology doesn’t exist yet?****

## 

****Activity 1: Introduction to Cloud Computing****

****Overview****  
In this activity, you will work in pairs to take notes about the basics of cloud computing. You will come up with sources to find definitions for key terms and list the benefits that cloud computing offers. You will also find the key services offered by cloud computing providers and give examples of how these services are used in industry.

**Objectives**

-   Define cloud computing and its impacts.
-   Identify the benefits of cloud computing.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **What is one way that cloud computing has impacted society as a whole?**
-   **Was any of the information surprising or unexpected?**
-   **What are some of the sources in which you found your information, and what led you to believe these sources are credible and accurate?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity1+worksheet.docx.pdf)

## 

**Activity 2: Using Cloud Services**

**Overview**  
In this activity, you will generate cloud service usage plans for a number of business case studies. For each business, you will describe how each of the four services you learned about can be used to improve or benefit the business.

**Objective**

-   Compare the major services offered by cloud computing providers.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **What are the main differences in the ways that the businesses are using the services?**
-   **Does one of the services stand out to you as being the most important? If so, why?**
-   **If a close friend or family member were starting a business and wanted to use cloud services, what advice would you give?** [](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity+2+worksheet.docx.pdf)

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Real-world business connections**

1.  You can begin to look at real-world case studies of companies that rely on AWS here: AWS Customer Success: [https://aws.amazon.com/solutions/case-studies/](https://aws.amazon.com/solutions/case-studies/)
2.  Pick a company and write a short summary of how cloud computing permitted the company to do something that would otherwise not have been possible.

**Unplugged option**

1.  Select a company that might benefit from cloud services.
2.  Then make suggestions for:
    1.  Which CSP the company should use and provide reasoning
    2.  Which cloud services the company could use and how the services would benefit them

## 

**Additional connections**

-   [What is Cloud Computing?](https://aws.amazon.com/what-is-cloud-computing/)[](https://searchcloudcomputing.techtarget.com/definition/cloud-computing)[](https://uknowit.uwgb.edu/page.php?id=30276)[](https://aws.amazon.com/education/awseducate/14-and-older/)
-   [AWS Free Tier](https://aws.amazon.com/free)