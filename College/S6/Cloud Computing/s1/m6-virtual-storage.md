# Module 6: Virtual Storage
## **Module purpose**
In this module, you will receive an overview of Amazon Elastic Block Store (Amazon EBS). You will compare Amazon EBS and Amazon Simple Storage Service (Amazon S3). Students will learn about different storage tiers and how to choose the best type of storage for a given scenario.

## **Module description**
You will discuss the benefits and features of Amazon EBS. Then you will analyze real-world case studies related to Amazon EBS. Finally, you will create their own EBS volumes and attach them to an Amazon Elastic Compute Cloud (Amazon EC2) instance.

## **Technology terminology**
[[AWS Terminology#Module 6 m6-virtual-storage]]

## **Background and misconceptions**

Amazon EBS is storage for EC2 instances with major benefits:

-   Data availability
-   Data persistence
-   Data encryption
-   Snapshots

Amazon EBS storage is implemented as a series of fixed-length blocks that can be read and written by the operating system. There is nothing stored about what these blocks represent or their attributes. The blocks are very much like the New Technology File System (NTFS) or File Allocation Table (FAT) file systems that run on your PC or Mac. This means that they can be accessed quickly.

Amazon S3 storage is implemented as an object that has to be read and written by the application that uses the object. Objects contain metadata—data about the object’s attributes that helps the system to catalog and identify the object. Examples of objects are pictures, videos, and music. Objects cannot be processed incrementally. They have to be read and written in their entirety. This can have performance and consistency implications.

There are several other differences between Amazon S3 and Amazon EBS storage, including differences in cost, throughput, and performance. These are discussed on the AWS website about storage objects at [https://aws.amazon.com/products/storage/](https://aws.amazon.com/products/storage/). It is up to the user or application designer to decide whether Amazon S3 or Amazon EBS storage is more appropriate for a given application.

There are two major types of EBS volumes, and each major type has two subtypes. Each type has benefits and drawbacks, so it is important to choose the type that fits best with the work you are using it for.

More information about EBS volume types can be found at the AWS webpage “Amazon EBS volume types” ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html)).

Here are several more key differences between Amazon S3 and Amazon EBS data storage:

-   Amazon EBS can only be used when attached to an EC2 instance. In contrast, Amazon S3 can be accessed on its own using Hypertext Transfer Protocol (HTTP) protocols.
-   Amazon EBS cannot hold as much data as Amazon S3.
-   Amazon EBS can only be attached to one EC2 instance, whereas data in an S3 bucket can be accessed by multiple EC2 instances.
-   Amazon S3 experiences more delays than Amazon EBS when writing data.
-   The _eventual consistency_ data model is implemented differently between Amazon EBS and Amazon S3. Eventual consistency means that if you do a write followed by a read, the data read will eventually be the same as the data written. With Amazon EBS, the delay in achieving this consistency is almost zero, whereas it might be considerably longer with Amazon S3.
-   EBS volumes are encrypted in their entirety, whereas Amazon S3 objects are encrypted individually by server-side encryption (SSE).
-   Amazon EBS includes three types of volumes, whereas Amazon S3 includes more types:
    -   **S3 Standard**
    -   **S3 Standard-Infrequent Access (S3 Standard-IA)**
    -   **S3 One Zone-Infrequent Access (S3 One Zone-IA)**
    -   **S3 Intelligent-Tiering**
    -   **S3 Glacier**
    -   **S3 Glacier Deep Archive**

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **In your opinion, how has cloud computing impacted the way that society interacts with technology? Is it a positive or negative impact overall? Why?**
2.  **In an election, it is important to make sure that the vote count is accurate. Do you know how votes are actually counted? In some areas, election officials manually read each ballot and add up the number of votes in each race. Other areas have computerized voting systems that transmit vote totals to the central counting facility. Computerized voting is faster than counting ballots by hand, but some people argue that it is risky because it opens elections up to the possibility of hacking. Do you think that it is a good idea to use cloud services to protect and count votes? Why or why not?**
3.  **As a student, how do you think cloud computing services could improve your school? Think about the ways that you turn in work, take exams, attend classes and events, or any other factor related to your school.**

## 

**Activity 1: All About Amazon EBS**

****Overview****  
In this activity, you will learn about Amazon EBS. You will learn about the different volume types and discuss the benefits that Amazon EBS provides.

**Objective**

-   Recognize the benefits, features, and use cases of the four types of EBS volumes.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

The following information about the benefits of Amazon EBS might help you with the activity:

**Data availability**

a) When you create an EBS volume in an Availability Zone, it is automatically replicated within that zone to prevent data loss due to failure of any single hardware component. After you create a volume, you can attach it to any EC2 instance in the same Availability Zone.

b) An EBS volume can be attached to only one instance at a time, but multiple volumes can be attached to a single instance.

c) An EBS volume and the instance to which it attaches must be in the same Availability Zone.

**Data persistence**

a) An EBS volume is off-instance storage that can persist independently from the life of an instance. You continue to pay for the volume usage as long as the data persists.

b) EBS volumes that are attached to a running instance can automatically detach from the instance with their data intact when the instance is terminated. The volume can then be reattached to a new instance, permitting quick recovery.

**Data encryption**

a) For simplified data encryption, you can create encrypted EBS volumes with the Amazon EBS encryption feature. All EBS volume types support encryption. You can use encrypted EBS volumes to meet a wide range of data-at-rest encryption requirements for regulated or audited data and applications.

**Snapshots**

a) Amazon EBS provides the ability to create snapshots (backups) of any EBS volume and write a copy of the data in the volume to Amazon S3, where it is stored redundantly in multiple Availability Zones. Snapshots of encrypted EBS volumes are automatically encrypted.

b) When you create a new volume from a snapshot, it's an exact copy of the original volume at the time the snapshot was taken. EBS volumes that are restored from encrypted snapshots are automatically encrypted.

You might also find the Amazon EBS volume types page at [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html.) useful for your research.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **Why do you think AWS provides four different options for EBS volumes?**
-   **Describe what makes each of the four benefits of Amazon EBS so important.**

## 

**Activity 2: Amazon EBS Use Cases**

**Overview**  
In this activity, you will read real-world use cases related to cloud computing and recommend the best type of EBS volume for each use case, citing details about the volume that make it the best option.

**Objective**

-   Analyze a use case and recommend the best type of virtual storage for the particular situation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **What factor most influenced your recommendation for the type of volume? Why?**

## 

**Lab: Attaching an EBS Volume**

**Overview**  
In this lab, you will create an EC2 instance if you do not already have one. Students will then attach an EBS volume to their EC2 instance.

**Objective**

-   Create an EBS volume and attach it to an EC2 instance.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **Was any step in the lab confusing? Where could you look for answers?**
-   **How do you think Amazon EBS could help a school run more effectively?**
-   **What is an issue that is important to you, and how could you use your knowledge of Amazon EBS and other cloud services to make an impact on that issue?** 

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Amazon EBS current events**

1.  Navigate to the Amazon Elastic Block Store page ([https://aws.amazon.com/ebs/](https://aws.amazon.com/ebs/)).
2.  Scroll down to the **What’s new with Amazon EBS** section.
    1.  Select and read about a recent Amazon EBS update.
    2.  Summarize the change or update.
3.  Give an example of how the update would improve EBS.

**Unplugged option: Your own Amazon EBS update**

1.  Brainstorm ideas for what you would add or change about EBS, if you could.
2.  Select one or two ideas and explain why the change would be beneficial and how it would be used.