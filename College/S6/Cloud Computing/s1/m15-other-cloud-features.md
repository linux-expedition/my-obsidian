# Module 15: Other Cloud Features

Lesson 1 of 1

## **Module purpose**

This module explores Amazon Web Services (AWS) services related to protecting data and managing networks. It also explores the integration of blockchain technologies within AWS services.

## **Module description**

You will use whitepapers to research and become a specialist in a service, later reporting back to your class. Next, an educator-led discussion about blockchain technologies will prepare you to debate blockchain benefits and scenarios.

## **Technology terminology**

Blockchain

Click to flip

A decentralized database technology that maintains a continually growing set of transactions and smart contracts hardened against tampering and revision using cryptography

Click to flip

Block

A growing list of records in a blockchain

Transaction

An exchange, usually of currency, in a blockchain

Ledger

A digital record of transactions

Immutable

The ability of a blockchain to remain unchanged

Trust

The dispersed confidence in accuracy among blockchain users

Transparency

Clear and open accounting of transactions

## **Background and misconceptions**

### ****Amazon Athena****

Athena is an interactive query service that makes it easy to analyze data in Amazon Simple Storage Service (Amazon S3) using standard structured query language (SQL).

Athena is serverless, so there is no infrastructure to manage, and users pay only for the queries that they run. Users can quickly query your data without having to set up and manage any servers or data warehouses. Point to the data in Amazon S3, define the schema, and start querying using the built-in query editor. Athena lets users tap into all of their data in S3 without the need to set up complex processes to extract, transform, and load (ETL) the data.

Athena is easy to use; there’s no need for complex ETL jobs to prepare data for analysis. This makes it easy for anyone with SQL skills to quickly analyze large-scale datasets.

Athena is out-of-the-box integrated with AWS Glue Data Catalog, letting users create a unified metadata repository across various services, crawl data sources to discover schemas, populate the Data Catalog with new and modified table and partition definitions, and maintain schema versioning. Users can also use AWS Glue fully managed ETL capabilities to transform data or convert it into columnar formats to optimize cost and improve performance.

With Athena, users pay only for the queries that they run. Users can save from 30 percent to 90 percent on per-query costs and get better performance by compressing, partitioning, and converting data into columnar formats. Athena queries data directly in Amazon S3. There are no additional storage charges beyond S3.

Athena uses Presto with ANSI SQL support and works with a variety of standard data formats, including CSV, JavaScript Object Notation (JSON), Apache ORC, Apache Avro, and Apache Parquet. Athena is ideal for quick, one-time querying, but it can also handle complex analysis, including large joins, window functions, and arrays. Athena is highly available and runs queries using compute resources across multiple facilities and multiple devices in each facility. Athena uses Amazon S3 as its underlying data store, making data highly available and durable.

With Athena, users don't have to worry about having enough compute resources to get fast, interactive query performance. Athena automatically runs queries in parallel, so most results come back within seconds.

### ****Amazon Macie****

Macie is a security service that uses machine learning to automatically discover, classify, and protect sensitive data in AWS. Macie recognizes sensitive data such as personally identifiable information (PII) or intellectual property and provides dashboards and alerts that give visibility into how this data is being accessed or moved. The fully managed service continuously monitors data access activity for anomalies and generates detailed alerts when it detects risk of unauthorized access or inadvertent data leaks. Macie is available to protect data stored in Amazon S3, with support for additional AWS data stores coming soon.

Macie makes it easy for security administrators to have management visibility into data storage environments, beginning with S3, with additional AWS data stores coming soon.

Macie uses machine learning to automate the process of discovering, classifying, and protecting data stored in AWS. This helps you better understand where sensitive information is stored and how it’s being accessed, including user authentication and access patterns.

Macie can send all findings to Amazon CloudWatch Events. This lets users build custom remediation and alert management for existing security ticketing systems.

### ****Blockchain and Amazon Managed Blockchain****

Blockchain is a way to manage an open distributed ledger of transactions. A ledger is a type of database in which transactions are only appended, never changed, making it immutable and trusted because there is no way to alter entries. A transaction is the record of some event taking place where ownership or possession is being transferred. An example of a transaction is the transfer of money from one account to another. The primary objective of a blockchain is to eliminate a central authority or middleman in order to speed things up.

Blockchain is a growing list of records, like a database—called _blocks_—that are linked using cryptography. Each block contains information about the block before it, a timestamp, and transaction data. Blockchain began with the development of cryptocurrency and has now branched out into new products and services.

Blockchain makes it possible to build applications where multiple parties can run transactions without the need for a trusted central authority. Today, building a scalable blockchain network with existing technologies is complex to set up and hard to manage. To create a blockchain network, each network member needs to manually provision hardware, install software, create and manage certificates for access control, and configure networking components. After the blockchain network is running, you need to continuously monitor the infrastructure and adapt to changes, such as an increase in transaction requests or new members joining or leaving the network.

Managed Blockchain is a fully managed service that lets users set up and manage a scalable blockchain network with a few clicks. Managed Blockchain eliminates the overhead required to create the network and automatically scales to meet the demands of thousands of applications running millions of transactions. After the network is up and running, Managed Blockchain makes it easy to manage and maintain your blockchain network. It manages certificates and lets users easily invite new members to join the network.

Managed Blockchain is a service that assists in choosing and provisioning other AWS services together with non-AWS services such as Ethereum in order to implement a blockchain. By asking a series of structured questions, Managed Blockchain guides the process of choosing the services needed to implement one of several types of blockchain.

With Managed Blockchain, users can quickly create blockchain networks that span multiple AWS accounts, letting a group of members run transactions and share data without a central authority. Unlike self-hosting a blockchain infrastructure, Managed Blockchain eliminates the need for manually provisioning hardware, configuring software, and setting up networking and security components. With the Managed Blockchain voting application programming interface (API), network participants can vote to add or remove members. After a new member is added, Managed Blockchain lets that member launch and configure multiple blockchain peer nodes to process transaction requests and store a copy of the ledger. Managed Blockchain also monitors the network and automatically replaces poorly performing nodes.

Managed Blockchain supports two popular blockchain frameworks: Hyperledger Fabric and Ethereum. Hyperledger Fabric is well suited for applications that require stringent privacy and permission controls with a known set of members; for example, a financial application where certain trade-related data is only shared with select banks. Ethereum is well suited for highly distributed blockchain networks where transparency of data for all members is important; for example, a customer loyalty blockchain network that lets any retailer in the network independently verify a user's activity across all members to redeem benefits. Alternatively, Ethereum can be used for joining a public Ethereum blockchain network. Ethereum and Hyperledger Fabric are products that are produced by companies outside AWS.

Managed Blockchain can easily scale your blockchain network as the usage of applications on the network grows over time. When a network member requires additional capacity for creating and validating transactions, the member can quickly add a new peer node using Managed Blockchain APIs. Managed Blockchain provides a selection of instance types that comprise varying combinations of central processing unit (CPU) and memory to give the flexibility to choose the appropriate mix of resources for the workload. Additionally, Managed Blockchain secures the network’s certificates with AWS Key Management Service (AWS KMS) technology, eliminating the need for users to set up their own secure key storage.

![Amazon Managed Blockchain diagram. Create a network: Choose an open source blockchain framework, set up a new blockchain network and your membership in your AWS account with just a few clicks. Invite members: Invite other AWS accounts to join the network. Add nodes: Create and configure blockchain peer nodes that store a copy of the distributed ledger. Deploy applications: Create and deploy decentralized applications to your network through your peer nodes. Transact with other members on the network.](https://awsacademy.contentcontroller.com/vault/1273a852-b5f9-47c2-b55b-6a034e3c8831/courses/5b9dfe7a-6f48-4eac-bb7f-abdb1a5a593d/0/assets/PfeZCu5IdYBgbvXz_MJwmlD7jJYW1H1Xv.jpg)

Image source: [https://aws.amazon.com/managed-blockchain/](https://aws.amazon.com/managed-blockchain/)

Blockchain technologies are often used to solve two types of customer needs. In the first case, multiple parties work with a centralized, trusted authority to maintain a complete and verifiable record of transactions. An example is a retail customer looking to connect their suppliers with a centralized ledger that maintains a transparent and verifiable history of information related to the movement of a product through its supply chain.

In the other case, multiple parties transact in a decentralized manner without the need for a centralized, trusted authority. An example is a consortium of banks and export houses looking to perform cross-boundary transfer of assets (for example, letters of credit) among each other, without a centralized authority acting as a liaison.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **A grocery store chain is attempting to set up a nationwide network for hosting their customer discount card. What types of hardware and software hurdles might they encounter?**
2.  **If an international banking conglomerate wanted to host transaction data, such as transferring money from one person to another, on a virtual computing environment, would it want a central person managing the network and data or for all users to have equally transparent access to data?**
3.  **Blockchain is a digital ledger of transactions in a virtual environment, where transactions are encrypted and secure, but also transparent and accessible by all. Digital currency relies on blockchain. Can you describe a time when you heard of blockchain being used by another company?**

## 

****Activity 1: AWS Services Experts****

****Overview****  
You will use whitepapers to research and answer questions related to AWS services.

**Objective**

-   Identify cloud services that can analyze and protect data, and manage networks.

### 

**Activity instructions**

You have learned about many AWS services that help individuals and businesses to effectively meet their technological needs. Today, you will look at several that protect data and help people manage networks.

1.  Your educator will divide the class into three equal groups.
2.  Each group will be assigned one of the following articles:
    1.  What Is Amazon Macie? [https://docs.aws.amazon.com/macie/latest/user/what-is-macie.html](https://docs.aws.amazon.com/macie/latest/user/what-is-macie.html)
    2.  What is Amazon Athena? [](https://docs.aws.amazon.com/macie/latest/userguide/what-is-macie.html)[](https://docs.aws.amazon.com/macie/latest/userguide/what-is-macie.html)[https://docs.aws.amazon.com/athena/latest/ug/what-is.html](https://docs.aws.amazon.com/athena/latest/ug/what-is.html)
    3.  Blockchain on AWS [https://aws.amazon.com/blockchain/](https://aws.amazon.com/blockchain/)
3.  Each group will gather the following information for their assigned AWS service:
    1.  Features
    2.  Function or purpose
    3.  Case study of use by a company
    4.  Pricing
    5.  Any other important information found
4.  Your groups will then present their findings to the remainder of the class.

### 

**Reflect**

Follow your educator’s instructions to answer and discuss the following questions.

-   **How do AWS services protect confidential data from security breaches?**

## 

**Activity 2: Blockchain Discussion**

**Overview**  
In this activity, the educator will lead a discussion about blockchain technologies. This discussion prepares you to summarize the benefits of using blockchain.

**Objective**

-   Explain benefits of blockchain technologies.

### 

**Activity instructions**

1. View the What Is Blockchain on AWS? video. ([https://youtu.be/9xbtq362Scs](https://youtu.be/9xbtq362Scs))

Consider the following:

a. What is an important benefit of blockchain?

b. How does using blockchain help address security issues?

2. Review the blockchain partner stories found at [https://aws.amazon.com/partners/success/](https://aws.amazon.com/partners/success/) (search for "blockchain").

a. Your educator might divide the class into groups for this task.

b. Either individually or as a group, you will be assigned one of the partner stories to focus on.

3. Read the assigned partner story and explain how the business uses blockchain.

a. Summarize the benefits of using blockchain.

b. Be prepared to share findings with the class.

4. A volunteer from each group will share their group’s findings.

### 

**Reflect**

Follow your educator’s instructions to answer and discuss the following question.

-   **When is it beneficial to use blockchain?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Convincing the bank**

1.  Using what you have learned in this module, write a letter to a fictional nationwide bank chain, explaining the benefits of using Athena and Macie in protecting customer data and analyzing company-wide data trends. Research and list specific benefits that the bank might expect from these two services.

## 

**Additional connections**

-   Blockchain on AWS  ([https://aws.amazon.com/blockchain/](https://aws.amazon.com/blockchain/))
-   Amazon Managed Blockchain ([https://aws.amazon.com/managed-blockchain/](https://aws.amazon.com/managed-blockchain/))
-   Amazon Managed Blockchain Resources ([https://aws.amazon.com/managed-blockchain/resources/](https://aws.amazon.com/managed-blockchain/resources/))
-   Amazon Managed Blockchain FAQs ([https://aws.amazon.com/managed-blockchain/faqs/](https://aws.amazon.com/managed-blockchain/faqs/))
-   AWS Blockchain Templates  ([https://aws.amazon.com/blockchain/templates/](https://aws.amazon.com/blockchain/templates/))