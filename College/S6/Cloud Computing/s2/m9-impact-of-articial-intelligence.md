# Module 9: Impact of Artificial Intelligence

Lesson 1 of 1

## 

**Module purpose**

This module addresses the ethical implementation of artificial intelligence (AI). You will use a debate to deepen learning and internalize greater ethical concepts.

## 

**Module description**

You will have a debate about the ethical implications of widespread AI in different areas of society. Next, you will research emerging technology and choose the one you feel is the most valuable, how it can be used to improve people's lives, and how you might personally use it or benefit from it. Finally, you will create guidelines for the ethical use of AI for a given scenario.

## 

**Technology terminology**

Artificial intelligence (AI)

Click to flip

A computer system able to perform tasks that normally require human intelligence, such as visual perception, speech recognition, decision-making, and translation between languages

Click to flip

## 

**Background and misconceptions**

As AI and machine learning (ML) become more prevalent in society, ethical questions have been posed based on actual and predicted events. In a broader sense, there are concerns about how humans treat robots, how robots interact with humans, how AI services make decisions ethically, and how individuals and groups use AI services.

Some examples of specific AI ethical dilemmas are:

-   The _human_ rights of AI robots
-   Ethical considerations caused by robotic replacement of care positions
    -   AI medical diagnoses and treatments
    -   AI police work
-   The transparency and accountability of AI makers
-   Biases placed into AI facial and voice recognition
    -   Racial bias
    -   Gender bias
-   Biases in algorithms, such as language learning
-   Liability of automated vehicles

You might be confused by examples of AI you have seen in movies and television, which often depict robots as highly intelligent and self-aware beings. Current AI products and services mimic human conversation, but are not at the level portrayed in movies. This is exemplified by the ELIZA effect, where humans tend to perceive computer systems as having greater human-like qualities than they actually possess. For example, a person using an automated chatbot to troubleshoot phone hardware might begin to treat the chatbot as a real person and ascribe emotional motivations to them.

## 

****Focus questions****

Follow your educator's instructions to answer and discuss the following questions.

### 

**Questions**

1.  **An ethical dilemma is a situation where a person has to make a choice between two courses of action, with the decision involving a moral principle. For example, a character in a story might have to decide between staying home to protect his family or fulfilling his national duty to fight in a war. Describe an ethical dilemma you have experienced. What happened? How did you handle the ethical dilemma?**
    1.  **Follow-up question: What might be some ethical dilemmas involving robots or AI?**
2.  **What would be some benefits of having an AI-enabled robot diagnose illnesses or perform surgeries? What might some of the problems be with having a robot replace a human doctor for certain tasks? How would you feel about an AI-enabled robot making a medical diagnosis for you or a loved one?**
3.  **Drones can be useful in many ways. The military can use drones to reach remote areas, gather information, and even launch attacks. Is it ethical for drones to use AI to decide which targets to attack?**

## 

**Activity 1: Debating AI in Society**

****Overview****  
In this activity, you will have a debate with your classmates about the ethical implications of widespread AI in different areas of society. You will brainstorm AI situations based on scenarios posed by your educator and prepare debate notes. Then you will engage in a debate based on your assigned role.

**Objective**

-   Analyze the ethical implications of AI.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **In your opinion, is there a scenario or issue that should never be decided by an AI being?**
-   **Isaac Asimov's famous book—which is now a movie starring Will Smith—_I, Robot_, has three laws of robotics. What are they?**
-   **Do you agree with these laws? Are there others that you would add?**

## 

**Activity 2: Using AI in Today’s World**

**Overview**  
You will research emerging technology and choose the one you feel is the most valuable, how it could be used to improve people's lives, and how you might personally use it or benefit from it.

**Objective**

-   Appraise the value of emerging AI technology.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **How can AI services impact global issues?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**AI guidelines at home**

1.  Brainstorm an AI solution that would benefit your home or community.
2.  Create a set of guidelines for implementing this new AI solution.
3.  Modify your previous guidelines or create entirely new ones, depending on the issue selected.

## 

**Additional connections**

-   [https://aws.amazon.com/machine-learning/customers/](https://aws.amazon.com/machine-learning/customers/)
-   [https://aws.amazon.com/machine-learning/ai-services/](https://aws.amazon.com/machine-learning/ai-services/)