# Module 6: Lambda

Lesson 1 of 1

## 

**Module purpose**

In this module, you will identify the benefits of Amazon Elastic Compute Cloud (Amazon EC2) and AWS Lambda. You will identify real companies that use these powerful solutions to increase performance and functionality and use this knowledge to contextualize your learning.

## 

**Module description**

You will label a blank diagram to show each instance state and match instance states to descriptions. You will then determine the most likely instance state in given situations. You will also choose the most cost-efficient instance type for various scenarios.

## 

**Technology terminology**

AWS Lambda

Click to flip

Lambda lets you run code without provisioning or managing servers. You pay only for the compute time you consume—there is no charge when your code is not running (and you start paying only after the first million requests per month on the [AWS Free Tier](https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all)). With Lambda, you can run code for virtually any type of application or backend service—all with zero administration. Upload your code and Lambda takes care of everything required to run and scale your code with high availability. You can set up your code to automatically be initiated from other AWS services or events, or it can be set up to respond directly to an HTTP or HTTPS request.

Click to flip

Amazon Elastic Compute Cloud (Amazon EC2)

A web service that provides secure, resizable compute capacity in the cloud. Think of it as renting a computer in the cloud.

## 

**Background and misconceptions**

Lambda lets you run code without provisioning or managing servers. You pay only for the compute time you consume—there is no charge when your code is not running.

With Lambda, you can run code for virtually any type of application or backend service—all with zero administration. Upload your code and Lambda takes care of everything required to run and scale your code with high availability. You can set up your code to be automatically initiated from other Amazon Web Services (AWS) services, or it can be called directly from any web or mobile app. For more information, see AWS Lambda ([https://aws.amazon.com/lambda/](https://aws.amazon.com/lambda/)).

Amazon EC2 is a web service that provides secure, resizable compute capacity in the cloud. It is designed to make web-scale cloud computing easier for developers. Amazon EC2 has a simple web service interface that lets you obtain and configure capacity with minimal friction. It provides you with complete control of your computing resources and lets you run on the proven computing environment of AWS. For more information, see Amazon EC2 ([https://aws.amazon.com/ec2/](https://aws.amazon.com/ec2/)).

One major difference between Lambda and Amazon EC2 is the cost of operation. The pricing models of the two services are quite different. Lambda cost is based on usage of the Lambda functions and the amount of storage used by the functions, whereas Amazon EC2 cost is based on the type of machine image used and the amount and type of storage used. For light and medium workloads, Lambda is a dramatically less expensive solution than Amazon EC2.

By working with Amazon EC2 to manage your instances from the moment you launch them through their termination, you ensure that your customers have the best possible performance and resulting experience with the applications or sites that you host on your instances.

The following illustration represents the transitions between instance states:

![Diagram that represents the transitions between instance states: An arrow leads from an Amazon Machine Image (AMI) to a box labeled pending. Another arrow leads from pending to a box labeled running. Three arrows lead in different directions away from the running box: One arrow, labeled Reboot, leads to a box labeled rebooting, and an arrow pointing in the opposite direction connects back to the running box. A second arrow, labeled Terminate, leads from the running box to a box labeled shutting-down. Another arrow leads from the shutting-down box to a box labeled terminated. Another arrow, labeled Stop and Stop-Hibernate, leads from the running box to a box labeled stopping. Another arrow leads from the stopping box to a box labeled stopped. Two arrows lead in different directions away from the stopped box: One arrow, labeled Start, leads back to the pending box. A second arrow, labeled Terminate, leads back to the terminated box. These arrows, plus the stopping and stopped boxes, are contained by a box labeled EBS-backed instances only.](https://awsacademy.contentcontroller.com/vault/3b4b44b2-fa38-41cd-88ae-64ad19f9f31b/courses/6417243b-0e06-47e8-887c-c57b11ae3c64/1/assets/dsa1UCX6I6GHpVSm_cU2EaD_SfwLfleBc.png)

Image source: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html)

Amazon EC2 provides the following purchasing options for you to make purchases based on your needs:

-   **On-Demand Instances:** Pay by the second for the instances that you launch.
-   **Reserved Instances:** Purchase, at a significant discount, instances that are always available, for a term of 1 to 3 years.
-   **Scheduled Instances:** Purchase instances that are always available on the specified recurring schedule, for a 1-year term.
-   **Spot Instances:** Request unused EC2 instances, which can lower your Amazon EC2 costs significantly.
-   **Dedicated Hosts:** Pay for a physical host that is fully dedicated to running your instances, and bring your existing per-socket, per-core, or per-virtual machine (VM) software licenses to reduce costs.
-   **Dedicated Instances:** Pay by the hour for instances that run on single-tenant hardware.
-   **Capacity Reservations:** Reserve capacity for your EC2 instances in a specific Availability Zone for any duration.

If you require a capacity reservation, purchase reserved instances or capacity reservations for a specific Availability Zone, or purchase scheduled instances. Spot instances are a cost-effective choice if you can be flexible about when your applications run and if they can be interrupted. Dedicated hosts or dedicated instances can help you address compliance requirements and reduce costs by using your existing server-bound software licenses. For more information, see “Instance purchasing options” ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html)).

Lambda is a serverless computing environment that makes it easy to run code in response to events such as changes to Amazon Simple Storage Service (Amazon S3) buckets, updates to an Amazon DynamoDB table, or custom events generated by your applications or devices. With Lambda, you do not have to provision your own instances. Lambda performs all the operational and administrative activities on your behalf, including capacity provisioning, monitoring fleet health, applying security patches to the underlying compute resources, deploying your code, running a web service front end, and monitoring and logging your code. Lambda provides scaling and high availability to your code without additional effort on your part.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Here at school, we have a physical server. This server helps process all the software and data that we use and create. When our server is very busy, the network slows down. If our server were to malfunction, we would be without internet connectivity, software updates, and other critical parts of our school infrastructure. Some businesses use Amazon EC2, a virtual computing environment that acts as a server in the cloud and is able to process more information than our physical server. How would our school be different if we used a virtual server?**
2.  **Lambda is a virtual computing environment that can run code without a server. This lets large companies run a lot of devices, programs, and other applications while simultaneously uploading and analyzing vast amounts of data. For example, a company might use Lambda to track internet-enabled smart trucks that are connected to the company network and are constantly transmitting data back to corporate headquarters. Can you think of a company you know that might use a solution like Lambda? Describe what components and processes might exist.**
3.  **Massively multiplayer online games (MMOs) run many servers that often need maintenance. These maintenance periods often shut down the game for brief periods of time. How might a service like Lambda improve this experience for the company and the customers?**

## 

Activities

### 

**Activity 1: Instance Lifecycles**

****Overview****  
You will read technical literature that teaches you about the instance lifecycle. You will then label a blank diagram showing each instance state and define what it means. Then you will fill in missing instance state descriptions and names to complete a table.

**Objective**

-   Recall the process for deploying a function using the Lambda console.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **In four stages, what is a complete EC2 instance lifecycle?**

### 

**Activity 2: Instance State Situations**

**Overview**  
You will read technical literature that teaches about situations describing different instance states. You will then determine the most likely instance state for several situations.

**Objective**

-   Recall the process for deploying a function using the Lambda console.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **If you were developing a function on the Lambda console to run every time a customer started an order but didn’t complete the transaction, would you want it to end in a _stopped_ or _terminated_ state?**

### 

**Activity 3: Instance Purchasing Scenarios**

**Overview**  
You will choose the most cost-efficient instance type for a given scenario and you will explain your reasoning.

**Objective**

-   Recall the process for deploying a function using the Lambda console.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **In general, when would on-demand EC2 instances be a logical choice? Would Lambda instances be an alternative solution to consider?**

## 

**Lab**

### 

**Lab: Creating a Lambda Function**

**Overview**  
You will use the Lambda console to create and implement a Lambda function. You will apply what you have learned about Lambda instances and their uses, and instance states to create a working digital artifact.

**Objective**

-   Create a Lambda function using the Lambda console.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **Were there any steps that needed further explanation?**
-   ****When would you use the Lambda console in your work if you were working for a small shipping company?****
-   ******When would you use this in your personal life? Can you use it to create a personal web program?******

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Create a new Lambda function**

1.  Using the directions for the lab, create a second Lambda function that is different from your first.
2.  Show your educator, your parent, or a friend this new function and explain how it works.

**Unplugged option**

1.  Using the printed lab directions for your class, write a brief paragraph explaining the steps to someone who is younger or less experienced than you.

## 

**Additional connections**

-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Instance lifecycle ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html))
-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Instance purchasing options ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html))
-   AWS Lambda FAQs ([https://aws.amazon.com/lambda/faqs/](https://aws.amazon.com/lambda/faqs/))
-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Amazon EC2 instances ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Instances.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Instances.html))
-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Instance types ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html))
-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Launch your instance ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/LaunchingAndUsingInstances.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/LaunchingAndUsingInstances.html))
-   Amazon Elastic Compute Cloud User Guide for Linux Instances: Stop and start your instance ([https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Stop_Start.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Stop_Start.html))