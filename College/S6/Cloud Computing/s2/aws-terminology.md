#### m1
##### Cloud services
A system of technologies that stores, processes, and allows access to the shared information in the cloud.

##### Shared responsibility model
A cloud service that provides tools and methods to secure the cloud, but the user shares in the security by implementing security based on the services provided.

##### Infrastructure as a service (IaaS)
A model in which virtual machines (VMs) and servers are used for customers to host a wide range of applications, and IT services are provided. 

##### Software as a service (SaaS)
Provides applications through the internet that a third party manages.

##### Platform as a service (PaaS)
Provides a virtual platform for customers to create custom software.

##### Identity and access management (IAM)

Involves the application of controls to users who need access to computing resources.

##### Principle of least privilege

This principle is centered on the concept that the least number of permissions is to be applied to a user to add, modify, or delete information.

Denial of service (DoS)

An attacker can issue a DoS attack against the cloud service to render it inaccessible, thereby disrupting the service. There are a number of ways an attacker can disrupt the service in a virtualized cloud environment: by using all its central processing unit (CPU), random access memory (RAM), disk space, or network bandwidth.

Watering hole attack

A security exploit in which the attacker seeks to compromise a specific group of end users by infecting websites that members of the group are known to visit. The goal is to infect a targeted user's computer and gain access to the network at the target's place of employment.

Multi-factor authentication (MFA)

A security system that requires more than one method of authentication from independent categories of credentials to verify the user's identity for a login or other transaction.