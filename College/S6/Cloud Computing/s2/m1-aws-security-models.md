# Module 1: AWS Security Models

Lesson 1 of 1

## **Module purpose**

The purpose of this module is to give you an introduction to the shared responsibility model for security of cloud services. You will review the responsibilities of the client and Amazon Web Services (AWS) in this model.

## **Module description**

In this module, you will differentiate between customer and AWS responsibilities under the shared responsibility model.

## **Technology terminology**
[[aws-terminology]]

## **Background and misconceptions**

The security objectives of confidentiality, integrity, availability, authenticity, accountability, liability, and privacy form the basis for IT security in general. These objectives also apply to cloud systems. However, they cannot be applied to cloud systems on a one-to-one basis because various services and application architectures have different requirements.

In this module, we will cover the shared responsibility model for securing resources and discuss the different responsibilities for the customer and AWS.

AWS security best practices begin with the AWS shared responsibility model that dictates which security controls are AWS's responsibility and which are the customer’s. There are common misunderstandings, such as the cloud provider should be fully responsible or the customer should be fully responsible, when in fact, a shared responsibility model is best.

By the very nature of the name AWS shared responsibility model, it should be clear that security implementation on the AWS Cloud is not the sole responsibility of any one player, but is shared between AWS and the customer. Applying security best practices should be seen not as a burden, but as a way for all parties to be most effective when using cloud services to advance their goals in sustainable and beneficial ways.

Customers should carefully consider which services they choose because their responsibilities vary depending on the services used, the integration of those services into their IT environment, and applicable laws and regulations.

The cloud is growing rapidly, and new services are emerging daily. This is great news for customers who want to achieve goals more quickly and easily than they have been able to in the past, but this also makes security something of a moving target.

## ****Focus questions****

Follow your educator's instructions to answer and discuss the following questions.

### **Questions**

1.  **You own a small standalone coffee shop.**
    1.  **You have identified several resources that you would need to protect for your business.  
          
        Next, you need to prioritize your security needs. Evaluate the risk associated with each resource. Describe the impact a security breach would have on your business. Rate the likelihood of the resource being compromised, and then explain your rationale for your ratings.**
    2.  **Use the following table as a guide:**

**Resource**  

**If it were breached, what would be the impact on your business?**  

**Likelihood the resource will be compromised (high, medium, low)**  

**Rationale**  

Website

  

  

  

Customer credit card data

  

  

  

Employee information

  

  

  

2. **The PoLP is centered on the concept that the least number of permissions is to be applied to a user to search (READ), add (WRITE), delete (DELETE), modify (WRITE and/or DELETE), or edit permissions for all other users and take ownership for any and all data (FULL). The PoLP can be applied to every level of a system. It applies to end users, systems, processes, networks, databases, applications, and every other facet of an IT environment.** 

**For example, with the PoLP, an employee whose job is to enter data into a database only needs the ability to add (WRITE) records to that specific database. If malware infects that employee’s computer or if the employee clicks a link in a phishing email, the malicious attack is limited to making database entries. If that employee has root (FULL) access privileges, however, the infection can spread system-wide.**

**In your coffee shop, you have hired a new barista to help with the morning shift. They will be accepting customers’ orders, filling the orders, and taking payment for the orders.** 

a. **Use the PoLP to determine what systems this new employee would need access to. What privileges would they need for each system? Would you give them open access to your systems? Why or why not?**

b. **Your coffee shop has added a new frequent buyer program. What additional systems (if any) would this new employee need access to? What privileges would they need for each system? Is it sensible from an information security standpoint to provide them with each of these privileges?**

3. **A DoS attack is a deliberate attempt to make your website or application unavailable to users, such as by flooding it with network traffic. To achieve this, attackers use a variety of techniques that consume large amounts of network bandwidth or tie up other system resources, disrupting access for legitimate users. In its simplest form, a lone attacker uses a single source to run a DoS attack against a target.**

**But in a distributed denial of service (DDoS) attack, an attacker uses multiple sources—which might be distributed groups of malware-infected computers, routers, Internet of Things (IoT) devices, and other endpoints—to orchestrate an attack against a target. A network of compromised hosts participates in the attack, generating a flood of packets or requests to overwhelm the target.** 

**The most common DDoS attacks are infrastructure layer attacks. An attacker generates large volumes of traffic that can inundate the capacity of a network or tie up resources on systems like a server, firewall, intrusion protection system (IPS), or load balancer. Although these attacks can be easy to identify, to effectively mitigate them, you must have a network or systems that scale up capacity more rapidly than the inbound traffic flood. This extra capacity is to filter out or absorb the attack traffic, enabling your system and application to respond to your legitimate customer traffic.**

a. **How would the owner of a coffee shop best protect their assets from a DDoS?**

## **Activity: Whose Responsibility Is It?**

**Overview**  
You will examine whether AWS or the customer is responsible for securing each component of a cloud service, based on the type of service.

**Objectives**

-   Describe the shared responsibility model.
-   Determine security responsibility for cloud resources.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Why share the responsibility?**

## 

**Optional connection**

Follow your educator’s instructions to complete the activity.

**Security at home**

1.  What personal information do you need to protect?
2.  Identify security risks at home.
3.  List possible security steps to be taken.

  

## 

**Additional connections**

-   Best Practices for Security, Identity, & Compliance: [https://aws.amazon.com/architecture/security-identity-compliance/](https://aws.amazon.com/architecture/security-identity-compliance/)