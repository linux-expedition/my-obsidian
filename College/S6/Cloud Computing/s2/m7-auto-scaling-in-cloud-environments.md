# Module 7: Auto Scaling in Cloud Environments

Lesson 1 of 1

## 

**Module purpose**

In this module, you will explore the functions of automatic scaling and the use of launch templates, and will learn how to monitor Auto Scaling groups.

## 

**Module description**

In this module, you will explore the main functions of automatic scaling, create a launch template, create a functional Auto Scaling group, and then develop a plan for monitoring an Auto Scaling group.

## 

**Technology terminology**

Automatic scaling

Click to flip

Automatic scaling services monitor your applications and automatically adjust capacity to maintain steady, predictable performance at the lowest possible cost. The term _scaling_ means that new instances can be created as needed, depending on network traffic, or existing instances can be augmented with additional storage or compute power to handle increased network traffic. The latter type of scaling is called vertical scaling.

Click to flip

Fleet

A single instance or a group of instances.

Launch template

Specifies instance configuration information.

Scale-out

Add Amazon Elastic Compute Cloud (Amazon EC2) instances as needed to respond to demand.

Scale-in

Remove EC2 instances when there is a reduction in demand.

## 

**Background and misconceptions**

Amazon EC2 Auto Scaling helps maintain application availability and permits users to automatically add or remove EC2 instances according to conditions they define. Users can use the fleet management features of Amazon EC2 Auto Scaling to maintain the health and availability of a fleet. They can also use the dynamic and predictive scaling features of Amazon EC2 Auto Scaling to add or remove EC2 instances. Dynamic scaling responds to changing demand, and predictive scaling automatically schedules the right number of EC2 instances based on predicted demand. Dynamic scaling and predictive scaling can be used together to scale faster.

-   Amazon EC2 Auto Scaling can detect when an instance is unhealthy, terminate it, and replace it with a new one.
-   Amazon EC2 Auto Scaling helps to ensure that your application always has the right amount of compute power and proactively provisions capacity with predictive scaling.
-   Amazon EC2 Auto Scaling adds instances only when needed and can scale across purchase options to optimize performance and cost.

Whether they are running one EC2 instance or thousands, users can use Amazon EC2 Auto Scaling to detect impaired EC2 instances and unhealthy applications, and replace the instances without direct intervention. This helps to ensure that their application is getting the compute capacity that they expect. Amazon EC2 Auto Scaling will perform three main functions to automate fleet management for EC2 instances:

-   **Monitors the health of running instances**  
    Amazon EC2 Auto Scaling helps to ensure that your application is able to receive traffic and that EC2 instances are working properly. Amazon EC2 Auto Scaling periodically performs health checks to identify any instances that are unhealthy.
-   **Replaces impaired instances automatically**  
    When an impaired instance fails a health check, Amazon EC2 Auto Scaling automatically terminates it and replaces it with a new one. That means that you don’t need to respond manually when an instance needs replacing.
-   **Balances capacity across Availability Zones**  
    Amazon EC2 Auto Scaling can automatically balance instances across zones, and it always launches new instances so that they are balanced between zones as evenly as possible across your entire fleet. Load balancing distributes load across existing instances, whereas Auto Scaling creates or removes instances based on demand.

**Scheduled scaling**

When scaling based on a schedule, users can scale their application ahead of known load changes. For example, every week, the traffic to the web application starts to increase on Wednesday, remains high on Thursday, and starts to decrease on Friday. Users can plan their scaling activities based on the known traffic patterns of the web application.

**Dynamic scaling**

With Amazon EC2 Auto Scaling, users can follow the demand curve for their applications closely, reducing the need to manually provision Amazon EC2 capacity in advance. For example, users can use target-tracking scaling policies to select a load metric for their application, such as CPU usage. Or they can set a target value using the new Request Count Per Target metric from Application Load Balancer, a load-balancing option for the Elastic Load Balancing service. Amazon EC2 Auto Scaling will then automatically adjust the number of EC2 instances as needed to maintain your target. An example of when dynamic scaling would be useful is when music or video goes viral. Dynamic scaling would sense the increased traffic and spin up new instances to keep pace with the increased demand.

**Predictive scaling**

Predictive scaling, a feature of Amazon EC2 Auto Scaling, uses machine learning (ML) to schedule the right number of EC2 instances in anticipation of approaching traffic changes. Predictive scaling predicts future traffic, including regularly occurring spikes, and provisions the right number of EC2 instances in advance. Predictive scaling’s ML algorithms detect changes in daily and weekly patterns, automatically adjusting their forecasts. This removes the need for manual adjustment of Auto Scaling parameters as cyclicality changes over time, making Auto Scaling simpler to configure. Auto Scaling enhanced with Predictive scaling delivers faster, simpler, and more accurate capacity provisioning, resulting in lower cost and more responsive applications.

**AWS Auto Scaling features**

**Automatic resource discovery**

AWS Auto Scaling scans and automatically discovers the scalable cloud resources underlying an application, so there’s no need to manually identify these resources one by one through individual service interfaces.

**Built-in scaling strategies**

Using AWS Auto Scaling, users can select one of three predefined optimization strategies designed to optimize performance and costs, or balance the two. If preferred, users can set their own target resource usage. Using their selected scaling strategy, AWS Auto Scaling will create the scaling policies for each resource.

**Smart scaling policies**

AWS Auto Scaling continually calculates the appropriate scaling adjustments and immediately adds and removes capacity as needed to keep metrics on target. Amazon Web Services (AWS) target tracking scaling policies are self-optimizing and learn actual load patterns to minimize fluctuations in resource capacity. This results in smoother, smarter scaling, and the user pays only for the resources they need.

**Launch templates**

A launch template is similar to a launch configuration in that it specifies instance configuration information. Included are the ID of the Amazon Machine Image (AMI), the instance type, a key pair, security groups, and the other parameters that are used to launch EC2 instances. However, defining a launch template instead of a launch configuration facilitates users having multiple versions of a template. With versioning, users can create a subset of the full set of parameters and then reuse it to create other templates or template versions. For example, a user can create a default template that defines common configuration parameters such as tags or network configurations, and permit the other parameters to be specified as part of another version of the same template.

## 

****Focus questions****

Follow your educator's instructions to answer and discuss the following questions.

### 

**Questions**

1.  **AWS Auto Scaling monitors the health of running instances, replaces impaired instances automatically, and balances capacity across Availability Zones. Why would a business need these features?**
2.  **A customer wants to use AWS Auto Scaling for a shipping website. He has limited historical data on web traffic for his site. He feels that he can somewhat accurately predict future traffic, but isn’t entirely sure. Dynamic scaling reacts to instance use as it happens, whereas predictive scaling uses predictions based on historical data to plan ahead for computing power. Would you recommend dynamic scaling or predictive scaling to the customer? Explain why your recommendation is the best one.**
3.  **If you were assigned to manage instances for a rapidly growing international company, how would you explain the need to invest in automatic scaling to your supervisor?**

## 

**Activity: Developing a Plan to Monitor Auto Scaling Groups**

****Overview****  
In this activity, you will explore the use of Auto Scaling groups and how they are monitored. Then the class will break into small groups to discuss and form a plan for monitoring an Auto Scaling group.

**Objectives**

-   Recognize the three main functions of AWS Auto Scaling.
-   Develop a plan for monitoring an Auto Scaling instance or group.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **What factors does Amazon EC2 monitor with Auto Scaling groups to measure performance?**

## 

**Lab: Creating Launch Templates and Auto Scaling Groups**

**Overview**  
In this lab, you will create a launch template and an Auto Scaling group.

**Objective**

-   Create a launch template and an Auto Scaling group.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

Reflect

After completing the lab, be prepared to answer and discuss these questions.

-   **What did you find most challenging about this lab? What was the simplest?**
-   ****When would a company such as Prime Video use Auto Scaling groups?****
-   ******How could you use what you learned in this lab to monitor an application that you created and distributed on the cloud?******

## 

**Optional connection**

Follow your educator's instructions for completing this activity.

**Terminating an instance in an Auto Scaling group and deleting scaling infrastructure**

1.  Complete the optional steps outlined toward the end of Monitoring CloudWatch Metrics for Your Auto Scaling Groups and Instances ([https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-monitoring.html](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-monitoring.html)).

**Unplugged option**

1.  Your educator will give you printed directions and you will summarize what you learned in a brief, written response.

## 

**Additional connections**

-   [https://aws.amazon.com/ec2/autoscaling/](https://aws.amazon.com/ec2/autoscaling/)[  
    ](http://https://aws.amazon.com/ec2/autoscaling/)
-   [https://aws.amazon.com/autoscaling/features/](https://aws.amazon.com/autoscaling/features/)
-   [https://docs.aws.amazon.com/autoscaling/ec2/userguide/LaunchTemplates.html](https://docs.aws.amazon.com/autoscaling/ec2/userguide/LaunchTemplates.html)
-   [https://docs.aws.amazon.com/autoscaling/ec2/userguide/GettingStartedTutorial.html](https://docs.aws.amazon.com/autoscaling/ec2/userguide/GettingStartedTutorial.html)
-   [https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-monitoring.html](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-monitoring.html)
-   [https://youtu.be/bSRTAMPqS3E](https://youtu.be/bSRTAMPqS3E)
-   [https://www.youtube.com/watch?v=PideBMIcwBQ](https://www.youtube.com/watch?v=PideBMIcwBQ)