# Module 3: Cloud Services and Instance States

Lesson 1 of 1

## 

**Module purpose**

The purpose of this module is to introduce you to the instance lifecycle and the transition between states as well as Amazon Web Services (AWS) billing. You will learn how to manage instances from launch through termination.

## 

**Module description**

In this module, you will diagram instance states and transitions, determine the most likely instance state, indicate usage billing, and learn how to choose the most cost-efficient instance type.

## 

**Technology terminology**

Amazon Elastic Compute Cloud (Amazon EC2)

Click to flip

A web service that provides secure, resizable compute capacity in the cloud. Think of it as renting a computer in the cloud.

Click to flip

EC2 instance

A virtual server in Amazon Elastic Compute Cloud (Amazon EC2) for running applications on the Amazon Web Services (AWS) infrastructure.

Amazon Elastic Block Store (Amazon EBS)

Storage for specific Amazon Elastic Compute Cloud (Amazon EC2) instances. Think of it as the storage drive for your EC2 instance.

Instance store volumes

Temporary storage that is not persistent through instance stops, terminations, or hardware failures.

Amazon Machine Image (AMI)

Special type of virtual appliance that is used to create a virtual machine (VM) within Amazon Elastic Compute Cloud (Amazon EC2). It serves as the basic unit of deployment for services delivered using Amazon EC2.

IPv4 address

A 32-bit number that uniquely identifies a network interface on a machine. An IPv4 address is typically written in decimal digits and formatted as four 8-bit fields that are separated by periods.

IPv6 address

A 128-bit alphanumeric string that identifies a device in the addressing scheme for IPv6. IPv6 addresses are preferred by professional users such as network engineers, tech companies, data centers, and mobile carriers.

Elastic IP address

A static IPv4 address designed for dynamic cloud computing that is associated with your Amazon Web Services (AWS) account. The Elastic IP address remains in place through events that normally cause the address to change, such as stopping or restarting the instance.

## 

**Background and misconceptions**

One of the main benefits of cloud technology is the ability to pay for just what you need and to pay for things as you use them. To make this possible, the cost for VMs in AWS is broken down into the states that instances go through during their lifetime. When you launch an instance in the cloud, a server will progress through various states.

When you initially launch an instance, it enters the pending state. This means that AWS is getting your instance ready, and you cannot access it yet. After the instance is ready for you, it enters the running state. You can connect to your running instance and use it the way that you would use a computer in front of you.

As soon as your instance transitions to the running state, you are billed for each second, with a 1-minute minimum, that you keep the instance running, even if the instance remains idle and you do not connect to it.

When you no longer need an instance, you can stop or terminate it. As soon as the status of an instance changes to shutting down or terminated, you stop incurring charges for that instance. You can stop or hibernate an instance and restart it later. However, some temporary data might be lost when you do so. Following is a table showing the effects of stopping and restarting an instance.

The following table summarizes the key differences between rebooting, stopping, hibernating, and terminating your instance.

**Characteristic**  

**Reboot**  

**Stop/Start (Amazon EBS Backed Instances Only)**  

**Hibernate (Amazon EBS Backed Instances Only)**  

**Terminate**  

Host computer

The instance stays on the same host computer.  

In most cases, we move the instance to a new host computer. Your instance can stay on the same host computer if there are no problems with the host computer.

In most cases, we move the instance to a new host computer. Your instance can stay on the same host computer if there are no problems with the host computer.  

None  

Private and public IPv4 addresses  

These addresses stay the same.  

The instance keeps its private IPv4 address. The instance gets a new public IPv4 address, unless it has an Elastic IP address, which doesn't change during a stop or start.

The instance keeps its private IPv4 address. The instance gets a new public IPv4 address, unless it has an Elastic IP address, which doesn't change during a stop or start.  

None  

Elastic IP address (IPv4)  

The Elastic IP address remains associated with the instance.  

The Elastic IP address remains associated with the instance.  

The Elastic IP address remains associated with the instance.  

The Elastic IP address is disassociated from the instance.  

IPv6 address  

The address stays the same.  

The instance keeps its IPv6 address.  

The instance keeps its IPv6 address.  

None  

Instance store volumes  

The data is preserved.  

The data is erased.  

The data is erased.  

The data is erased.  

Root device volume  

The volume is preserved.  

The volume is preserved.  

The volume is preserved.  

The volume is deleted by default.  

Random access memory (RAM) (contents of memory)  

The RAM is erased.  

The RAM is erased.  

The RAM is saved to a file on the root volume.  

The RAM is erased.  

Billing  

The instance billing hour doesn't change.  

You stop incurring charges for an instance as soon as its state changes to stopping. Each time an instance transitions from stopped to running, we start a new instance billing period, billing a minimum of 1 minute every time you restart your instance.  

You incur charges while the instance is in the stopping state but stop incurring charges when the instance is in the stopped state. Each time an instance transitions from stopped to running, we start a new instance billing period, billing a minimum of 1 minute every time you restart your instance.  

You stop incurring charges for an instance as soon as its state changes to shutting down.  

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **When it comes to cell phone plans, understanding your data usage can help you save money. Understanding usage and state of your EC2 instances can help you better provision your resources to control costs.**
    1.  **What is considered data usage on your cell phone?**
    2.  **What are some common ways data is used on smartphones? How is this similar to using EC2 instances for computing power?**
2.  **Why might you use an On-Demand Instance when you can predict your instance needs far in advance or for ongoing, fluctuating computing power?**
    1.  **What attributes are used to determine the price of family cell phone plans? When would it be desirable to pay a set amount for instances rather than a variable amount based on usage?**

## 

**Activity 1: Instance and Animal Lifecycles**

****Overview****  
In this lesson, you will review the instance lifecycle and compare it to analogous animal and/or machine lifecycles.

**Objectives**

-   Describe the six instance states.
-   Diagram the transitions between instance states from launch to termination.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **How could we compare the lifecycle of a living thing to the rebooting state?**

## 

**Activity 2: Most Likely Instance State**

**Overview**  
Given a list of situations, you will determine the most likely instance state.

**Objectives**

-   Describe the six instance states.
-   Determine the most likely instance state for a given situation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **When an instance reaches its scheduled retirement date, it is stopped or terminated by AWS. What happens if your instance root device is an EBS volume? What if your instance root device is an instance store volume?**

## 

**Activity 3: Which Instance Billing Option Is the Most Cost-Efficient?**

****Overview****  
You will be given scenarios and you must choose the most cost-efficient instance billing option and explain why.

**Objective**

-   Indicate instance usage billing for each instance state.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **How do Savings Plans and Reserved Instances differ?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Unplugged option**

There are different billing options for instances: On-Demand Instances, Reserved Instances, and Spot Instances. What is the typical discount percentage? Give a brief description of the billing option.

**Option**  

**Discount**  

**Description**  

On-Demand  

  

  

Reserved  

  

  

Spot  

  

  

## 

**Additional connections**

-   Understand the Instance Store and EBS: [https://aws.amazon.com/premiumsupport/knowledge-center/instance-store-vs-ebs/](https://aws.amazon.com/premiumsupport/knowledge-center/instance-store-vs-ebs/)
-   Learn About Elastic IP Addresses:  
    [https://aws.amazon.com/premiumsupport/knowledge-center/intro-elastic-ip-addresses/](https://aws.amazon.com/premiumsupport/knowledge-center/intro-elastic-ip-addresses/)
-   Troubleshoot instance launch Issues: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/troubleshooting-launch.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/troubleshooting-launch.html)
-   Instance purchasing options:  
    [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-purchasing-options.html)
-   Spot Instance Best Practices:  
    [https://docs.aws.amazon.com/whitepapers/latest/cost-optimization-leveraging-ec2-spot-instances/spot-best-practices.html](https://docs.aws.amazon.com/whitepapers/latest/cost-optimization-leveraging-ec2-spot-instances/spot-best-practices.html)
-   AWS Cost and Usage Reports:  
    [https://console.aws.amazon.com/billing/home?#/reports](https://console.aws.amazon.com/billing/home?#/reports)
-   Cloud Economics Center:  
    [https://aws.amazon.com/economics/](https://aws.amazon.com/economics/)
-   Instance state usage billing:  
    [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html)
-   AWS Savings Plans:  
    [https://aws.amazon.com/savingsplans/](https://aws.amazon.com/savingsplans/)