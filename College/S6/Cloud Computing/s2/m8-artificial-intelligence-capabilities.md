# Module 8: Artificial Intelligence Capabilities

Lesson 1 of 1

## 

**Module purpose**

This module teaches you about the backend of artificial intelligence (AI) and machine learning (ML). You will learn about AI products and their applications, and apply this learning to demonstrate understanding.

## 

**Module description**

You will explore AI services provided by Amazon Web Services (AWS) and determine which type of AI product would best address a particular issue. You will then write a recommendation for how you would approach an issue using AI. Finally, you will apply your knowledge of AI services to run AI software that analyzes text in the form of book reviews.

## 

**Technology terminology**

Artificial Intelligence (AI)

Click to flip

A computer system able to perform tasks that normally require human intelligence, such as visual perception, speech recognition, decision making, and translation between languages

Click to flip

Machine learning (ML)

A subset of AI in which a computer algorithm can modify its own behavior

AWS DeepLens

A fully programmable video camera, with tutorials, code, and pretrained models designed to expand ML skills

## 

**Background and misconceptions**

AI can enhance the customer experience in a contact center, automate content moderation in media, improve health care analytics, forecast demand more accurately, and much more. With AI services from AWS, you can add capabilities such as image and video analysis, natural language, personalized recommendations, virtual assistants, and forecasting to your applications without deep expertise in ML. Each of the services can be used on their own or you can use them in concert to create sophisticated human-like functionality. Either way, you get instant access to fast, high-quality AI tools based on the same technology used to power Amazon’s own businesses.

AI services from AWS include the following:

-   Amazon Comprehend
    -   Discover insights and relationships in text
-   Amazon Forecast
    -   Increase forecast accuracy using ML
-   Amazon Lex
    -   Build voice and text chatbots
-   Amazon Personalize
    -   Build real-time recommendations into your applications
-   Amazon Polly
    -   Turn text into lifelike speech
-   Amazon Rekognition
    -   Analyze image and video
-   Amazon Textract
    -   Extract text and data from documents
-   Amazon Translate
    -   Translate texts with higher accuracy
-   Amazon Transcribe
    -   Translate audio files to text

AWS DeepLens helps put ML in the hands of developers—literally—with a fully programmable video camera, plus tutorials, code, and pretrained models designed to expand deep-learning skills.

AWS DeepLens lets developers of all skill levels get started with deep learning in less than 10 minutes by providing sample projects with practical, hands-on examples that can start running with a single click.

AWS developers can run any deep-learning framework, including TensorFlow and Caffe. AWS DeepLens comes preinstalled with a high-performance, efficient, optimized inference engine for deep learning using Apache MXNet.

AWS DeepLens integrates with Amazon Rekognition for advanced image analysis, Amazon SageMaker for training models, and Amazon Polly to create speech-enabled projects. The device also connects securely to Amazon Simple Queue Service (Amazon SQS), Amazon Simple Notification Service (Amazon SNS), Amazon Simple Storage Service (Amazon S3), Amazon DynamoDB, and more.

AWS DeepLens is easy to customize and is fully programmable using AWS Lambda. The deep-learning models in AWS DeepLens even run as part of a Lambda function, providing a familiar programming environment to experiment with.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Can you name any products or services that you have used that use AI? Explain how the product uses AI and why you think that AI is or isn’t beneficial.**
2.  **How might a grocery store use AI to improve its customer service or sales?**
3.  **If you were designing an AI-enhanced product or program, what would it do?**

## 

**Activity 1: Selecting the Best AI Product**

****Overview****  
You will be given a series of issues and will determine what type of AI product would best address that issue.

**Objectives**

-   Recognize capabilities of AI.
-   Determine an AI product that would help address a need or problem in a given situation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **When there are competing AI solutions possible for a situation or problem, how can a team decide which solution is best?**

## 

**Activity 2: Using AI to Solve Issues**

**Overview**  
You will write a recommendation for how you would approach an issue using AI.

**Objectives**

-   Recognize capabilities of AI.
-   Determine an AI product that would help address a need or problem in a given situation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

If you are having trouble linking your chosen idea to an AI solution, refer back to “AI Services” ([https://aws.amazon.com/machine-learning/ai-services/](https://aws.amazon.com/machine-learning/ai-services/)) to review the AI services available from AWS.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Which AI solution seems the most likely to succeed in our current age?**[](https://awseducate-content.s3.amazonaws.com/K12/Advanced+Cloud+Computing+I/Worksheets+and+Labs/S1M1_Student+activity+2+worksheet.docx.pdf)

## 

**Lab: Using AI for Text Analysis**

**Overview**  
In this lab, you will use the AWS Management Console to implement an AI solution for a specific analysis task.

**Objectives**

-   Recognize capabilities of AI.
-   Determine an AI product that would help address a need or problem in a given situation.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **Were there any steps that needed further explanation?**
-   ****When would you use AI services in a career situation, such as an online gift shop?****
-   ********When would you use this service in your personal life?********

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Learn about AWS DeepLens**

1.  Visit [https://aws.amazon.com/deeplens/](https://aws.amazon.com/deeplens/) to learn more about AWS DeepLens, an ML camera. Read the literature and watch the demo video.
2.  Show your educator, parent, or friend this new product and explain to them how it works.

**Unplugged option**

1.  Research a client case study about a business that uses AWS for ML services. Write about what you learned.

Your educator might print out case studies from these links:

-   [https://aws.amazon.com/machine-learning/customers/](https://aws.amazon.com/machine-learning/customers/)
-   [https://aws.amazon.com/partners/success/lyft-anodot/](https://aws.amazon.com/partners/success/lyft-anodot/)
-   [https://aws.amazon.com/statcastai/](https://aws.amazon.com/statcastai/)
-   [https://aws.amazon.com/blogs/machine-learning/assisting-people-at-haptik-using-amazon-polly/](https://aws.amazon.com/blogs/machine-learning/assisting-people-at-haptik-using-amazon-polly/)
-   [https://aws.amazon.com/blogs/machine-learning/zocdoc-builds-patient-confidence-using-tensorflow-on-aws/](https://aws.amazon.com/blogs/machine-learning/zocdoc-builds-patient-confidence-using-tensorflow-on-aws/)

## 

**Additional connections**

-   Explore AWS AI services ([https://aws.amazon.com/machine-learning/ai-services/](https://aws.amazon.com/machine-learning/ai-services/))
-   AWS DeepLens Overview ([https://aws.amazon.com/deeplens/](https://aws.amazon.com/deeplens/))
-   Machine Learning on AWS ([https://aws.amazon.com/machine-learning/](https://aws.amazon.com/machine-learning/))
-   AI with AWS Machine Learning ([https://aws.amazon.com/ai/](https://aws.amazon.com/ai/))