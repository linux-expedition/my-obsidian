# Module 10: Machine Learning

Lesson 1 of 1

## 

**Module purpose**

The purpose of this module is to teach the use cases and functions of machine learning (ML) technology. You will learn how ML can be applied to solve problems. You will also apply reinforcement learning by creating and evaluating an AWS DeepRacer model.

## 

**Module description**

The module will consist of two activities. You will learn about the use cases of ML and apply the ideas to scenarios. Then you will complete a lab experience in which you create and evaluate an AWS DeepRacer model.

## 

**Technology terminology**

Algorithm

Click to flip

A set of rules for a calculation to follow

Click to flip

Deep learning

An artificial intelligence (AI) learning process; the process of the AI scanning the artificial neural network

Forecasting

Using an algorithm to analyze data patterns to make predictions

Neural network

A model or algorithm that is designed to make decisions in a way similar to a human brain

Reinforcement learning

A type of ML that relies on the computer system improving its decision making as it learns from rewards it receives

Supervised learning

A type of ML in which the computer system learns from human-labeled or human-tagged data sources

Training

The process of providing more example data points to a computer system so it can learn

Machine learning (ML)

A subset of AI in which a computer algorithm can modify its own behavior

Unsupervised learning

A type of ML in which the computer system learns to analyze patterns in unlabeled or unstructured data points

## 

**Background and misconceptions**

ML is a specialized form of AI. ML refers to the ability of computers or networks to create and update algorithms to make predictions or perform tasks with increasingly better ability. By applying more data to models, the computer system increases ability and seemingly learns on its own. ML can be contrasted to computer systems where the programmer enters more information and directions for the computer system to follow. A different category of ML is called _reinforcement learning_. In this type of ML, the algorithm is initially coded to achieve a goal. Then the computer system experiences rewards or positive reinforcement as it learns to become more efficient.

The applications of ML cut across many sectors of the economy. Some current areas of use include:

-   Recommendations: This entails using user data patterns to personalize recommendations. These are familiar as advertising, news articles, or any type of suggestion to a user.
-   Forecasting: Current conditions are compared to data trends to make predictions.
-   Recognition: Computer systems are used to recognize patterns in images, video, text, or sound.
-   Fraud detection: Banks use ML algorithms to detect credit card fraud by analyzing purchasing patterns.
-   Transportation: ML is used in many ways in transportation. Mapping apps analyze data from traffic to suggest best routes. Self-driving cars adapt to recognize information from their sensors and react.
-   Health care: ML systems can be trained to diagnose illness.

Procedures in ML vary based on the task. However, some general principles are followed in most programming.

-   Data: First, acquire quality data from a data lake or other sources.
-   Model: Create an algorithm or model to make a decision or complete an action.
-   Training: Give the computer system data to apply to the model and learn from.
-   Evaluation: After the computer system has had sufficient data points to get the model to operate, it is time to test the algorithm. Give the computer new data and see if it can reach the correct result.
-   Fine tuning: Continue to give the system more data for training and update the algorithm as necessary.

Software companies create applications that can help you to create ML programming. Amazon SageMaker is the main Amazon Web Services (AWS) ML service. For information about SageMaker, see [https://aws.amazon.com/sagemaker/](https://aws.amazon.com/sagemaker/).

You might have some misconceptions about ML. Let’s correct these errors in reasoning.

-   AI and ML might be mistaken for living or sentient beings. However, ML is an effort to get computers to function somewhat like a human brain—in fact, sometimes ML systems are called _neural networks_ because of this similarity. Nonetheless, the core of ML is digital and nonliving.
-   Computer systems are sometimes considered superior learners or thinkers to people. Though this can be true in speed of processing, common human errors can also exist in ML. The _garbage in, garbage out_ principle is important here. The ML system still relies on algorithms and data that are affected by underlying flaws. For example, a bank might be trying to teach a machine to recognize fraud. If customers do a poor job of reporting fraud or file untruthful reports, the data will cause the computer system to incorrectly identify fraud.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Have you noticed how your web applications seem to learn about you? When you log in to a video streaming site, do you get recommended videos? Do you have a social media account that recommends new friends or accounts to follow? How did the application know what you might like?**
2.  **Imagine that you work for a large online retail store. You want to use ML to make your marketing, advertising, and sales work more efficiently. What ideas can you propose? What information about potential customers might help you?**
3.  **Do you hate spam email? How does your email service know which emails are spam and which are not? What type of data can be analyzed about the emails to make that decision? How would you design a test that a computer system can implement to determine if email is spam or not?**

## 

**Activity: Machine Learning Scenarios**

****Overview****  
In this activity, you will learn about common uses of ML and propose ML solutions to complex problems. The class will work in teams to determine an ML solution to the scenarios and then the teams will present their ideas to the class.

**Objectives**

-   Recognize use cases for ML.
-   Explain how ML can help address a need or problem in a given situation.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss these questions.

-   **How would your ML solution improve on what humans can do? What are some tasks that humans can do better than computer systems?**

## 

**Lab: Reinforcement Learning with AWS DeepRacer**

**Overview**  
You will learn about reinforcement learning in ML by creating and evaluating an AWS DeepRacer model. You will select a sample algorithm to start with, then customize the features of the model. This model is trained and tested, permitting you to view a streaming video of the simulation.

**Objectives**

-   Recognize use cases for ML.
-   Explain how ML can help address a need or problem in a given situation.
-   Create an ML algorithm.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **What is the purpose of training in this type of reinforcement model?**
-   ****How can AWS DeepRacer or a similar technology be used in a real car?****

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

You will make a decision tree for a problem that can be addressed by ML. Then you can use one of the example scenarios in this module or make up your own scenario.

**Modeling an algorithm with a decision tree**

1.  A decision tree is a simple form of an ML algorithm. It mirrors the thought process that an AI computer system or even the human brain uses.
2.  Look at the sample decision tree to get an idea of the format and the way decisions branch out. (The process looks like a tree with branches, giving it the name decision _tree_.)
3.  Pick your own topic for a decision tree. For this example, you are advised to pick a fairly simple decision, similar to the one in the sample.
4.  Make your own decision tree.

![Decision tree to decide whether to play soccer based on the weather. If the weather is cloudy, the answer is yes. If the weather is sunny, the decision then depends on the humidity level. If the humidity is high, the answer is no; if the humidity is normal, the answer is yes. If the weather is rainy, the decision then depends on the wind level. If the wind is strong, the answer is no; if the wind is weak, the answer is yes.](https://awsacademy.contentcontroller.com/vault/9dfd9378-14e2-4cc2-a6ff-92f27de5ac5b/courses/1ca79820-ccec-46ae-8f7c-6630eb45d5d3/0/assets/QFtzzMG-YdnpgDwY_cElwnjl1m1TyVoCi.jpg)

Decision tree to decide whether to play soccer based on the weather. If the weather is cloudy, the answer is yes. If the weather is sunny, the decision then depends on the humidity level. If the humidity is high, the answer is no; if the humidity is normal, the answer is yes. If the weather is rainy, the decision then depends on the wind level. If the wind is strong, the answer is no; if the wind is weak, the answer is yes.

## 

**Additional connections**

-   Machine Learning on AWS ([https://aws.amazon.com/machine-learning/](https://aws.amazon.com/machine-learning/))