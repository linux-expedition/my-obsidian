# Module 5: Dynamic Web Servers II

Lesson 1 of 1

## 

**Module purpose**

In this module, you will review creating Amazon CloudFront distributions. You will identify real-world examples and applications that use CloudFront.

## 

**Module description**

You will explore CloudFront and its real-world applications. This will transition into implementing a CloudFront distribution to decrease latency (increase speed of delivery).

## 

**Technology terminology**

Amazon CloudFront

Click to flip

A content delivery network (CDN) service that securely delivers content such as video, data, applications, and so on

Click to flip

Content delivery network (CDN)

A network of distributed services that delivers webpages and other web content securely based on the geographic location of the user

Edge location

A location where web content is temporarily stored (cached)

Origin

The location where all the objects associated with the webpage are permanently stored

Distribution

A collection of edge locations

Time to live (TTL)

The minimum and maximum length of time to cache content at an edge location

## 

**Background and misconceptions**

CloudFront is a fast CDN service that securely delivers data, videos, applications, and application programming interfaces (APIs) to customers globally with low latency and high transfer speeds, all within a developer-friendly environment. CloudFront is integrated with Amazon Web Services (AWS)—physical locations that are directly connected to the AWS global infrastructure and other AWS services. CloudFront works seamlessly with services including AWS Shield for distributed denial of service (DDoS) mitigation, Amazon Simple Storage Service (Amazon S3), Elastic Load Balancing (ELB) or Amazon Elastic Compute Cloud (Amazon EC2) as origins for your applications, and Lambda@Edge to run custom code closer to customers’ users and customize the user experience.

The CloudFront CDN is massively scaled and globally distributed. The CloudFront network has 200 points of presence (PoPs), and leverages the highly resilient AWS backbone network for superior performance and availability for end users.

CloudFront is a highly secure CDN that provides network- and application-level protection. You can also use configurable features such as AWS Certificate Manager (ACM) to create and manage custom Secure Sockets Layer (SSL) certificates.

CloudFront features can be customized for your specific application requirements. Lambda@Edge functions, initiated by CloudFront events, extend custom code across AWS locations worldwide, so users can move even complex application logic closer to end users to improve responsiveness.

CloudFront is integrated with AWS services such as Amazon S3, Amazon EC2, ELB, Amazon Route 53, and AWS Elemental Media Services. They are all accessible through the same console, and all features in the CDN can be programmatically configured by using APIs or the AWS Management Console: [https://aws.amazon.com/cloudfront/faqs](https://aws.amazon.com/cloudfront/faqs).

## 

****Focus questions****

Follow your educator's instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Have you ever experienced a persistently slow website? Describe the website, its content, and your experiences.**
2.  **CloudFront allows websites to cache content around the world using edge locations. How could a service like CloudFront improve performance for a website that is persistently slow?**
3.  **How would using CloudFront to cache content around the world using edge locations help provide steady, expedient access to a website around the world?**

## 

**Activity: Reviewing CloudFront Distributions**

****Overview****  
You will read technical literature and engage multimedia to review creating a CloudFront distribution.

**Objective**

-   Create a CloudFront distribution to increase the speed of your website.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Why is a CloudFront distribution useful to a website?**

## 

**Lab: Creating a CloudFront distribution**

**Overview**  
You will Amazon S3 and a CloudFront distribution to launch a simple website.

**Objectives**

-   Create a CloudFront distribution to increase the speed of your website.

### 

**Lab instructions**

Follow your educator’s instructions to complete the lab.

### 

**Reflect**

After completing the lab, be prepared to answer and discuss these questions.

-   **What did you find most challenging about this lab?**
-   **When would a company such as Prime Video use a CDN?**
-   **How could you use what you have learned in this lab to share timely information with your family members living far away?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Reviewing web hosting**

1.  Reread [https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html).
2.  Create a new static webpage hosted through the AWS console.

**Unplugged option**

1.  Reread the article using a printed set of directions.
2.  Write a summary of the process for hosting a static webpage using the AWS console.

## 

**Additional connections**

-   AWS Architecture Center at [https://aws.amazon.com/architecture/](https://aws.amazon.com/architecture/)[](https://aws.amazon.com/architecture/?solutions-all.sort-by=item.additionalFields.sortDate&solutions-all.sort-order=desc&whitepapers-main.sort-by=item.additionalFields.sortDate&whitepapers-main.sort-order=desc&reference-architecture.sort-by=item.additionalFields.sortDate&reference-architecture.sort-order=desc)
-   Web Hosting at [https://aws.amazon.com/websites/getting-started/start-your-project/](https://aws.amazon.com/websites/getting-started/start-your-project/)