# Module 2: Shared Security

Lesson 1 of 1

## 

**Module purpose**

The purpose of this module is to give you an introduction to the role of Amazon Inspector in providing cloud security and the steps required to resolve an AWS Trusted Advisor security alert. The module will also compare Amazon Inspector and Trusted Advisor.

## 

**Module description**

In this module, you will develop how-to documents to resolve common security alerts. You will also fill out a graphic organizer to compare and contrast Trusted Advisor and Amazon Inspector.

## 

**Technology terminology**

Amazon Inspector 

Click to flip

An automated security assessment service that helps you test the network accessibility of your Amazon Elastic Compute Cloud (Amazon EC2) instances and the security state of your applications running on the instances. 

Click to flip

AWS Trusted Advisor 

A security assessment service that applies to an entire Amazon Web Services (AWS) account. It gives best practices advice on security, cost optimization, performance, fault tolerance, and service limits. 

Amazon Simple Storage Service (Amazon S3) 

A service provided by AWS that stores data for users in the cloud.

Multi-factor authentication (MFA) 

A security system that requires more than one method of authentication from independent categories of credentials to verify the user's identity for a login or other transaction. 

AWS Identity and Access Management (IAM) 

Involves the application of controls to users who need access to computing resources. 

Amazon Elastic Block Store (Amazon EBS)

Storage for specific EC2 instances. Think of it as the storage drive for your EC2 instance. 

Amazon Relational Database Service (Amazon RDS) 

Lets developers create and manage relational databases in the cloud. Think of a relational database as a set of data with 1-to-1 relationships. For example, a database of transactions in a department store would match every customer with their purchases. Amazon RDS lets developers track large amounts of this data and organize and search through it efficiently. Amazon RDS is equipped with a nonprocedural structured query language (SQL) that facilitates user interactions with the RDS. 

## 

**Background and misconceptions**

The multitude of services offered by AWS can be combined in a variety of ways to achieve the goals of an application. Amazon Inspector and Trusted Advisor are tools to measure how well a given approach does in achieving these goals in terms of cost, performance, efficiency, and security. This module explores Amazon Inspector and Trusted Advisor and provides a framework for comparing these two important AWS services.

-   **Trusted Advisor** is an online tool that scans your AWS infrastructure, compares it to AWS best practices in five categories, and provides real-time guidance to help customers provision resources while following AWS best practices. It highlights potential problems with the way the customer uses AWS.
-   **Amazon Inspector** tests the network accessibility of customers’ EC2 instances and the security state of applications that run on those instances. Amazon Inspector produces a detailed list of security findings, prioritized by level of severity. These findings can be reviewed directly or as part of detailed assessment reports that are available through the Amazon Inspector console or application programming interface (API). Automation is a central tenet of modern security practices; AWS customers can automate security tests with Amazon Inspector.

Here is the main difference between these services:

-   Trusted Advisor applies to the AWS account and AWS administrations.
-   Amazon Inspector applies to the content of various EC2 instances.

## 

****Focus questions****

Follow your educator’s instructions to answer and discuss the following questions.

### 

**Questions**

1.  **Make a list of functions that a nutritional advisor might perform. Which of these advisory functions can be automated? Do you foresee any security concerns with automating any of these functions?**
2.  **What functions do fitness trackers provide? How might a fitness tracker assist a nutritional advisor to meet a customer’s health and fitness needs?**
3.  **Make a list of functions that a cloud security advisor might perform. Which of these advisory functions can be automated? Do you foresee any security concerns with automating any of these functions?**
4.  **What functions would you anticipate a security tracker such as Amazon Inspector to provide? How might a security tracker assist a security advisor such as Trusted Advisor to meet the organization’s security needs?**

## 

**Activity 1: Resolving Security Threats**

****Objective****

-   List the steps required to resolve a Trusted Advisor security alert.

**Overview**  
In this activity, you will be introduced to Trusted Advisor and its purpose. You will take notes on the six core Trusted Advisor checks and the process for resolving their security alerts. You will create your own how-to documents for common security threats. You will also explore how the Trusted Advisor notification features help customers stay up to date with their AWS resource deployment.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **Which of the common security threats do you think is the most dangerous? Explain your reasoning.**

## 

**Activity 2: Comparing Trusted Advisor and Amazon Inspector**

**Objectives**

-   Explain the role of Amazon Inspector in providing cloud security.
-   Compare Amazon Inspector and Trusted Advisor.

**Overview**  
In this activity, you will be introduced to Amazon Inspector and its purpose. You will take notes on the main benefits of Amazon Inspector. You will explore Amazon Inspector using a website that includes videos. You will also complete a Venn diagram that compares and contrasts Trusted Advisor and Amazon Inspector.

### 

**Activity instructions**

Follow your educator’s instructions to complete the activity.

### 

**Reflect**

After completing the activity, be prepared to answer and discuss this question.

-   **What is an easy way to distinguish between questions that are relevant to Amazon Inspector and those relevant to Trusted Advisor?**

## 

**Optional connection**

Follow your educator’s instructions for completing this activity.

**Auto insurance estimating**

Data analytics is at work every time a retailer suggests that a customer might want to buy a product, every time social media recommends a resource page, and whenever an insurance company assesses risk and sets rates for potential policyholders. The premium charged by an insurance company is determined by statistics and mathematical calculations done by the underwriting department of the insurance company. Cloud-based platforms tailored specifically to the needs of the insurance industry run predictive models to help insurers understand their consumers and then use that predictive analysis to make business decisions.

1.  What data points would an insurance company need to determine the auto insurance rate to charge for a driver? Categorize the data points.
2.  How might an insurance company automatically collect data on current drivers?
3.  How would predictive analytics and big data permit improved insurance rate modeling?

  

## 

**Additional connections**

-   Share an Amazon EBS Snapshot: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modifying-snapshot-permissions.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modifying-snapshot-permissions.html)
-   Sharing a DB Snapshot: [https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ShareSnapshot.html](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ShareSnapshot.html)
-   Bucket Policies and User Policies: [https://docs.aws.amazon.com/AmazonS3/latest/userguide/using-iam-policies.html](https://docs.aws.amazon.com/AmazonS3/latest/userguide/using-iam-policies.html)
-   AWS Trusted Advisor for Everyone: [https://aws.amazon.com/blogs/aws/trusted-advisor-console-basic/](https://aws.amazon.com/blogs/aws/trusted-advisor-console-basic/)