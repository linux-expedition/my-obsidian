
https://dribbble.com/shots/15318390--Exploration-Typography-and-Layout-Pitch-Deck/attachments/7075634?mode=media

https://designtemplateplace.com/product/nadire-presentation-template-215265

https://dribbble.com/shots/15944291-Pitch-Template



- Laporan dibuat dengan menuliskan: (1) Latar belakang (2) Konsep dari content/product yang akan dibuat (3) Referensi/model yang digunakan (4) Jadwal (5) Job desc anggota (6) Tools yang digunakan
- Konsep jelas (ada contoh atau prototype dari product) ada referensi
- Ada timeline dan jadwal di setiap minggu ada


https://id.pinterest.com/pin/753438212672166517/

https://gist.github.com/meain/6440b706a97d2dd71574769517e7ed32


backend
https://docs.google.com/presentation/d/1SzicHAXBPclrMzP8jpelBZcD_C1m-6ZoWUwZnUGxd1s/edit?usp=sharing

Frontend
https://docs.google.com/presentation/d/1LomoMB8Yq-7P_t5J8Q7VAsQAeFLOlO2wJX-Zhg8LkAc/edit?usp=sharing

##### Source
Materi PPT Underground ([https://pptunderground.com/](https://pptunderground.com/ "https://pptunderground.com/"))

1.  Perkenalan [https://drive.google.com/drive/folders/1u5WBlRxy7TOb70T9WvE7n-4WXaRkBNrM?usp=sharing](https://drive.google.com/drive/folders/1u5WBlRxy7TOb70T9WvE7n-4WXaRkBNrM?usp=sharing "https://drive.google.com/drive/folders/1u5wblrxy7tob70t9wve7n-4wxarkbnrm?usp=sharing")
2.  Persiapan [https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing](https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing "https://drive.google.com/drive/folders/1p8dcnzu5njt67awxnkqsrl7ql0crqefy?usp=sharing")
3.  Mastering Powerpoint [https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing](https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing "https://drive.google.com/drive/folders/1p8dcnzu5njt67awxnkqsrl7ql0crqefy?usp=sharing")
4.  Mastering Layout Design [https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing](https://drive.google.com/drive/folders/1p8DcNzU5NJt67AwXNkQSrL7Ql0cRqEFy?usp=sharing "https://drive.google.com/drive/folders/1p8dcnzu5njt67awxnkqsrl7ql0crqefy?usp=sharing")
5.  Study case [https://drive.google.com/drive/folders/18CaQmW7bnQQ9rOZ4HqpdYiJJ6S4IUqp7?usp=sharing](https://drive.google.com/drive/folders/18CaQmW7bnQQ9rOZ4HqpdYiJJ6S4IUqp7?usp=sharing "https://drive.google.com/drive/folders/18caqmw7bnqq9roz4hqpdyijj6s4iuqp7?usp=sharing")
