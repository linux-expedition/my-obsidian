.#### Simulasi Instrumen Investasi
- kumpulkan data terlebih dahulu
- 4 Instrument investasi
	- deposito | bca
	- emas
	- reksadana
	- Saham
- Dari ke-4 instrumen, buat simulasi dgn modal awal 100 juta
- Hitung nilainya selama **5 tahun** 
- Strategi apa yg dilakukan jika melakukan investasi di atas satu per satu (2017-2022)
- Bisa menggunakan strategi [[#lamsam]] atau secara bertahap misal bulan ini 10 jt, bulan depan 10jt, dst sampai habis.Per thn/ per 6 bln/ terserah. Intinya kapan semua modal dihabiskan. 
- Lalu simulasikan jika invest emas, dst.
- Asumsinya tidak tahu arah harga(yg diinvestasi) akan naik atau turun
- Kesimpulan, manakah dari 4 instrumen investasi ini yg menghasilkan hasil yang terbaik. Dan bertambah berapa persen kah dari nilai awal 100jt
- Bagaimana mengatur strateginya dan membandingkan mana yg paling menguntungkan


https://benefits.bankmandiri.co.id/article/tips-memilih-bank-deposito-terbaik


#### key terms
##### lamsam
100 jt ditaruh atau semua modal langsung dihapuskan



#### Deposit
[How Do You Calculate Present Value (PV) in Excel?](https://www.investopedia.com/ask/answers/040315/how-do-you-calculate-present-value-excel.asp)
[simulasi deposit calculator](https://www.simulasikredit.com/simulasi_perhitungan_deposito.php)
[Pengertian Deposito yang Harus Anda Pahami](https://www.cimbniaga.co.id/id/inspirasi/perencanaan/pengertian-deposito-yang-harus-anda-pahami)
[BNI Deposito](https://www.bni.co.id/id-id/personal/simpanan/bnideposito)

#### Gold
[Monex Gold Prices](https://www.monex.com/gold-prices/)
[Perbedaan investasi emas ETF dan fisik](https://www.suaramalam.com/714/perbedaan-investasi-emas-etf-dan-fisik-di-jm-bullion/.html)
[Tips invest emas](https://www.prudential.co.id/id/pulse/article/mau-investasi-emas-ketahui-terlebih-dahulu-tips-berikut-ini/)
