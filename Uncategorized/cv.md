I am a hardworking and ambitious individual with a great passion for the transport and logistics industry. I am currently in my second year of studying BA Logistics and Supply Chain Management at Aston University. I have excellent communication skills, enabling me to effectively communicate with a wide range of people. I am seeing a part-time position in the industry in which I can put into practice my knowledge and experience, ultimately benefiting the operations of the organisation that I work for.


I am a dedicated, hardworking and proactive Chemical Engineer with a strong background in design, plant operations, offshore operations, and process and safety improvements. I have solid work experience in designing, testing and analysing processes to increase the overall efficiency of operations. I am currently looking for an opportunity to utilise my technical skills in a challenging working environment and become a valuable asset to the organisation that I work for.


I am a hardworking and ambitious individual with a great passion for the transport and logistics industry. I am currently in my second year of studying BA Logistics and Supply Chain Management at Aston University. I have excellent communication skills, enabling me to effectively communicate with a wide range of people. I am seeing a part-time position in the industry in which I can put into practice my knowledge and experience, ultimately benefiting the operations of the organisation that I work for.


I am a highly competent IT professional with a proven track record in designing websites, networking and managing databases. I have strong technical skills as well as excellent interpersonal skills, enabling me to interact with a wide range of clients. I am eager to be challenged in order to grow and further improve my IT skills. My greatest passion is in life is using my technical know-how to benefit other people and organisations.


I am a creative and enthusiastic Pharmacologist with two years’ experience in the pharmaceutical industry. I have experience in designing and carrying out experiments, testing drugs, researching scientific literature and writing technical reports. I possess excellent analytical and communications skills and a dedicated approach to working in a highly controlled working environment. I am currently looking for a challenging opportunity within the science sector.


Highly organized and detail-oriented honors graduate from the University of Georgia seeking an entry-level position as an accountant. Served as a peer tutor for courses such as general accounting, budgeting and forecasting, and accounting principles and legislation.


Successful English teacher with more than 10 years of professional experience. Seeking a career change to a position involving copywriting for a medium to large marketing firm. Driven self-starter and fast learner who has volunteered to help teachers in various courses with their lesson-plan writing and after-school tutoring.


I am a highly organized and ambitious individual with a great passion for backend and android development.