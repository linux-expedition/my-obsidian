https://www.analyticsvidhya.com/blog/2017/02/test-data-scientist-clustering/#:~:text=K%2DMeans%20clustering%20algorithm%20fails,points%20follow%20non%2Dconvex%20shapes.

https://mcqmate.com/discussion/347/which-of-the-following-is-a-good-test-dataset-characteristic

http://theprofessionalspoint.blogspot.com/2019/02/advantages-and-disadvantages-of.html#:~:text=24%20February%202019-,Advantages%20and%20Disadvantages%20of%20Decision%20Trees%20in%20Machine%20Learning,to%20overfitting%20of%20the%20data.

https://www.examveda.com/special-programs-that-assist-programmers-are-called-12558/#:~:text=A.,intelligent%20programming%20tools

https://www.examveda.com/what-is-the-term-used-for-describing-the-judgmental-or-commonsense-part-of-problem-solving-12470/



Which of the following is a disadvantage of decision trees?
Factor analysis
Decision trees are robust to outliers
**Decision trees are prone to be overfit**
None of the above

A natural language generation program must decide
what to say
when to say something
why it is being used
**both (a) and (b)**

In which of the following cases will K-means clustering fail to give good results?
1) Data points with outliers
2) Data points with different densities
3) Data points with nonconvex shapes
1 and 2
2 and 3
**1, 2, and 3**
1 and 3

Which of the following is a good test dataset characteristic?
Large enough to yield meaningful results
Is representative of the dataset as a whole
**Both A and B**
None of the above

Special programs that assist programmers are called 
heuristic processors
symbolic programmers
**intelligent programming tools**
program recognizers


The area of AI that investigates methods of facilitating communication between people and computer is __________________.
**Natural language processing**
Symbolic processing
Decision support
Robotics


What is the term used for describing the judgmental or commonsense part of problem solving?
**Heuristic**
Critical
Value based
Analytical

Research scientists all over the world are taking steps towards building computers with circuits patterned after the complex inter connections existing among the human brain's nerve cells.
What name is given to such type of computers?
**Neural Network computer**


The most widely used metrics and tools to assess a classification model are 
Confusion matrix
Cost-sensitive accuracy
area under the ROC curve
**All of the above**


What is the purpose of performing cross-validation?
To assess the predictive performance of the models
To judge how the trained model performs outside the sample on test data
**Both A and B**
None of the above


The first widely-used commercial form of Artificial Intelligence (Al) is being used in many popular products like microwave ovens, automobiles and plug in circuit boards for desktop PCs. It allows machines to handle vague information with a deftness that mimics human intuition.
What is the name of this AI?
**Fuzzy Logic**

Which of the following is an example of feature extraction?
Constructing bag of words vector from an email
Applying PCA projects to a large high-dimensional data
Removing stopwords in a sentence
**All of the above**

When performing regression or classification, which of the following is the correct way to preprocess the data?
**Normalize the data -> PCA -> training**

What decade did formal AI research begin?
**1950s**

Output segments of AI programming contain(s) ...
printed language and synthesized speech
Manipulation of physical object
Locomotion
**All of the above**

To invoke the LISP system, you must enter ...
AI
LISP
CL (Common Lisp)
**both b and c**


Which of the following statements about regularization is not correct?
Using too large a value of lambda can cause your hypothesis to underfit the data.
Using too large a value of lambda can cause your hypothesis to overfit the data.
Using a very large value of lambda cannot hurt the performance of your hypothesis.
**None of the above**


Software designed to facilitate a conversation between a computer and a human end-user is called a 
**Chatbot**
Real time messaging
Interactive voice response
Walkie Talkie



The primary interactive method of communication used by humans is 
reading
writing
**speaking**
All of the above


What tasks can AI perform?
Act as a virtual assistant
Automate manual processes
Analyze unstructured data
**All of the above**





The practice of using algorithms to parse data. learn from it. and then make a determination or prediction about something is
**Machine learning**

Why is second order differencing in time series needed?
To remove stationarity
**To find the maxima or minima at the local point**
Both A and B
None of the above

How can you prevent a clustering algorithm from getting stuck in bad local optima?
Set the same seed value for each run
**Use multiple random initializations**
Both A and B
None of the above


Which of the following techniques can be used for normalization in text mining?
Stemming
Lemmatization
Stop Word Removal
**Both A and B**


The characteristics of the computer system capable of thinking. reasoning and learning is known is
**artificial intelligence**


Natural language processing is divided into the two subfields of
symbolic and numeric
time and motion
algorithmic and heuristic
**understanding and generation**


The first AI programming language was called
BASIC
FORTRAN
**IPL**
LISP



A computer vision technique that relies on image templates is
edge detection
binocular vision
**model-based vision**
robot vision


What is the weakness of AI?
- the embodiment of human intellectual capabilities within a computer.
- a set of computer programs that produce output that would be considered to reflect intelligence if it were generated by humans.
- **the study of mental faculties through the use of mental models implemented on a computer.**
- All of the above


Which of the following is a widely used and effective machine learning algorithm based on the idea of bagging?
Decision tree
Regression
Classification
**Random Forest**


One method of programming a computer to exhibit human intelligence is called modeling or ____________.
**simulation**
cognitization
duplication
psychic amelioration



How do you handle missing or corrupted data in a dataset?
Drop missing rows or columns
Replace missing values with mean/median/mode
Assign a unique category to missing values
**All of the above**



What is artificial intelligence?
Putting your intelligence into a computer
Programming with your own language
**Making a machine intelligent**
Putting more memory into a computer




Which of the following is a good test dataset characteristic?
Large enough to yield meaningful results
Is representative of the dataset as a whole
**Both A and B**
None of the above




Output segments of AI programming contain(s)
printed language and synthesized speech
Manipulation of physical object
Locomotion
**All of the above**





What stage of the manufacturing process has been described as "the mapping of function onto form"?
**Design**
Distribution
Project Management
Field Service




Which of the following is an example of AI?
Finding the CPU on a server
**Software changing from one network to another when it detects a problem**
Offering augmented reality (AR) information in headsets for service technicians
Reformatting a hard drive





If a robot can alter its own trajectory in response to external conditions. it is considered to be
**intelligent**
mobile
open loop
non-servo




Which of the following have computers traditionally done better than people?
storing information
responding flexibly
computing numerically
**both (a) and (c)**




What tasks can AI perform?
Act as a virtual assistant
Automate manual processes
Analyze unstructured data
**All of the above**




Decision support programs are designed to help managers make
budget projections
visual presentations
**business decisions**
vacation schedules