Source: [healthline.com](https://www.healthline.com/health/how-to-fix-lazy-eye#overview)
[[#Navigation]]

---


Lazy eye, or [amblyopia](https://www.healthline.com/health/lazy-eye), is a condition that causes poor vision, usually in one eye. It affects about [3 out of every 100Trusted Source](https://www.nei.nih.gov/learn-about-eye-health/eye-conditions-and-diseases/amblyopia-lazy-eye) children.

People with lazy eye have one eye that is stronger than the other, because the brain and the weaker eye do not communicate well.

Your eyes and brain must work together for vision to occur. To enable this, your retina sends nerve signals from the back of the eye to the optic nerve, which carries signals to the brain. There, they become interpreted as the things you see.

If you have one eye that’s weaker than the other, your brain may start to favor the stronger eye and stop receiving signals from the weaker eye.

Without treatment, lazy eye can get worse over time. But the condition is treatable. In this article, we’ll go over the various treatments available for this condition, and how you can get the best results.

## Is it possible to correct lazy eye?

The nerves and communication pathways that connect the eyes to the brain form in childhood. For this reason, lazy eye treatment is often most effective in people who are 7 years old or younger.

The earlier treatment starts, the more likely you are to get good results. However, positive results can still be seen in teenagers, up to around age 17.

If you have lazy eye and are older than 17, don’t let your age be a deterrent. Even adults with lazy eye can often achieve better vision with treatment, so it’s worth talking to your doctor about options.

Treatment options for lazy eye include:

-   corrective eyeglasses and contact lenses
-   eyepatches
-   Bangerter filter
-   eye drops
-   training
-   surgery

We’ll review each option below.

## Corrective eyeglasses or contact lenses

Lazy eye is sometimes caused by differing vision in each eye. For example, one eye may be [farsighted (hyperopia)](https://www.healthline.com/health/farsightedness) or [nearsighted (myopia)](https://www.healthline.com/health/nearsightedness). This causes a difference in vision sharpness between each eye. This is called refractive amblyopia.

[Astigmatism](https://www.healthline.com/health/astigmatism), or an irregular curve in the cornea, in one eye can also cause lazy eye.

These causes of lazy eye can often be corrected with [eyeglasses](https://www.healthline.com/health/contacts-online) or [contact lenses](https://www.healthline.com/health/glasses-online).

### Getting a prescription

To get this type of eyewear, you or your child will need to have your eyes examined and evaluated by an eye doctor, such as an [ophthalmologist](https://www.healthline.com/health/eye-health/optometrist-vs-ophthalmologist) or optometrist.

You’ll need a prescription for corrective eyewear, and you can typically have glasses made by an optometrist or optician.

### Cost

If you have health insurance with vision benefits, the cost for corrective lenses should be included in your coverage. However, you may still have to pay a deductible or coinsurance amount.

Each insurance company varies in terms of coverage. Make sure you check with your provider, so you can best determine what your out-of-pocket costs will be.

If you don’t have health insurance, your costs for corrective lenses may vary based upon your geographic area and the type of glasses you buy. You may expect to pay anywhere from $35 to several hundred dollars for glasses.

## Eyepatches

Wearing an eyepatch is a simple, cost-effective treatment for lazy eye. It helps improve vision in the weaker eye.

You should wear the eyepatch over the eye that has better vision for around 2 to 6 hours daily. Your doctor will tell you how long you should keep the patch on.

It’s important to follow your doctor’s instructions. Wearing the patch for many hours can sometimes cause lazy eye to manifest in the stronger eye. When this happens, the condition is typically rectified easily with treatment.

### Where to find

Eyepatches may be used alone or with corrective lenses. Your doctor may be able to supply you with eyepatches. If not, they’re readily available in pharmacies and [online](https://www.amazon.com/s?k=eye+patches+for+lazy+eye&crid=1OTLOWKU80D9F&sprefix=antibac%2Caps%2C188&ref=nb_sb_ss_i_5_7), and they tend to be inexpensive.

Many eyepatches have cute designs so that small children will be less resistant to wearing them.

## Bangerter filter

Children who cannot tolerate eyepatches may get the same or similar results with Bangerter filters. These filters are a type of opaque covering that fits on the inside of the eyeglass lens worn over the dominant eye.

Bangerter filters should be worn full time. They can be modified for density and opaqueness over time, as symptoms improve. For this reason, they may be useful after patching has occurred, as a secondary treatment.

## Eye drops

Medicated eye drops can be used to blur the vision in the dominant eye, making the weaker eye work harder. The medication typically used is atropine, which is sold under the brand name Isopto Atropine.

Atropine dilates the eye’s pupil, causing blurry vision. It may be used several times daily to diminish vision in the dominant eye, making the lazy eye work harder.

### You’ll need a prescription

Atropine is available by prescription only and should be used according to your doctor’s directions.

### Cost

Atropine is covered by insurance, though your plan may require you to get the generic type. This medication may range in price from $25 to over $60.

## Training

Games and activities designed to challenge the weak eye have been shown to be beneficial, but these may not be enough to correct vision as a stand-alone treatment.

Eye training tools include specific types of computer or iPad games and activities such as putting together jigsaw puzzles and drawing pictures.

Training with computer games and videos has been shown to be effective in several small studies, including [one from 2016Trusted Source](https://jamanetwork.com/journals/jamaophthalmology/fullarticle/2579931) and [one from 2018](https://www.sciencedirect.com/science/article/pii/S0042698918300580?via%3Dihub). However, more research is needed before it can be considered effective enough to use without other forms of therapy, such as wearing an eyepatch.

## Surgery

Surgery for lazy eye is done to adjust the length or positioning of the eye muscles. It may be used when amblyopia is caused by a:

-   squint
-   droopy eyelid
-   cataract

Surgical solutions for lazy eye usually require additional strategies, such as eye patching, in order to correct vision. Surgery is also used to improve the cosmetic appearance of the eye.

### Success rates

Success rates for this type of surgery vary significantly, from around 30 to 80 percent.

### Risks

The risks associated with this type of surgery include overcorrection or undercorrection of the eye. There are also the usual minimal risks associated with any type of surgery, such as infection.

Complications that might result in loss of vision are very rare.

### Recovery

Recovery time at home will take a few days to a week. During this time, there may be red or pink tears coming out of the eye. The eye may also be red. Mild pain and swelling are to be expected.

### Cost

Costs for this type of surgery will depend on your insurance and geographic location. They may be as high as $6,000 or more.

### Can laser refractive surgery correct lazy eye?

Laser refractive surgery may be used to improve mild or moderate amblyopia in children and adults.

## Early diagnosis is important

Lazy eye often goes undiagnosed in children. This may lead to vision loss.

If you suspect that you or your child has lazy eye, it’s important to see a doctor. They can recommend treatment options designed specifically to address the underlying problem, saving you time and possibly your vision.

You can talk to a general practitioner, or you can look for a board certified specialist in your area using this online [tool](https://www.optometrists.org/findeyedoctordirectory/).

## Takeaway

Lazy eye, or amblyopia, affects around 3 out of every 100 children. The condition is treatable and typically responds well to strategies such as eye patching and wearing corrective lenses.

The best results for lazy eye are typically seen when the condition is treated early, in children who are 7 years old or younger.




---
## Navigation
[[#Is it possible to correct lazy eye]]
[[#Corrective eyeglasses or contact lenses]]
	[[#Getting a prescription]]
	[[#Cost]]
	[[#Eyepatches]]
	[[#Where to find]]
[[#Bangerter filter]]
[[#Eye drops]]
	[[#You’ll need a prescription]]
	[[#Cost]]
[[#Training]]
[[#Surgery]]
	[[#Success rates]]
	[[#Risks]]
	[[#Recovery]]
	[[#Cost]]
	[[#Can laser refractive surgery correct lazy eye]]
[[#Early diagnosis is important]]
[[#Takeaway]]