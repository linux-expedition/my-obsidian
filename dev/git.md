
# Branch
git branch -a
git branch -d branch-name
git push origin -d branch-name

# Deal git warning pulling
```
warning: Pulling without specifying how to reconcile divergent branches is
discouraged
```
Deal with
``` bash
git pull --ff-only
```

###### Change git configuration
If you have Git 2.29 or above, you can now set `pull.ff` to `false`, `true` or `only` to get rid of the warning.
``` bash
git config pull.ff true
```
`true` - This is the default behaviour. Pull is fast-forwarded if possible, otherwise it's merged.
``` bash
git config pull.ff false
```
`false` - Pull is never fast-forwarded, and a merge is always created.
``` bash
git config pull.ff only
```
`only` - Pull is fast-forwarded if possible, otherwise operation is aborted with an error message.

